//
//  ModelMorBijili.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 11/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import Foundation
import Reachability
import AlamofireSoap
import Alamofire
import SwiftyXMLParser
class ElectricityPartnerModelDataAPI
{
    
    var reachablty = try! Reachability()
    
    
    func serviceCalling( obj:HomeViewController ,success:@escaping (AnyObject)-> Void)
    {
        
       // obj.startLoading()
        reachablty = try! Reachability()
        if(reachablty.connection != .unavailable)
        {
           
           
           
                 let dateFormatterInv = DateFormatter()
                 dateFormatterInv.timeZone = NSTimeZone.system
                 dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
            let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
              let invStrUnique = dateFormatterInv.string(from: Date())
              let request_no = invStrUnique + mobile
              let message = "ZBAPI_CS_CTI_BPDETAIL|\(mobile)|\(secID)|\(request_no)"

                  let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")



              AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_dataset", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
               print("Request: \(response.value)")
               guard let respo = response.value else {
                   return
               }

               let xmlStr = try! XML.parse(respo)
              let accessor = xmlStr["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table1"]
                obj.deleteParnterData()
              guard let countSet = accessor.all?.count else {
                obj.getPartnerSharingData()
                  return
              }

                var dataArray : [String] = []
                if(countSet > 0){
                    for i in 0...countSet - 1  {
                        let child = accessor.all![i]
                        let val = child.childElements[1].text
                        dataArray.append(val!)
                    }
                }
               
                success(dataArray as AnyObject)

              }

            
        }
        else{

        }
        
        
    }
    
    
}
class ElectricityRemovePartnerModelDataAPI
{
    
    var reachablty = try! Reachability()
    
    
    func serviceCalling( obj:RemoveConsumerViewController ,success:@escaping (AnyObject)-> Void)
    {
        
       // obj.startLoading()
        reachablty = try! Reachability()
        if(reachablty.connection != .unavailable)
        {
           
           
           
                 let dateFormatterInv = DateFormatter()
                 dateFormatterInv.timeZone = NSTimeZone.system
                 dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
            let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
              let invStrUnique = dateFormatterInv.string(from: Date())
              let request_no = invStrUnique + mobile
              let message = "ZBAPI_CS_CTI_BPDETAIL|\(mobile)|\(secID)|\(request_no)"

                  let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")



              AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_dataset", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
              // print("Request: \(response.value)")
               guard let respo = response.value else {
                   return
               }

               let xmlStr = try! XML.parse(respo)
              let accessor = xmlStr["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table1"]
              guard let countSet = accessor.all?.count else {
                obj.getPartnerSharingData()
                  return
              }

                var dataArray : [String] = []
                if(countSet > 0){
                    for i in 0...countSet - 1  {
                        let child = accessor.all![i]
                        let val = child.childElements[1].text
                        dataArray.append(val!)
                    }
                }
               
                success(dataArray as AnyObject)

              }

            
        }
        else{

        }
        
        
    }
    
    
}
class ElectricityAddPartnerModelDataAPI
{
    
    var reachablty = try! Reachability()
    
    
    func serviceCalling( obj:AddConsumerViewController ,success:@escaping (AnyObject)-> Void)
    {
        
       // obj.startLoading()
        reachablty = try! Reachability()
        if(reachablty.connection != .unavailable)
        {
           
           
           
                 let dateFormatterInv = DateFormatter()
                 dateFormatterInv.timeZone = NSTimeZone.system
                 dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
            let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
              let invStrUnique = dateFormatterInv.string(from: Date())
              let request_no = invStrUnique + mobile
              let message = "ZBAPI_CS_CTI_BPDETAIL|\(mobile)|\(secID)|\(request_no)"

                  let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")



              AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_dataset", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
              // print("Request: \(response.value)")
               guard let respo = response.value else {
                   return
               }

               let xmlStr = try! XML.parse(respo)
              let accessor = xmlStr["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table1"]
              guard let countSet = accessor.all?.count else {
                obj.getPartnerSharingData()
                  return
              }

                var dataArray : [String] = []
                if(countSet > 0){
                    for i in 0...countSet - 1  {
                        let child = accessor.all![i]
                        let val = child.childElements[1].text
                        dataArray.append(val!)
                    }
                }
               
                success(dataArray as AnyObject)

              }

            
        }
        else{

        }
        
        
    }
    
    
}
class ElectricityRemoveModelDataAPI
{
    
    var reachablty = try! Reachability()
    
    
    func serviceCalling( obj:RemoveConsumerViewController ,bpNo : String,success:@escaping (AnyObject)-> Void)
    {
        
       // obj.startLoading()
        reachablty = try! Reachability()
        if(reachablty.connection != .unavailable)
        {
                    obj.startLoadingPK(view: obj.view, text: "please wait...")
                   
                   
                         let dateFormatterInv = DateFormatter()
                         dateFormatterInv.timeZone = NSTimeZone.system
                         dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
                    let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
                      let invStrUnique = dateFormatterInv.string(from: Date())
                      let request_no = invStrUnique + bpNo
                      let message = "ZBAPI_BI_LTBILLDISP|\(bpNo)|||\(secID)|\(request_no)"
        print(message)
                          let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
        print(encode)


                      AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_dataset", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
                      // print("Request: \(response.value)")
                       guard let respo = response.value else {
                        obj.stopLoadingPK(view: obj.view)
                        obj.getBillDetailsData(bpno: obj.cellData.lblPartner.text!)
                           return
                       }
                        obj.stopLoadingPK(view: obj.view)
                        print(response.value)
                       let xml = try! XML.parse(respo)
                      let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table6"]
                        let accessorTable2 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table2"]
                        let accessorTable1 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table1"]
                         let accessorTable3 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table3"]
                        
                        let accessorTable4 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table4"]
                        let accessorTable5 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table5"]
                         let accessorTable7 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table7"]
                        
                        
                        
                        var dataArray : [ElectricMainDetailsDisplay] = []
                        guard let countSetTable1 = accessorTable1.all?.count else {
                         obj.stopLoadingPK(view: obj.view)
                         obj.getBillDetailsData(bpno: obj.cellData.lblPartner.text!)
                                        return
                        }
                        guard let countSetTable2 = accessorTable2.all?.count else {
                         // obj.getBillDetailsData(bpno: bpNo)
                                        return
                        }
                        guard let countSetTable3 = accessorTable3.all?.count else {
                         // obj.getBillDetailsData(bpno: bpNo)
                                        return
                        }
                        guard let countSetTable4 = accessorTable4.all?.count else {
                         // obj.getBillDetailsData(bpno: bpNo)
                                        return
                        }
                        guard let countSetTable5 = accessorTable5.all?.count else {
                         // obj.getBillDetailsData(bpno: bpNo)
                                        return
                        }
                        guard let countSetTable7 = accessorTable7.all?.count else {
                         // obj.getBillDetailsData(bpno: bpNo)
                                        return
                        }
                        guard let countSet = accessor.all?.count else {
                          // obj.getBillDetailsData(bpno: bpNo)
                                         return
                         }
                        
                        if(countSetTable1 > 0){
                            for i in 0...countSetTable1 - 1  {
                                
                                 let objCons = ElectricMainDetailsDisplay()
                                 let RAOTXT = accessorTable1[i]["RAOTXT"].text
                                 let DIVTXT = accessorTable1[i]["DIVTXT"].text
                                 let BP_NO = accessorTable1[i]["BP_NO"].text
                                 let BP_NAME = accessorTable1[i]["BP_NAME"].text
                                 let ADD1 = accessorTable1[i]["ADD1"].text
                                 let ADD2 = accessorTable1[i]["ADD2"].text
                                 let ADD3 = accessorTable1[i]["ADD3"].text
                                 let RT_CT = accessorTable1[i]["RT_CT"].text
                                 let SD_1 = accessorTable1[i]["SD_1"].text
                                 let TEA = accessorTable1[i]["TEA"].text
                                 let SD_INT = accessorTable1[i]["SD_INT"].text
                                 let TEL_NUMBER = accessorTable1[i]["TEL_NUMBER"].text
                                if(RAOTXT == nil){
                                    objCons.RAOTXT = ""
                                }else{
                                    objCons.RAOTXT = RAOTXT!
                                }
                                if(DIVTXT == nil){
                                    objCons.DIVTXT = ""
                                }else{
                                    objCons.DIVTXT = DIVTXT!
                                }
                                if(BP_NAME == nil){
                                    objCons.BP_NAME = ""
                                }else{
                                    objCons.BP_NAME = BP_NAME!
                                }
                                if(BP_NO == nil){
                                    objCons.BP_NO = ""
                                }else{
                                    objCons.BP_NO = BP_NO!
                                }
                                if(ADD1 == nil){
                                    objCons.ADD1 = ""
                                }else{
                                    objCons.ADD1 = ADD1!
                                }
                                if(ADD2 == nil){
                                    objCons.ADD2 = ""
                                }else{
                                    objCons.ADD2 = ADD2!
                                }
                                if(ADD3 == nil){
                                    objCons.ADD3 = ""
                                }else{
                                    objCons.ADD3 = ADD3!
                                }
                                if(RT_CT == nil){
                                    objCons.RT_CT = ""
                                }else{
                                    objCons.RT_CT = RT_CT!
                                }
                                if(SD_1 == nil){
                                    objCons.SD_1 = ""
                                }else{
                                    objCons.SD_1 = SD_1!
                                }
                                if(TEA == nil){
                                    objCons.TEA = ""
                                }else{
                                    objCons.TEA = TEA!
                                }
                                if(SD_INT == nil){
                                    objCons.SD_INT = ""
                                }else{
                                    objCons.SD_INT = SD_INT!
                                }
                                if(TEL_NUMBER == nil){
                                    objCons.TEL_NUMBER = ""
                                }else{
                                    objCons.TEL_NUMBER = TEL_NUMBER!
                                }
                               
                                dataArray.append(objCons)
                            }
                        }
                       
                        if(countSetTable3 > 0){
                            for i in 0...countSetTable3 - 1  {
                                let objCons = ElectricBillUnitDetailsDisplay()
                                let CON_LD = accessorTable3[i]["CON_LD"].text
                                let CON_LOAD = accessorTable3[i]["CON_LOAD"].text
                                let CR_RD = accessorTable3[i]["CR_RD"].text
                                let PR_RD = accessorTable3[i]["PR_RD"].text
                                let CONS = accessorTable3[i]["CONS"].text
                                let MD = accessorTable3[i]["MD"].text
                                
                                let KWH_ON = accessorTable3[i]["KWH_ON"].text
                                let PF = accessorTable3[i]["PF"].text
                                let CONS_TEXT = accessorTable3[i]["CONS_TEXT"].text
                                let SOLAR_UNIT = accessorTable3[i]["SOLAR_UNIT"].text
                                let BILLING_TYPE = accessorTable3[i]["BILLING_TYPE"].text
                                let PURPOSE = accessorTable3[i]["PURPOSE"].text
                                let MF = accessorTable3[i]["MF"].text
                                let ASSD_UNIT = accessorTable3[i]["ASSD_UNIT"].text
                                let TOT_UNIT = accessorTable3[i]["TOT_UNIT"].text
                                let AVRG_QTY = accessorTable3[i]["AVRG_QTY"].text
                                if(CON_LD == nil){
                                    objCons.CON_LD = ""
                                }else{
                                    objCons.CON_LD = CON_LD!
                                }
                                if(CON_LOAD == nil){
                                    objCons.CON_LOAD = ""
                                }else{
                                    objCons.CON_LOAD = CON_LOAD!
                                }
                                if(CR_RD == nil){
                                    objCons.CR_RD = ""
                                }else{
                                    objCons.CR_RD = CR_RD!
                                }
                                if(PR_RD == nil){
                                    objCons.PR_RD = ""
                                }else{
                                    objCons.PR_RD = PR_RD!
                                }
                                if(CONS == nil){
                                    objCons.CONS = ""
                                }else{
                                    objCons.CONS = CONS!
                                }
                                if(MD == nil){
                                    objCons.MD = ""
                                }else{
                                    objCons.MD = MD!
                                }
                                if(KWH_ON == nil){
                                    objCons.KWH_ON = ""
                                }else{
                                    objCons.KWH_ON = KWH_ON!
                                }
                                if(PF == nil){
                                    objCons.PF = ""
                                }else{
                                    objCons.PF = PF!
                                }
                                if(CONS_TEXT == nil){
                                    objCons.CONS_TEXT = ""
                                }else{
                                    objCons.CONS_TEXT = CONS_TEXT!
                                }
                                if(SOLAR_UNIT == nil){
                                    objCons.SOLAR_UNIT = ""
                                }else{
                                    objCons.SOLAR_UNIT = SOLAR_UNIT!
                                }
                                if(BILLING_TYPE == nil){
                                    objCons.BILLING_TYPE = ""
                                }else{
                                    objCons.BILLING_TYPE = BILLING_TYPE!
                                }
                                if(PURPOSE == nil){
                                    objCons.PURPOSE = ""
                                }else{
                                    objCons.PURPOSE = PURPOSE!
                                }
                                if(MF == nil){
                                    objCons.MF = ""
                                }else{
                                    objCons.MF = MF!
                                }
                                if(ASSD_UNIT == nil){
                                    objCons.ASSD_UNIT = ""
                                }else{
                                    objCons.ASSD_UNIT = ASSD_UNIT!
                                }
                                if(TOT_UNIT == nil){
                                    objCons.TOT_UNIT = ""
                                }else{
                                    objCons.TOT_UNIT = TOT_UNIT!
                                }
                                if(AVRG_QTY == nil){
                                    objCons.AVRG_QTY = ""
                                }else{
                                    objCons.AVRG_QTY = AVRG_QTY!
                                }
                             
                                obj.electricUnitsDetailsDB.append(objCons)
                            }
                        }
                       if(countSetTable4 > 0){
                            for i in 0...countSetTable4 - 1  {
                                
                                let objCons = ElectricBillBPDataDetailsDisplay()
                                let EC_1 = accessorTable4[i]["EC_1"].text
                                let FIX_CH_1 = accessorTable4[i]["FIX_CH_1"].text
                                let FCA_1 = accessorTable4[i]["FCA_1"].text
                                let BP_NAME_QUOTES = accessorTable4[i]["BP_NAME_QUOTES"].text
                                let ADD1_QUOTES = accessorTable4[i]["ADD1_QUOTES"].text
                                let CESS_1 = accessorTable4[i]["CESS_1"].text
                                let ADD2_QUOTES = accessorTable4[i]["ADD2_QUOTES"].text
                                let MTR_1 = accessorTable4[i]["MTR_1"].text
                                let ADD3_QUOTES = accessorTable4[i]["ADD3_QUOTES"].text
                                let WTCS_1 = accessorTable4[i]["WTCS_1"].text
                                let DIVTXT_QUOTES = accessorTable4[i]["DIVTXT_QUOTES"].text
                                let ASD_1 = accessorTable4[i]["ASD_1"].text
                                let REB_1 = accessorTable4[i]["REB_1"].text
                                let ADJ_DMD_1 = accessorTable4[i]["ADJ_DMD_1"].text
                                let AVG_ADJ_1 = accessorTable4[i]["AVG_ADJ_1"].text
                                let MIS_1 = accessorTable4[i]["MIS_1"].text
                                let PRE_ARR_1 = accessorTable4[i]["PRE_ARR_1"].text
                                let TOT_BILL_1 = accessorTable4[i]["TOT_BILL_1"].text
                                let TOT_AMT_1 = accessorTable4[i]["TOT_AMT_1"].text
                                let AMT = accessorTable4[i]["AMT"].text
                                let SOLAR_RBT = accessorTable4[i]["SOLAR_RBT"].text
                                let SG400U_50PR = accessorTable4[i]["SG400U_50PR"].text
                                let NET_BILL = accessorTable4[i]["NET_BILL"].text
                                let ED_1 = accessorTable4[i]["ED_1"].text
                                if(ED_1 == nil){
                                    objCons.ED_1 = ""
                                }else{
                                    objCons.ED_1 = ED_1!
                                }
                                
                                if(EC_1 == nil){
                                    objCons.EC_1 = ""
                                }else{
                                    objCons.EC_1 = EC_1!
                                }
                                if(FIX_CH_1 == nil){
                                    objCons.FIX_CH_1 = ""
                                }else{
                                    objCons.FIX_CH_1 = FIX_CH_1!
                                }
                                if(FCA_1 == nil){
                                    objCons.FCA_1 = ""
                                }else{
                                    objCons.FCA_1 = FCA_1!
                                }
                                if(BP_NAME_QUOTES == nil){
                                    objCons.BP_NAME_QUOTES = ""
                                }else{
                                    objCons.BP_NAME_QUOTES = BP_NAME_QUOTES!
                                }
                                if(ADD1_QUOTES == nil){
                                    objCons.ADD1_QUOTES = ""
                                }else{
                                    objCons.ADD1_QUOTES = ADD1_QUOTES!
                                }
                                if(CESS_1 == nil){
                                    objCons.CESS_1 = ""
                                }else{
                                    objCons.CESS_1 = CESS_1!
                                }
                                if(ADD2_QUOTES == nil){
                                    objCons.ADD2_QUOTES = ""
                                }else{
                                    objCons.ADD2_QUOTES = ADD2_QUOTES!
                                }
                                if(MTR_1 == nil){
                                    objCons.MTR_1 = ""
                                }else{
                                    objCons.MTR_1 = MTR_1!
                                }
                                if(ADD3_QUOTES == nil){
                                    objCons.ADD3_QUOTES = ""
                                }else{
                                    objCons.ADD3_QUOTES = ADD3_QUOTES!
                                }
                                if(WTCS_1 == nil){
                                    objCons.WTCS_1 = ""
                                }else{
                                    objCons.WTCS_1 = WTCS_1!
                                }
                                if(DIVTXT_QUOTES == nil){
                                    objCons.DIVTXT_QUOTES = ""
                                }else{
                                    objCons.DIVTXT_QUOTES = DIVTXT_QUOTES!
                                }
                                if(ASD_1 == nil){
                                    objCons.ASD_1 = ""
                                }else{
                                    objCons.ASD_1 = ASD_1!
                                }
                                if(REB_1 == nil){
                                    objCons.REB_1 = ""
                                }else{
                                    objCons.REB_1 = REB_1!
                                }
                                if(ADJ_DMD_1 == nil){
                                    objCons.ADJ_DMD_1 = ""
                                }else{
                                    objCons.ADJ_DMD_1 = ADJ_DMD_1!
                                }
                                if(AVG_ADJ_1 == nil){
                                    objCons.AVG_ADJ_1 = ""
                                }else{
                                    objCons.AVG_ADJ_1 = AVG_ADJ_1!
                                }
                                if(MIS_1 == nil){
                                    objCons.MIS_1 = ""
                                }else{
                                    objCons.MIS_1 = MIS_1!
                                }
                                if(PRE_ARR_1 == nil){
                                    objCons.PRE_ARR_1 = ""
                                }else{
                                    objCons.PRE_ARR_1 = PRE_ARR_1!
                                }
                                if(TOT_BILL_1 == nil){
                                    objCons.TOT_BILL_1 = ""
                                }else{
                                    objCons.TOT_BILL_1 = TOT_BILL_1!
                                }
                                if(TOT_AMT_1 == nil){
                                    objCons.TOT_AMT_1 = ""
                                }else{
                                    objCons.TOT_AMT_1 = TOT_AMT_1!
                                }
                                if(AMT == nil){
                                    objCons.AMT = ""
                                }else{
                                    objCons.AMT = AMT!
                                }
                                if(SOLAR_RBT == nil){
                                    objCons.SOLAR_RBT = ""
                                }else{
                                    objCons.SOLAR_RBT = SOLAR_RBT!
                                }
                                if(SG400U_50PR == nil){
                                    objCons.SG400U_50PR = ""
                                }else{
                                    objCons.SG400U_50PR = SG400U_50PR!
                                }
                                if(NET_BILL == nil){
                                    objCons.NET_BILL = ""
                                }else{
                                    objCons.NET_BILL = NET_BILL!
                                }
                              
                                
                                
                                obj.electricBillBPDetailsDB.append(objCons)
                            }
                        }
                        if(countSetTable5 > 0){
                            for i in 0...countSetTable5 - 1  {
                                
                                let objCons = ElectricBillLNDetailsDisplay()
                                let TI1 = accessorTable5[i]["TI1"].text
                                let EN1 = accessorTable5[i]["EN1"].text
                                let PA1 = accessorTable5[i]["PA1"].text
                                let PD1 = accessorTable5[i]["PD1"].text
                                let OP1 = accessorTable5[i]["OP1"].text
                                let SL1 = accessorTable5[i]["SL1"].text
                                let LN1 = accessorTable5[i]["LN1"].text
                                
                               if(TI1 == nil){
                                   objCons.TI1 = ""
                               }else{
                                   objCons.TI1 = TI1!
                               }
                               if(EN1 == nil){
                                   objCons.EN1 = ""
                               }else{
                                   objCons.EN1 = EN1!
                               }
                               if(PA1 == nil){
                                   objCons.PA1 = ""
                               }else{
                                   objCons.PA1 = PA1!
                               }
                               if(PD1 == nil){
                                   objCons.PD1 = ""
                               }else{
                                   objCons.PD1 = PD1!
                               }
                               if(OP1 == nil){
                                   objCons.OP1 = ""
                               }else{
                                   objCons.OP1 = OP1!
                               }
                               if(SL1 == nil){
                                   objCons.SL1 = ""
                               }else{
                                   objCons.SL1 = SL1!
                               }
                               if(LN1 == nil){
                                   objCons.LN1 = ""
                               }else{
                                   objCons.LN1 = LN1!
                               }
                               
                               
                                
                                
                                obj.electricBillLNDetailsDB.append(objCons)
                            }
                        }
                        if(countSetTable7 > 0){
                            for i in 0...countSetTable7 - 1  {
                                
                                let objCons = ElectricOfficeDetailsDisplay()
                                let COKEY = accessorTable7[i]["COKEY"].text
                                let DCPHONE = accessorTable7[i]["DCPHONE"].text
                                let JEPHONE = accessorTable7[i]["JEPHONE"].text
                                let JENAME = accessorTable7[i]["JENAME"].text
                                let AENAME = accessorTable7[i]["AENAME"].text
                                let AEPHONE = accessorTable7[i]["AEPHONE"].text
                                if(COKEY == nil){
                                    objCons.COKEY = ""
                                }else{
                                    objCons.COKEY = COKEY!
                                }
                                if(DCPHONE == nil){
                                    objCons.DCPHONE = ""
                                }else{
                                    objCons.DCPHONE = DCPHONE!
                                }
                                if(JEPHONE == nil){
                                    objCons.JEPHONE = ""
                                }else{
                                    objCons.JEPHONE = JEPHONE!
                                }
                                if(JENAME == nil){
                                    objCons.JENAME = ""
                                }else{
                                    objCons.JENAME = JENAME!
                                }
                                if(AENAME == nil){
                                    objCons.AENAME = ""
                                }else{
                                    objCons.AENAME = AENAME!
                                }
                                if(AEPHONE == nil){
                                    objCons.AEPHONE = ""
                                }else{
                                    objCons.AEPHONE = AEPHONE!
                                }
                                
                                
                                obj.electricOfficeDetailsDB.append(objCons)
                            }
                        }
                      
                       
                        
                        if(countSet > 0){
                            for i in 0...5  {
                                
                                let objCons = ConsumptionDisplay()
                                let bm = accessor["BM" + String(i + 1)].text
                                let mr = accessor["MR" + String(i + 1)].text
                                let SM_CON = accessor["SM_CON" + String(i + 1)].text
                                let SM_MTR_STS = accessor["SM_MTR_STS" + String(i + 1)].text
                                if(bm == nil){
                                    objCons.BM = ""
                                }else{
                                    objCons.BM = bm!
                                }
                                if(mr == nil){
                                    objCons.MR = ""
                                }else{
                                    objCons.MR = mr!
                                }
                                if(SM_CON == nil){
                                    objCons.SM_CON = ""
                                }else{
                                    objCons.SM_CON = SM_CON!
                                }
                                if(SM_MTR_STS == nil){
                                    objCons.SM_MTR_STS = ""
                                }else{
                                    objCons.SM_MTR_STS = SM_MTR_STS!
                                }
                                
                                obj.consumptionDB.append(objCons)
                            }
                        }
                        if(countSetTable2 > 0){
                            for i in 0...countSetTable2 - 1  {
                                let objCons = ElectricBillDetailsDisplay()
                                let DOC_NO = accessorTable2[i]["DOC_NO"].text
                                let BILL_MNTH = accessorTable2[i]["BILL_MNTH"].text
                                let ISSUE_DT = accessorTable2[i]["ISSUE_DT"].text
                                let DUE_DT = accessorTable2[i]["DUE_DT"].text
                                let BP_FIRST = accessorTable2[i]["BP_FIRST"].text
                                let DUEDT_CHQ = accessorTable2[i]["DUEDT_CHQ"].text
                                if(DOC_NO == nil){
                                    objCons.DOC_NO = ""
                                }else{
                                    objCons.DOC_NO = DOC_NO!
                                }
                                if(BILL_MNTH == nil){
                                    objCons.BILL_MNTH = ""
                                }else{
                                    objCons.BILL_MNTH = BILL_MNTH!
                                }
                                if(ISSUE_DT == nil){
                                    objCons.ISSUE_DT = ""
                                }else{
                                    objCons.ISSUE_DT = ISSUE_DT!
                                }
                                if(DUE_DT == nil){
                                    objCons.DUE_DT = ""
                                }else{
                                    objCons.DUE_DT = DUE_DT!
                                }
                                if(DUEDT_CHQ == nil){
                                    objCons.DUEDT_CHQ = ""
                                }else{
                                    objCons.DUEDT_CHQ = DUEDT_CHQ!
                                }
                                if(BP_FIRST == nil){
                                    objCons.BP_FIRST = ""
                                }else{
                                    objCons.BP_FIRST = BP_FIRST!
                                }
                               
                                obj.electricBillDetailsDB.append(objCons)
                            }
                        }
                       
                        success(dataArray as AnyObject)

                      }

                    
                }
        else{

        }
        
        
    }
    
    
}



class ElectricityDetailsModelDataAPI
{
    
    var reachablty = try! Reachability()
    
    
    func serviceCalling( obj : ElectricityBillViewController ,bpNo : String, success:@escaping (AnyObject)-> Void)
    {
        
       // obj.startLoading()
        reachablty = try! Reachability()
        if(reachablty.connection != .unavailable)
        {
            if(obj.cntdata == 0){
            obj.startLoadingPK(view: obj.view, text: "please wait...")
            }
           
           
                 let dateFormatterInv = DateFormatter()
                 dateFormatterInv.timeZone = NSTimeZone.system
                 dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
            let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
              let invStrUnique = dateFormatterInv.string(from: Date())
              let request_no = invStrUnique + bpNo
              let message = "ZBAPI_BI_LTBILLDISP|\(bpNo)|||\(secID)|\(request_no)"
print(message)
                  let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
print(encode)


              AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_dataset", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
              // print("Request: \(response.value)")
               guard let respo = response.value else {
                obj.cntdata = obj.cntdata + 1
                if(obj.cntdata < 3){
                    obj.stopLoadingPK(view: obj.view)
                 obj.getBillDetailsData(bpno: obj.cellData.lblPartner.text!)
                }
                
               
                   return
               }
                
                print(response.value)
               let xml = try! XML.parse(respo)
              let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table6"]
                let accessorTable2 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table2"]
                let accessorTable1 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table1"]
                 let accessorTable3 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table3"]
                
                let accessorTable4 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table4"]
                let accessorTable5 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table5"]
                 let accessorTable7 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table7"]
                
                
                
                var dataArray : [ElectricMainDetailsDisplay] = []
                guard let countSetTable1 = accessorTable1.all?.count else {
                obj.cntdata = obj.cntdata + 1
                if(obj.cntdata < 3){
                    obj.stopLoadingPK(view: obj.view)
                 obj.getBillDetailsData(bpno: obj.cellData.lblPartner.text!)
                }
                 
                                return
                }
                guard let countSetTable2 = accessorTable2.all?.count else {
                 // obj.getBillDetailsData(bpno: bpNo)
                                return
                }
                guard let countSetTable3 = accessorTable3.all?.count else {
                 // obj.getBillDetailsData(bpno: bpNo)
                                return
                }
                guard let countSetTable4 = accessorTable4.all?.count else {
                 // obj.getBillDetailsData(bpno: bpNo)
                                return
                }
                guard let countSetTable5 = accessorTable5.all?.count else {
                 // obj.getBillDetailsData(bpno: bpNo)
                                return
                }
                guard let countSetTable7 = accessorTable7.all?.count else {
                 // obj.getBillDetailsData(bpno: bpNo)
                                return
                }
                guard let countSet = accessor.all?.count else {
                  // obj.getBillDetailsData(bpno: bpNo)
                                 return
                 }
                obj.cntdata = 0
                 obj.stopLoadingPK(view: obj.view)
                if(countSetTable1 > 0){
                    for i in 0...countSetTable1 - 1  {
                        
                         let objCons = ElectricMainDetailsDisplay()
                         let RAOTXT = accessorTable1[i]["RAOTXT"].text
                         let DIVTXT = accessorTable1[i]["DIVTXT"].text
                         let BP_NO = accessorTable1[i]["BP_NO"].text
                         let BP_NAME = accessorTable1[i]["BP_NAME"].text
                         let ADD1 = accessorTable1[i]["ADD1"].text
                         let ADD2 = accessorTable1[i]["ADD2"].text
                         let ADD3 = accessorTable1[i]["ADD3"].text
                         let RT_CT = accessorTable1[i]["RT_CT"].text
                         let SD_1 = accessorTable1[i]["SD_1"].text
                         let TEA = accessorTable1[i]["TEA"].text
                         let SD_INT = accessorTable1[i]["SD_INT"].text
                         let TEL_NUMBER = accessorTable1[i]["TEL_NUMBER"].text
                        if(RAOTXT == nil){
                            objCons.RAOTXT = ""
                        }else{
                            objCons.RAOTXT = RAOTXT!
                        }
                        if(DIVTXT == nil){
                            objCons.DIVTXT = ""
                        }else{
                            objCons.DIVTXT = DIVTXT!
                        }
                        if(BP_NAME == nil){
                            objCons.BP_NAME = ""
                        }else{
                            objCons.BP_NAME = BP_NAME!
                        }
                        if(BP_NO == nil){
                            objCons.BP_NO = ""
                        }else{
                            objCons.BP_NO = BP_NO!
                        }
                        if(ADD1 == nil){
                            objCons.ADD1 = ""
                        }else{
                            objCons.ADD1 = ADD1!
                        }
                        if(ADD2 == nil){
                            objCons.ADD2 = ""
                        }else{
                            objCons.ADD2 = ADD2!
                        }
                        if(ADD3 == nil){
                            objCons.ADD3 = ""
                        }else{
                            objCons.ADD3 = ADD3!
                        }
                        if(RT_CT == nil){
                            objCons.RT_CT = ""
                        }else{
                            objCons.RT_CT = RT_CT!
                        }
                        if(SD_1 == nil){
                            objCons.SD_1 = ""
                        }else{
                            objCons.SD_1 = SD_1!
                        }
                        if(TEA == nil){
                            objCons.TEA = ""
                        }else{
                            objCons.TEA = TEA!
                        }
                        if(SD_INT == nil){
                            objCons.SD_INT = ""
                        }else{
                            objCons.SD_INT = SD_INT!
                        }
                        if(TEL_NUMBER == nil){
                            objCons.TEL_NUMBER = ""
                        }else{
                            objCons.TEL_NUMBER = TEL_NUMBER!
                        }
                       
                        dataArray.append(objCons)
                    }
                }
               
                if(countSetTable3 > 0){
                    for i in 0...countSetTable3 - 1  {
                        let objCons = ElectricBillUnitDetailsDisplay()
                        let CON_LD = accessorTable3[i]["CON_LD"].text
                        let CON_LOAD = accessorTable3[i]["CON_LOAD"].text
                        let CR_RD = accessorTable3[i]["CR_RD"].text
                        let PR_RD = accessorTable3[i]["PR_RD"].text
                        let CONS = accessorTable3[i]["CONS"].text
                        let MD = accessorTable3[i]["MD"].text
                        
                        let KWH_ON = accessorTable3[i]["KWH_ON"].text
                        let PF = accessorTable3[i]["PF"].text
                        let CONS_TEXT = accessorTable3[i]["CONS_TEXT"].text
                        let SOLAR_UNIT = accessorTable3[i]["SOLAR_UNIT"].text
                        let BILLING_TYPE = accessorTable3[i]["BILLING_TYPE"].text
                        let PURPOSE = accessorTable3[i]["PURPOSE"].text
                        let MF = accessorTable3[i]["MF"].text
                        let ASSD_UNIT = accessorTable3[i]["ASSD_UNIT"].text
                        let TOT_UNIT = accessorTable3[i]["TOT_UNIT"].text
                        let AVRG_QTY = accessorTable3[i]["AVRG_QTY"].text
                        if(CON_LD == nil){
                            objCons.CON_LD = ""
                        }else{
                            objCons.CON_LD = CON_LD!
                        }
                        if(CON_LOAD == nil){
                            objCons.CON_LOAD = ""
                        }else{
                            objCons.CON_LOAD = CON_LOAD!
                        }
                        if(CR_RD == nil){
                            objCons.CR_RD = ""
                        }else{
                            objCons.CR_RD = CR_RD!
                        }
                        if(PR_RD == nil){
                            objCons.PR_RD = ""
                        }else{
                            objCons.PR_RD = PR_RD!
                        }
                        if(CONS == nil){
                            objCons.CONS = ""
                        }else{
                            objCons.CONS = CONS!
                        }
                        if(MD == nil){
                            objCons.MD = ""
                        }else{
                            objCons.MD = MD!
                        }
                        if(KWH_ON == nil){
                            objCons.KWH_ON = ""
                        }else{
                            objCons.KWH_ON = KWH_ON!
                        }
                        if(PF == nil){
                            objCons.PF = ""
                        }else{
                            objCons.PF = PF!
                        }
                        if(CONS_TEXT == nil){
                            objCons.CONS_TEXT = ""
                        }else{
                            objCons.CONS_TEXT = CONS_TEXT!
                        }
                        if(SOLAR_UNIT == nil){
                            objCons.SOLAR_UNIT = ""
                        }else{
                            objCons.SOLAR_UNIT = SOLAR_UNIT!
                        }
                        if(BILLING_TYPE == nil){
                            objCons.BILLING_TYPE = ""
                        }else{
                            objCons.BILLING_TYPE = BILLING_TYPE!
                        }
                        if(PURPOSE == nil){
                            objCons.PURPOSE = ""
                        }else{
                            objCons.PURPOSE = PURPOSE!
                        }
                        if(MF == nil){
                            objCons.MF = ""
                        }else{
                            objCons.MF = MF!
                        }
                        if(ASSD_UNIT == nil){
                            objCons.ASSD_UNIT = ""
                        }else{
                            objCons.ASSD_UNIT = ASSD_UNIT!
                        }
                        if(TOT_UNIT == nil){
                            objCons.TOT_UNIT = ""
                        }else{
                            objCons.TOT_UNIT = TOT_UNIT!
                        }
                        if(AVRG_QTY == nil){
                            objCons.AVRG_QTY = ""
                        }else{
                            objCons.AVRG_QTY = AVRG_QTY!
                        }
                     
                        obj.electricUnitsDetailsDB.append(objCons)
                    }
                }
               if(countSetTable4 > 0){
                    for i in 0...countSetTable4 - 1  {
                        
                        let objCons = ElectricBillBPDataDetailsDisplay()
                        let EC_1 = accessorTable4[i]["EC_1"].text
                        let FIX_CH_1 = accessorTable4[i]["FIX_CH_1"].text
                        let FCA_1 = accessorTable4[i]["FCA_1"].text
                        let BP_NAME_QUOTES = accessorTable4[i]["BP_NAME_QUOTES"].text
                        let ADD1_QUOTES = accessorTable4[i]["ADD1_QUOTES"].text
                        let CESS_1 = accessorTable4[i]["CESS_1"].text
                        let ADD2_QUOTES = accessorTable4[i]["ADD2_QUOTES"].text
                        let MTR_1 = accessorTable4[i]["MTR_1"].text
                        let ADD3_QUOTES = accessorTable4[i]["ADD3_QUOTES"].text
                        let WTCS_1 = accessorTable4[i]["WTCS_1"].text
                        let DIVTXT_QUOTES = accessorTable4[i]["DIVTXT_QUOTES"].text
                        let ASD_1 = accessorTable4[i]["ASD_1"].text
                        let REB_1 = accessorTable4[i]["REB_1"].text
                        let ADJ_DMD_1 = accessorTable4[i]["ADJ_DMD_1"].text
                        let AVG_ADJ_1 = accessorTable4[i]["AVG_ADJ_1"].text
                        let MIS_1 = accessorTable4[i]["MIS_1"].text
                        let PRE_ARR_1 = accessorTable4[i]["PRE_ARR_1"].text
                        let TOT_BILL_1 = accessorTable4[i]["TOT_BILL_1"].text
                        let TOT_AMT_1 = accessorTable4[i]["TOT_AMT_1"].text
                        let AMT = accessorTable4[i]["AMT"].text
                        let SOLAR_RBT = accessorTable4[i]["SOLAR_RBT"].text
                        let SG400U_50PR = accessorTable4[i]["SG400U_50PR"].text
                        let NET_BILL = accessorTable4[i]["NET_BILL"].text
                        let ED_1 = accessorTable4[i]["ED_1"].text
                        if(ED_1 == nil){
                            objCons.ED_1 = ""
                        }else{
                            objCons.ED_1 = ED_1!
                        }
                        
                        if(EC_1 == nil){
                            objCons.EC_1 = ""
                        }else{
                            objCons.EC_1 = EC_1!
                        }
                        if(FIX_CH_1 == nil){
                            objCons.FIX_CH_1 = ""
                        }else{
                            objCons.FIX_CH_1 = FIX_CH_1!
                        }
                        if(FCA_1 == nil){
                            objCons.FCA_1 = ""
                        }else{
                            objCons.FCA_1 = FCA_1!
                        }
                        if(BP_NAME_QUOTES == nil){
                            objCons.BP_NAME_QUOTES = ""
                        }else{
                            objCons.BP_NAME_QUOTES = BP_NAME_QUOTES!
                        }
                        if(ADD1_QUOTES == nil){
                            objCons.ADD1_QUOTES = ""
                        }else{
                            objCons.ADD1_QUOTES = ADD1_QUOTES!
                        }
                        if(CESS_1 == nil){
                            objCons.CESS_1 = ""
                        }else{
                            objCons.CESS_1 = CESS_1!
                        }
                        if(ADD2_QUOTES == nil){
                            objCons.ADD2_QUOTES = ""
                        }else{
                            objCons.ADD2_QUOTES = ADD2_QUOTES!
                        }
                        if(MTR_1 == nil){
                            objCons.MTR_1 = ""
                        }else{
                            objCons.MTR_1 = MTR_1!
                        }
                        if(ADD3_QUOTES == nil){
                            objCons.ADD3_QUOTES = ""
                        }else{
                            objCons.ADD3_QUOTES = ADD3_QUOTES!
                        }
                        if(WTCS_1 == nil){
                            objCons.WTCS_1 = ""
                        }else{
                            objCons.WTCS_1 = WTCS_1!
                        }
                        if(DIVTXT_QUOTES == nil){
                            objCons.DIVTXT_QUOTES = ""
                        }else{
                            objCons.DIVTXT_QUOTES = DIVTXT_QUOTES!
                        }
                        if(ASD_1 == nil){
                            objCons.ASD_1 = ""
                        }else{
                            objCons.ASD_1 = ASD_1!
                        }
                        if(REB_1 == nil){
                            objCons.REB_1 = ""
                        }else{
                            objCons.REB_1 = REB_1!
                        }
                        if(ADJ_DMD_1 == nil){
                            objCons.ADJ_DMD_1 = ""
                        }else{
                            objCons.ADJ_DMD_1 = ADJ_DMD_1!
                        }
                        if(AVG_ADJ_1 == nil){
                            objCons.AVG_ADJ_1 = ""
                        }else{
                            objCons.AVG_ADJ_1 = AVG_ADJ_1!
                        }
                        if(MIS_1 == nil){
                            objCons.MIS_1 = ""
                        }else{
                            objCons.MIS_1 = MIS_1!
                        }
                        if(PRE_ARR_1 == nil){
                            objCons.PRE_ARR_1 = ""
                        }else{
                            objCons.PRE_ARR_1 = PRE_ARR_1!
                        }
                        if(TOT_BILL_1 == nil){
                            objCons.TOT_BILL_1 = ""
                        }else{
                            objCons.TOT_BILL_1 = TOT_BILL_1!
                        }
                        if(TOT_AMT_1 == nil){
                            objCons.TOT_AMT_1 = ""
                        }else{
                            objCons.TOT_AMT_1 = TOT_AMT_1!
                        }
                        if(AMT == nil){
                            objCons.AMT = ""
                        }else{
                            objCons.AMT = AMT!
                        }
                        if(SOLAR_RBT == nil){
                            objCons.SOLAR_RBT = ""
                        }else{
                            objCons.SOLAR_RBT = SOLAR_RBT!
                        }
                        if(SG400U_50PR == nil){
                            objCons.SG400U_50PR = ""
                        }else{
                            objCons.SG400U_50PR = SG400U_50PR!
                        }
                        if(NET_BILL == nil){
                            objCons.NET_BILL = ""
                        }else{
                            objCons.NET_BILL = NET_BILL!
                        }
                      
                        
                        
                        obj.electricBillBPDetailsDB.append(objCons)
                    }
                }
                if(countSetTable5 > 0){
                    for i in 0...countSetTable5 - 1  {
                        
                        let objCons = ElectricBillLNDetailsDisplay()
                        let TI1 = accessorTable5[i]["TI1"].text
                        let EN1 = accessorTable5[i]["EN1"].text
                        let PA1 = accessorTable5[i]["PA1"].text
                        let PD1 = accessorTable5[i]["PD1"].text
                        let OP1 = accessorTable5[i]["OP1"].text
                        let SL1 = accessorTable5[i]["SL1"].text
                        let LN1 = accessorTable5[i]["LN1"].text
                        
                       if(TI1 == nil){
                           objCons.TI1 = ""
                       }else{
                           objCons.TI1 = TI1!
                       }
                       if(EN1 == nil){
                           objCons.EN1 = ""
                       }else{
                           objCons.EN1 = EN1!
                       }
                       if(PA1 == nil){
                           objCons.PA1 = ""
                       }else{
                           objCons.PA1 = PA1!
                       }
                       if(PD1 == nil){
                           objCons.PD1 = ""
                       }else{
                           objCons.PD1 = PD1!
                       }
                       if(OP1 == nil){
                           objCons.OP1 = ""
                       }else{
                           objCons.OP1 = OP1!
                       }
                       if(SL1 == nil){
                           objCons.SL1 = ""
                       }else{
                           objCons.SL1 = SL1!
                       }
                       if(LN1 == nil){
                           objCons.LN1 = ""
                       }else{
                           objCons.LN1 = LN1!
                       }
                       
                       
                        
                        
                        obj.electricBillLNDetailsDB.append(objCons)
                    }
                }
                if(countSetTable7 > 0){
                    for i in 0...countSetTable7 - 1  {
                        
                        let objCons = ElectricOfficeDetailsDisplay()
                        let COKEY = accessorTable7[i]["COKEY"].text
                        let DCPHONE = accessorTable7[i]["DCPHONE"].text
                        let JEPHONE = accessorTable7[i]["JEPHONE"].text
                        let JENAME = accessorTable7[i]["JENAME"].text
                        let AENAME = accessorTable7[i]["AENAME"].text
                        let AEPHONE = accessorTable7[i]["AEPHONE"].text
                        if(COKEY == nil){
                            objCons.COKEY = ""
                        }else{
                            objCons.COKEY = COKEY!
                        }
                        if(DCPHONE == nil){
                            objCons.DCPHONE = ""
                        }else{
                            objCons.DCPHONE = DCPHONE!
                        }
                        if(JEPHONE == nil){
                            objCons.JEPHONE = ""
                        }else{
                            objCons.JEPHONE = JEPHONE!
                        }
                        if(JENAME == nil){
                            objCons.JENAME = ""
                        }else{
                            objCons.JENAME = JENAME!
                        }
                        if(AENAME == nil){
                            objCons.AENAME = ""
                        }else{
                            objCons.AENAME = AENAME!
                        }
                        if(AEPHONE == nil){
                            objCons.AEPHONE = ""
                        }else{
                            objCons.AEPHONE = AEPHONE!
                        }
                        
                        
                        obj.electricOfficeDetailsDB.append(objCons)
                    }
                }
              
               
                
                if(countSet > 0){
                    for i in 0...5  {
                        
                        let objCons = ConsumptionDisplay()
                        let bm = accessor["BM" + String(i + 1)].text
                        let mr = accessor["MR" + String(i + 1)].text
                        let SM_CON = accessor["SM_CON" + String(i + 1)].text
                        let SM_MTR_STS = accessor["SM_MTR_STS" + String(i + 1)].text
                        if(bm == nil){
                            objCons.BM = ""
                        }else{
                            objCons.BM = bm!
                        }
                        if(mr == nil){
                            objCons.MR = ""
                        }else{
                            objCons.MR = mr!
                        }
                        if(SM_CON == nil){
                            objCons.SM_CON = ""
                        }else{
                            objCons.SM_CON = SM_CON!
                        }
                        if(SM_MTR_STS == nil){
                            objCons.SM_MTR_STS = ""
                        }else{
                            objCons.SM_MTR_STS = SM_MTR_STS!
                        }
                        
                        obj.consumptionDB.append(objCons)
                    }
                }
                if(countSetTable2 > 0){
                    for i in 0...countSetTable2 - 1  {
                        let objCons = ElectricBillDetailsDisplay()
                        let DOC_NO = accessorTable2[i]["DOC_NO"].text
                        let BILL_MNTH = accessorTable2[i]["BILL_MNTH"].text
                        let ISSUE_DT = accessorTable2[i]["ISSUE_DT"].text
                        let DUE_DT = accessorTable2[i]["DUE_DT"].text
                        let BP_FIRST = accessorTable2[i]["BP_FIRST"].text
                        let DUEDT_CHQ = accessorTable2[i]["DUEDT_CHQ"].text
                        if(DOC_NO == nil){
                            objCons.DOC_NO = ""
                        }else{
                            objCons.DOC_NO = DOC_NO!
                        }
                        if(BILL_MNTH == nil){
                            objCons.BILL_MNTH = ""
                        }else{
                            objCons.BILL_MNTH = BILL_MNTH!
                        }
                        if(ISSUE_DT == nil){
                            objCons.ISSUE_DT = ""
                        }else{
                            objCons.ISSUE_DT = ISSUE_DT!
                        }
                        if(DUE_DT == nil){
                            objCons.DUE_DT = ""
                        }else{
                            objCons.DUE_DT = DUE_DT!
                        }
                        if(DUEDT_CHQ == nil){
                            objCons.DUEDT_CHQ = ""
                        }else{
                            objCons.DUEDT_CHQ = DUEDT_CHQ!
                        }
                        if(BP_FIRST == nil){
                            objCons.BP_FIRST = ""
                        }else{
                            objCons.BP_FIRST = BP_FIRST!
                        }
                       
                        obj.electricBillDetailsDB.append(objCons)
                    }
                }
               
                success(dataArray as AnyObject)

              }

            
        }
        else{

        }
        
        
    }
    
    
}

class ShowOnLoadModelDataAPI
{
    
    var reachablty = try! Reachability()
    
    
    func serviceCalling( success:@escaping (AnyObject)-> Void)
    {
        
       // obj.startLoading()
        reachablty = try! Reachability()
        if(reachablty.connection != .unavailable)
        {
           
          
           
                 let dateFormatterInv = DateFormatter()
                 dateFormatterInv.timeZone = NSTimeZone.system
                 dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
          //  let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
              let invStrUnique = dateFormatterInv.string(from: Date())
              let request_no = invStrUnique
              let message = "MA_getdowntimemsg|\(secID)"
print(message)
                  let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
print(encode)


              AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_datatable", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
              // print("Request: \(response.value)")
               guard let respo = response.value else {
                   return
               }
                print(response.value)
                
               let xml = try! XML.parse(respo)
                let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_datatableResponse"]["MA_cons_datatableResult"]["diffgr:diffgram"]["DocumentElement"]["MA_getdowntimemsg"]["DISP_FLG"]
               
                guard let countSet = accessor.text else {
                   
                                 return
                 }
                if(countSet == "true"){
                    let DISP_START = xml["soap:Envelope"]["soap:Body"]["MA_cons_datatableResponse"]["MA_cons_datatableResult"]["diffgr:diffgram"]["DocumentElement"]["MA_getdowntimemsg"]["DISP_START"]
                    let DISP_END = xml["soap:Envelope"]["soap:Body"]["MA_cons_datatableResponse"]["MA_cons_datatableResult"]["diffgr:diffgram"]["DocumentElement"]["MA_getdowntimemsg"]["DISP_END"]
                    let DOWNTIME_MSG_ENG = xml["soap:Envelope"]["soap:Body"]["MA_cons_datatableResponse"]["MA_cons_datatableResult"]["diffgr:diffgram"]["DocumentElement"]["MA_getdowntimemsg"]["DOWNTIME_MSG_ENG"]
                    let DOWNTIME_MSG_HND = xml["soap:Envelope"]["soap:Body"]["MA_cons_datatableResponse"]["MA_cons_datatableResult"]["diffgr:diffgram"]["DocumentElement"]["MA_getdowntimemsg"]["DOWNTIME_MSG_HND"]
                    
                    ShowMsg.startDate = DISP_START.text!
                    ShowMsg.endDate = DISP_END.text!
                    if(DOWNTIME_MSG_ENG.text! != "" && DOWNTIME_MSG_ENG.text! != "-"){
                        ShowMsg.messageEng = DOWNTIME_MSG_ENG.text!
                    }
                    if(DOWNTIME_MSG_HND.text! != "" && DOWNTIME_MSG_HND.text! != "-"){
                        ShowMsg.messageHin = DOWNTIME_MSG_HND.text!
                    }
                   
                    
                    
                    NotificationCenter.default.post(name:NSNotification.Name.init("ShowPop"), object: nil)
                    
                }
               
                //success(dataArray as AnyObject)

              }

            
        }
        else{

        }
        
        
    }
    
    
}
struct ShowMsg {
    
    static var messageHin = String()
    static var messageEng = String()
    static var endDate = String()
    static var startDate = String()
   
}

class BillPaymentModelDataAPI
{
    
    var reachablty = try! Reachability()
    
    
    func serviceCalling( obj : BillPaymentViewController ,bpNo : String, success:@escaping (AnyObject)-> Void)
    {
        
       // obj.startLoading()
        reachablty = try! Reachability()
        if(reachablty.connection != .unavailable)
        {
            if(obj.cntdata == 0){
          obj.startLoadingPK(view: obj.view, text: "Please Wait ....")
            }
           
                 let dateFormatterInv = DateFormatter()
                 dateFormatterInv.timeZone = NSTimeZone.system
                 dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
            let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
              let invStrUnique = dateFormatterInv.string(from: Date())
              let request_no = invStrUnique + bpNo
              let message = "ZISU_RFCBILL_PAYTM|\(bpNo)|\(secID)|\(request_no)"
print(message)
                  let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
print(encode)


              AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_arraystring", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
              // print("Request: \(response.value)")
               guard let respo = response.value else {
                   return
               }
                print(response.value)
                obj.stopLoadingPK(view: obj.view)
               let xml = try! XML.parse(respo)
              let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_arraystringResponse"]["MA_cons_arraystringResult"]
                var dataArray : [BillDisplay] = []
                guard let countSet = accessor.all?.count else {
                 
                    
                    obj.cntdata = obj.cntdata + 1
                    if(obj.cntdata < 3){
                        obj.stopLoadingPK(view: obj.view)
                     obj.getPaymentBillDetailsData(bpno: obj.cellData.lblPartner.text!)
                    }
                    
                                 return
                 }
                obj.cntdata = 0
                 obj.stopLoadingPK(view: obj.view)
                if(countSet > 0){
                    for i in 0...countSet - 1  {
                        let child = accessor.all![i]
                        let obj = BillDisplay()
                        obj.name = child.childElements[1].text!
                        obj.mobile = child.childElements[7].text!
                        obj.billno = child.childElements[4].text!
                        obj.amount = child.childElements[5].text!
                        obj.lastDate = child.childElements[6].text!
                        obj.lastMon = child.childElements[8].text!
                         obj.address = child.childElements[3].text!
                        dataArray.append(obj)
                    }
                }
               
                success(dataArray as AnyObject)

              }

            
        }
        else{

        }
        
        
    }
    
    
}
class SendMeterModelDataAPI
{
    
    var reachablty = try! Reachability()
    
    
    func serviceCalling( obj : SendMeterReadingViewController ,bpNo : String, success:@escaping (AnyObject)-> Void)
    {
        
       // obj.startLoading()
        reachablty = try! Reachability()
        if(reachablty.connection != .unavailable)
        {
            if(obj.cntdata == 0){
            obj.startLoadingPK(view: obj.view, text: "please wait...")
            }
           
                 let dateFormatterInv = DateFormatter()
                 dateFormatterInv.timeZone = NSTimeZone.system
                 dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
            let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
              let invStrUnique = dateFormatterInv.string(from: Date())
              let request_no = invStrUnique + bpNo
              let message = "ZBAPI_BI_LTBILLDISP|\(bpNo)|||\(secID)|\(request_no)"
print(message)
                  let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
print(encode)


              AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_dataset", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
              // print("Request: \(response.value)")
               guard let respo = response.value else {
               
                
                
                obj.cntdata = obj.cntdata + 1
                if(obj.cntdata < 3){
                    obj.stopLoadingPK(view: obj.view)
                 obj.getPaymentBillDetailsData(bpno: obj.cellData.lblPartner.text!)
                }
                   return
               }
                obj.stopLoadingPK(view: obj.view)
                print(response.value)
               let xml = try! XML.parse(respo)
              let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table6"]
                let accessorTable2 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table2"]
                let accessorTable1 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table1"]
                 let accessorTable3 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table3"]
                
                let accessorTable4 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table4"]
                let accessorTable5 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table5"]
                 let accessorTable7 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table7"]
                
                
                
                var dataArray : [ElectricMainDetailsDisplay] = []
                guard let countSetTable1 = accessorTable1.all?.count else {
                obj.cntdata = obj.cntdata + 1
                 if(obj.cntdata < 3){
                     obj.stopLoadingPK(view: obj.view)
                  obj.getPaymentBillDetailsData(bpno: obj.cellData.lblPartner.text!)
                 }
                                return
                }
                guard let countSetTable2 = accessorTable2.all?.count else {
                 // obj.getBillDetailsData(bpno: bpNo)
                                return
                }
                guard let countSetTable3 = accessorTable3.all?.count else {
                 // obj.getBillDetailsData(bpno: bpNo)
                                return
                }
                guard let countSetTable4 = accessorTable4.all?.count else {
                 // obj.getBillDetailsData(bpno: bpNo)
                                return
                }
                guard let countSetTable5 = accessorTable5.all?.count else {
                 // obj.getBillDetailsData(bpno: bpNo)
                                return
                }
                guard let countSetTable7 = accessorTable7.all?.count else {
                 // obj.getBillDetailsData(bpno: bpNo)
                                return
                }
                guard let countSet = accessor.all?.count else {
                  // obj.getBillDetailsData(bpno: bpNo)
                                 return
                 }
                obj.cntdata = 0
                obj.stopLoadingPK(view: obj.view)
                if(countSetTable1 > 0){
                    for i in 0...countSetTable1 - 1  {
                        
                         let objCons = ElectricMainDetailsDisplay()
                         let RAOTXT = accessorTable1[i]["RAOTXT"].text
                         let DIVTXT = accessorTable1[i]["DIVTXT"].text
                         let BP_NO = accessorTable1[i]["BP_NO"].text
                         let BP_NAME = accessorTable1[i]["BP_NAME"].text
                         let ADD1 = accessorTable1[i]["ADD1"].text
                         let ADD2 = accessorTable1[i]["ADD2"].text
                         let ADD3 = accessorTable1[i]["ADD3"].text
                         let RT_CT = accessorTable1[i]["RT_CT"].text
                         let SD_1 = accessorTable1[i]["SD_1"].text
                         let TEA = accessorTable1[i]["TEA"].text
                         let SD_INT = accessorTable1[i]["SD_INT"].text
                         let TEL_NUMBER = accessorTable1[i]["TEL_NUMBER"].text
                        if(RAOTXT == nil){
                            objCons.RAOTXT = ""
                        }else{
                            objCons.RAOTXT = RAOTXT!
                        }
                        if(DIVTXT == nil){
                            objCons.DIVTXT = ""
                        }else{
                            objCons.DIVTXT = DIVTXT!
                        }
                        if(BP_NAME == nil){
                            objCons.BP_NAME = ""
                        }else{
                            objCons.BP_NAME = BP_NAME!
                        }
                        if(BP_NO == nil){
                            objCons.BP_NO = ""
                        }else{
                            objCons.BP_NO = BP_NO!
                        }
                        if(ADD1 == nil){
                            objCons.ADD1 = ""
                        }else{
                            objCons.ADD1 = ADD1!
                        }
                        if(ADD2 == nil){
                            objCons.ADD2 = ""
                        }else{
                            objCons.ADD2 = ADD2!
                        }
                        if(ADD3 == nil){
                            objCons.ADD3 = ""
                        }else{
                            objCons.ADD3 = ADD3!
                        }
                        if(RT_CT == nil){
                            objCons.RT_CT = ""
                        }else{
                            objCons.RT_CT = RT_CT!
                        }
                        if(SD_1 == nil){
                            objCons.SD_1 = ""
                        }else{
                            objCons.SD_1 = SD_1!
                        }
                        if(TEA == nil){
                            objCons.TEA = ""
                        }else{
                            objCons.TEA = TEA!
                        }
                        if(SD_INT == nil){
                            objCons.SD_INT = ""
                        }else{
                            objCons.SD_INT = SD_INT!
                        }
                        if(TEL_NUMBER == nil){
                            objCons.TEL_NUMBER = ""
                        }else{
                            objCons.TEL_NUMBER = TEL_NUMBER!
                        }
                       
                        dataArray.append(objCons)
                    }
                }
               
                if(countSetTable3 > 0){
                    for i in 0...countSetTable3 - 1  {
                        let objCons = ElectricBillUnitDetailsDisplay()
                        let CON_LD = accessorTable3[i]["CON_LD"].text
                        let CON_LOAD = accessorTable3[i]["CON_LOAD"].text
                        let CR_RD = accessorTable3[i]["CR_RD"].text
                        let PR_RD = accessorTable3[i]["PR_RD"].text
                        let CONS = accessorTable3[i]["CONS"].text
                        let MD = accessorTable3[i]["MD"].text
                        
                        let KWH_ON = accessorTable3[i]["KWH_ON"].text
                        let PF = accessorTable3[i]["PF"].text
                        let CONS_TEXT = accessorTable3[i]["CONS_TEXT"].text
                        let SOLAR_UNIT = accessorTable3[i]["SOLAR_UNIT"].text
                        let BILLING_TYPE = accessorTable3[i]["BILLING_TYPE"].text
                        let PURPOSE = accessorTable3[i]["PURPOSE"].text
                        let MF = accessorTable3[i]["MF"].text
                        let ASSD_UNIT = accessorTable3[i]["ASSD_UNIT"].text
                        let TOT_UNIT = accessorTable3[i]["TOT_UNIT"].text
                        let AVRG_QTY = accessorTable3[i]["AVRG_QTY"].text
                        if(CON_LD == nil){
                            objCons.CON_LD = ""
                        }else{
                            objCons.CON_LD = CON_LD!
                        }
                        if(CON_LOAD == nil){
                            objCons.CON_LOAD = ""
                        }else{
                            objCons.CON_LOAD = CON_LOAD!
                        }
                        if(CR_RD == nil){
                            objCons.CR_RD = ""
                        }else{
                            objCons.CR_RD = CR_RD!
                        }
                        if(PR_RD == nil){
                            objCons.PR_RD = ""
                        }else{
                            objCons.PR_RD = PR_RD!
                        }
                        if(CONS == nil){
                            objCons.CONS = ""
                        }else{
                            objCons.CONS = CONS!
                        }
                        if(MD == nil){
                            objCons.MD = ""
                        }else{
                            objCons.MD = MD!
                        }
                        if(KWH_ON == nil){
                            objCons.KWH_ON = ""
                        }else{
                            objCons.KWH_ON = KWH_ON!
                        }
                        if(PF == nil){
                            objCons.PF = ""
                        }else{
                            objCons.PF = PF!
                        }
                        if(CONS_TEXT == nil){
                            objCons.CONS_TEXT = ""
                        }else{
                            objCons.CONS_TEXT = CONS_TEXT!
                        }
                        if(SOLAR_UNIT == nil){
                            objCons.SOLAR_UNIT = ""
                        }else{
                            objCons.SOLAR_UNIT = SOLAR_UNIT!
                        }
                        if(BILLING_TYPE == nil){
                            objCons.BILLING_TYPE = ""
                        }else{
                            objCons.BILLING_TYPE = BILLING_TYPE!
                        }
                        if(PURPOSE == nil){
                            objCons.PURPOSE = ""
                        }else{
                            objCons.PURPOSE = PURPOSE!
                        }
                        if(MF == nil){
                            objCons.MF = ""
                        }else{
                            objCons.MF = MF!
                        }
                        if(ASSD_UNIT == nil){
                            objCons.ASSD_UNIT = ""
                        }else{
                            objCons.ASSD_UNIT = ASSD_UNIT!
                        }
                        if(TOT_UNIT == nil){
                            objCons.TOT_UNIT = ""
                        }else{
                            objCons.TOT_UNIT = TOT_UNIT!
                        }
                        if(AVRG_QTY == nil){
                            objCons.AVRG_QTY = ""
                        }else{
                            objCons.AVRG_QTY = AVRG_QTY!
                        }
                     
                        obj.electricUnitsDetailsDB.append(objCons)
                    }
                }
               if(countSetTable4 > 0){
                    for i in 0...countSetTable4 - 1  {
                        
                        let objCons = ElectricBillBPDataDetailsDisplay()
                        let EC_1 = accessorTable4[i]["EC_1"].text
                        let FIX_CH_1 = accessorTable4[i]["FIX_CH_1"].text
                        let FCA_1 = accessorTable4[i]["FCA_1"].text
                        let BP_NAME_QUOTES = accessorTable4[i]["BP_NAME_QUOTES"].text
                        let ADD1_QUOTES = accessorTable4[i]["ADD1_QUOTES"].text
                        let CESS_1 = accessorTable4[i]["CESS_1"].text
                        let ADD2_QUOTES = accessorTable4[i]["ADD2_QUOTES"].text
                        let MTR_1 = accessorTable4[i]["MTR_1"].text
                        let ADD3_QUOTES = accessorTable4[i]["ADD3_QUOTES"].text
                        let WTCS_1 = accessorTable4[i]["WTCS_1"].text
                        let DIVTXT_QUOTES = accessorTable4[i]["DIVTXT_QUOTES"].text
                        let ASD_1 = accessorTable4[i]["ASD_1"].text
                        let REB_1 = accessorTable4[i]["REB_1"].text
                        let ADJ_DMD_1 = accessorTable4[i]["ADJ_DMD_1"].text
                        let AVG_ADJ_1 = accessorTable4[i]["AVG_ADJ_1"].text
                        let MIS_1 = accessorTable4[i]["MIS_1"].text
                        let PRE_ARR_1 = accessorTable4[i]["PRE_ARR_1"].text
                        let TOT_BILL_1 = accessorTable4[i]["TOT_BILL_1"].text
                        let TOT_AMT_1 = accessorTable4[i]["TOT_AMT_1"].text
                        let AMT = accessorTable4[i]["AMT"].text
                        let SOLAR_RBT = accessorTable4[i]["SOLAR_RBT"].text
                        let SG400U_50PR = accessorTable4[i]["SG400U_50PR"].text
                        let NET_BILL = accessorTable4[i]["NET_BILL"].text
                        let ED_1 = accessorTable4[i]["ED_1"].text
                        if(ED_1 == nil){
                            objCons.ED_1 = ""
                        }else{
                            objCons.ED_1 = ED_1!
                        }
                        
                        if(EC_1 == nil){
                            objCons.EC_1 = ""
                        }else{
                            objCons.EC_1 = EC_1!
                        }
                        if(FIX_CH_1 == nil){
                            objCons.FIX_CH_1 = ""
                        }else{
                            objCons.FIX_CH_1 = FIX_CH_1!
                        }
                        if(FCA_1 == nil){
                            objCons.FCA_1 = ""
                        }else{
                            objCons.FCA_1 = FCA_1!
                        }
                        if(BP_NAME_QUOTES == nil){
                            objCons.BP_NAME_QUOTES = ""
                        }else{
                            objCons.BP_NAME_QUOTES = BP_NAME_QUOTES!
                        }
                        if(ADD1_QUOTES == nil){
                            objCons.ADD1_QUOTES = ""
                        }else{
                            objCons.ADD1_QUOTES = ADD1_QUOTES!
                        }
                        if(CESS_1 == nil){
                            objCons.CESS_1 = ""
                        }else{
                            objCons.CESS_1 = CESS_1!
                        }
                        if(ADD2_QUOTES == nil){
                            objCons.ADD2_QUOTES = ""
                        }else{
                            objCons.ADD2_QUOTES = ADD2_QUOTES!
                        }
                        if(MTR_1 == nil){
                            objCons.MTR_1 = ""
                        }else{
                            objCons.MTR_1 = MTR_1!
                        }
                        if(ADD3_QUOTES == nil){
                            objCons.ADD3_QUOTES = ""
                        }else{
                            objCons.ADD3_QUOTES = ADD3_QUOTES!
                        }
                        if(WTCS_1 == nil){
                            objCons.WTCS_1 = ""
                        }else{
                            objCons.WTCS_1 = WTCS_1!
                        }
                        if(DIVTXT_QUOTES == nil){
                            objCons.DIVTXT_QUOTES = ""
                        }else{
                            objCons.DIVTXT_QUOTES = DIVTXT_QUOTES!
                        }
                        if(ASD_1 == nil){
                            objCons.ASD_1 = ""
                        }else{
                            objCons.ASD_1 = ASD_1!
                        }
                        if(REB_1 == nil){
                            objCons.REB_1 = ""
                        }else{
                            objCons.REB_1 = REB_1!
                        }
                        if(ADJ_DMD_1 == nil){
                            objCons.ADJ_DMD_1 = ""
                        }else{
                            objCons.ADJ_DMD_1 = ADJ_DMD_1!
                        }
                        if(AVG_ADJ_1 == nil){
                            objCons.AVG_ADJ_1 = ""
                        }else{
                            objCons.AVG_ADJ_1 = AVG_ADJ_1!
                        }
                        if(MIS_1 == nil){
                            objCons.MIS_1 = ""
                        }else{
                            objCons.MIS_1 = MIS_1!
                        }
                        if(PRE_ARR_1 == nil){
                            objCons.PRE_ARR_1 = ""
                        }else{
                            objCons.PRE_ARR_1 = PRE_ARR_1!
                        }
                        if(TOT_BILL_1 == nil){
                            objCons.TOT_BILL_1 = ""
                        }else{
                            objCons.TOT_BILL_1 = TOT_BILL_1!
                        }
                        if(TOT_AMT_1 == nil){
                            objCons.TOT_AMT_1 = ""
                        }else{
                            objCons.TOT_AMT_1 = TOT_AMT_1!
                        }
                        if(AMT == nil){
                            objCons.AMT = ""
                        }else{
                            objCons.AMT = AMT!
                        }
                        if(SOLAR_RBT == nil){
                            objCons.SOLAR_RBT = ""
                        }else{
                            objCons.SOLAR_RBT = SOLAR_RBT!
                        }
                        if(SG400U_50PR == nil){
                            objCons.SG400U_50PR = ""
                        }else{
                            objCons.SG400U_50PR = SG400U_50PR!
                        }
                        if(NET_BILL == nil){
                            objCons.NET_BILL = ""
                        }else{
                            objCons.NET_BILL = NET_BILL!
                        }
                      
                        
                        
                        obj.electricBillBPDetailsDB.append(objCons)
                    }
                }
                if(countSetTable5 > 0){
                    for i in 0...countSetTable5 - 1  {
                        
                        let objCons = ElectricBillLNDetailsDisplay()
                        let TI1 = accessorTable5[i]["TI1"].text
                        let EN1 = accessorTable5[i]["EN1"].text
                        let PA1 = accessorTable5[i]["PA1"].text
                        let PD1 = accessorTable5[i]["PD1"].text
                        let OP1 = accessorTable5[i]["OP1"].text
                        let SL1 = accessorTable5[i]["SL1"].text
                        let LN1 = accessorTable5[i]["LN1"].text
                        
                       if(TI1 == nil){
                           objCons.TI1 = ""
                       }else{
                           objCons.TI1 = TI1!
                       }
                       if(EN1 == nil){
                           objCons.EN1 = ""
                       }else{
                           objCons.EN1 = EN1!
                       }
                       if(PA1 == nil){
                           objCons.PA1 = ""
                       }else{
                           objCons.PA1 = PA1!
                       }
                       if(PD1 == nil){
                           objCons.PD1 = ""
                       }else{
                           objCons.PD1 = PD1!
                       }
                       if(OP1 == nil){
                           objCons.OP1 = ""
                       }else{
                           objCons.OP1 = OP1!
                       }
                       if(SL1 == nil){
                           objCons.SL1 = ""
                       }else{
                           objCons.SL1 = SL1!
                       }
                       if(LN1 == nil){
                           objCons.LN1 = ""
                       }else{
                           objCons.LN1 = LN1!
                       }
                       
                       
                        
                        
                        obj.electricBillLNDetailsDB.append(objCons)
                    }
                }
                if(countSetTable7 > 0){
                    for i in 0...countSetTable7 - 1  {
                        
                        let objCons = ElectricOfficeDetailsDisplay()
                        let COKEY = accessorTable7[i]["COKEY"].text
                        let DCPHONE = accessorTable7[i]["DCPHONE"].text
                        let JEPHONE = accessorTable7[i]["JEPHONE"].text
                        let JENAME = accessorTable7[i]["JENAME"].text
                        let AENAME = accessorTable7[i]["AENAME"].text
                        let AEPHONE = accessorTable7[i]["AEPHONE"].text
                        if(COKEY == nil){
                            objCons.COKEY = ""
                        }else{
                            objCons.COKEY = COKEY!
                        }
                        if(DCPHONE == nil){
                            objCons.DCPHONE = ""
                        }else{
                            objCons.DCPHONE = DCPHONE!
                        }
                        if(JEPHONE == nil){
                            objCons.JEPHONE = ""
                        }else{
                            objCons.JEPHONE = JEPHONE!
                        }
                        if(JENAME == nil){
                            objCons.JENAME = ""
                        }else{
                            objCons.JENAME = JENAME!
                        }
                        if(AENAME == nil){
                            objCons.AENAME = ""
                        }else{
                            objCons.AENAME = AENAME!
                        }
                        if(AEPHONE == nil){
                            objCons.AEPHONE = ""
                        }else{
                            objCons.AEPHONE = AEPHONE!
                        }
                        
                        
                        obj.electricOfficeDetailsDB.append(objCons)
                    }
                }
              
               
                
                if(countSet > 0){
                    for i in 0...5  {
                        
                        let objCons = ConsumptionDisplay()
                        let bm = accessor["BM" + String(i + 1)].text
                        let mr = accessor["MR" + String(i + 1)].text
                        let SM_CON = accessor["SM_CON" + String(i + 1)].text
                        let SM_MTR_STS = accessor["SM_MTR_STS" + String(i + 1)].text
                        if(bm == nil){
                            objCons.BM = ""
                        }else{
                            objCons.BM = bm!
                        }
                        if(mr == nil){
                            objCons.MR = ""
                        }else{
                            objCons.MR = mr!
                        }
                        if(SM_CON == nil){
                            objCons.SM_CON = ""
                        }else{
                            objCons.SM_CON = SM_CON!
                        }
                        if(SM_MTR_STS == nil){
                            objCons.SM_MTR_STS = ""
                        }else{
                            objCons.SM_MTR_STS = SM_MTR_STS!
                        }
                        
                        obj.consumptionDB.append(objCons)
                    }
                }
                if(countSetTable2 > 0){
                    for i in 0...countSetTable2 - 1  {
                        let objCons = ElectricBillDetailsDisplay()
                        let DOC_NO = accessorTable2[i]["DOC_NO"].text
                        let BILL_MNTH = accessorTable2[i]["BILL_MNTH"].text
                        let ISSUE_DT = accessorTable2[i]["ISSUE_DT"].text
                        let DUE_DT = accessorTable2[i]["DUE_DT"].text
                        let BP_FIRST = accessorTable2[i]["BP_FIRST"].text
                        let DUEDT_CHQ = accessorTable2[i]["DUEDT_CHQ"].text
                        if(DOC_NO == nil){
                            objCons.DOC_NO = ""
                        }else{
                            objCons.DOC_NO = DOC_NO!
                        }
                        if(BILL_MNTH == nil){
                            objCons.BILL_MNTH = ""
                        }else{
                            objCons.BILL_MNTH = BILL_MNTH!
                        }
                        if(ISSUE_DT == nil){
                            objCons.ISSUE_DT = ""
                        }else{
                            objCons.ISSUE_DT = ISSUE_DT!
                        }
                        if(DUE_DT == nil){
                            objCons.DUE_DT = ""
                        }else{
                            objCons.DUE_DT = DUE_DT!
                        }
                        if(DUEDT_CHQ == nil){
                            objCons.DUEDT_CHQ = ""
                        }else{
                            objCons.DUEDT_CHQ = DUEDT_CHQ!
                        }
                        if(BP_FIRST == nil){
                            objCons.BP_FIRST = ""
                        }else{
                            objCons.BP_FIRST = BP_FIRST!
                        }
                       
                        obj.electricBillDetailsDB.append(objCons)
                    }
                }
               
                success(dataArray as AnyObject)

              }

            
        }
        else{

        }
        
        
    }
    
    
}
class RemoveModelDataAPI
{
    
    var reachablty = try! Reachability()
    
    
    func serviceCalling( obj : RemoveConsumerViewController ,bpNo : String, success:@escaping (AnyObject)-> Void)
    {
        
       // obj.startLoading()
        reachablty = try! Reachability()
        if(reachablty.connection != .unavailable)
        {
            obj.startLoadingPK(view: obj.view, text: "Please Wait ....")
          
           
                 let dateFormatterInv = DateFormatter()
                 dateFormatterInv.timeZone = NSTimeZone.system
                 dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
            let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
              let invStrUnique = dateFormatterInv.string(from: Date())
              let request_no = invStrUnique + bpNo
              let message = "ZISU_RFCBILL_PAYTM|\(bpNo)|\(secID)|\(request_no)"
print(message)
                  let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
print(encode)


              AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_arraystring", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
                obj.stopLoadingPK(view: obj.view)
               guard let respo = response.value else {
                   return
               }
                print(response.value)
               let xml = try! XML.parse(respo)
              let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_arraystringResponse"]["MA_cons_arraystringResult"]
                var dataArray : [BillDisplay] = []
                guard let countSet = accessor.all?.count else {
                  // obj.getBillDetailsData(bpno: bpNo)
                                 return
                 }
                if(countSet > 0){
                    for i in 0...countSet - 1  {
                        let child = accessor.all![i]
                        let obj = BillDisplay()
                        obj.name = child.childElements[1].text!
                        obj.mobile = child.childElements[7].text!
                        obj.billno = child.childElements[4].text!
                        obj.amount = child.childElements[5].text!
                        obj.lastDate = child.childElements[6].text!
                        obj.lastMon = child.childElements[8].text!
                        obj.address = child.childElements[3].text!
                        
                        dataArray.append(obj)
                    }
                }
               
                success(dataArray as AnyObject)

              }

            
        }
        else{

        }
        
        
    }
    
    
}
class AddConsumerModelDataAPI
{
    
    var reachablty = try! Reachability()
    
    
    
    func serviceCalling( obj:AddConsumerViewController ,bpNo : String,success:@escaping (AnyObject)-> Void)
    {
        
       // obj.startLoading()
        reachablty = try! Reachability()
        if(reachablty.connection != .unavailable)
        {
                    obj.startLoadingPK(view: obj.view, text: "please wait...")
                   
                   
                         let dateFormatterInv = DateFormatter()
                         dateFormatterInv.timeZone = NSTimeZone.system
                         dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
                    let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
                      let invStrUnique = dateFormatterInv.string(from: Date())
                      let request_no = invStrUnique + bpNo
                      let message = "ZBAPI_BI_LTBILLDISP|\(bpNo)|||\(secID)|\(request_no)"
        print(message)
                          let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
        print(encode)


                      AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_dataset", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
                      // print("Request: \(response.value)")
                       guard let respo = response.value else {
                        obj.stopLoadingPK(view: obj.view)
                        obj.getDetailsData(bpno: obj.txtConsumerNo.text!)
                           return
                       }
                        obj.stopLoadingPK(view: obj.view)
                        print(response.value)
                       let xml = try! XML.parse(respo)
                      let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table6"]
                        let accessorTable2 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table2"]
                        let accessorTable1 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table1"]
                         let accessorTable3 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table3"]
                        
                        let accessorTable4 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table4"]
                        let accessorTable5 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table5"]
                         let accessorTable7 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table7"]
                        
                        
                        
                        var dataArray : [ElectricMainDetailsDisplay] = []
                        guard let countSetTable1 = accessorTable1.all?.count else {
//                         obj.stopLoadingPK(view: obj.view)
//                         obj.getDetailsData(bpno: obj.txtConsumerNo.text!)
                                        return
                        }
                        guard let countSetTable2 = accessorTable2.all?.count else {
                         // obj.getBillDetailsData(bpno: bpNo)
                                        return
                        }
                        guard let countSetTable3 = accessorTable3.all?.count else {
                         // obj.getBillDetailsData(bpno: bpNo)
                                        return
                        }
                        guard let countSetTable4 = accessorTable4.all?.count else {
                         // obj.getBillDetailsData(bpno: bpNo)
                                        return
                        }
                        guard let countSetTable5 = accessorTable5.all?.count else {
                         // obj.getBillDetailsData(bpno: bpNo)
                                        return
                        }
                        guard let countSetTable7 = accessorTable7.all?.count else {
                         // obj.getBillDetailsData(bpno: bpNo)
                                        return
                        }
                        guard let countSet = accessor.all?.count else {
                          // obj.getBillDetailsData(bpno: bpNo)
                                         return
                         }
                        
                        if(countSetTable1 > 0){
                            for i in 0...countSetTable1 - 1  {
                                
                                 let objCons = ElectricMainDetailsDisplay()
                                 let RAOTXT = accessorTable1[i]["RAOTXT"].text
                                 let DIVTXT = accessorTable1[i]["DIVTXT"].text
                                 let BP_NO = accessorTable1[i]["BP_NO"].text
                                 let BP_NAME = accessorTable1[i]["BP_NAME"].text
                                 let ADD1 = accessorTable1[i]["ADD1"].text
                                 let ADD2 = accessorTable1[i]["ADD2"].text
                                 let ADD3 = accessorTable1[i]["ADD3"].text
                                 let RT_CT = accessorTable1[i]["RT_CT"].text
                                 let SD_1 = accessorTable1[i]["SD_1"].text
                                 let TEA = accessorTable1[i]["TEA"].text
                                 let SD_INT = accessorTable1[i]["SD_INT"].text
                                 let TEL_NUMBER = accessorTable1[i]["TEL_NUMBER"].text
                                if(RAOTXT == nil){
                                    objCons.RAOTXT = ""
                                }else{
                                    objCons.RAOTXT = RAOTXT!
                                }
                                if(DIVTXT == nil){
                                    objCons.DIVTXT = ""
                                }else{
                                    objCons.DIVTXT = DIVTXT!
                                }
                                if(BP_NAME == nil){
                                    objCons.BP_NAME = ""
                                }else{
                                    objCons.BP_NAME = BP_NAME!
                                }
                                if(BP_NO == nil){
                                    objCons.BP_NO = ""
                                }else{
                                    objCons.BP_NO = BP_NO!
                                }
                                if(ADD1 == nil){
                                    objCons.ADD1 = ""
                                }else{
                                    objCons.ADD1 = ADD1!
                                }
                                if(ADD2 == nil){
                                    objCons.ADD2 = ""
                                }else{
                                    objCons.ADD2 = ADD2!
                                }
                                if(ADD3 == nil){
                                    objCons.ADD3 = ""
                                }else{
                                    objCons.ADD3 = ADD3!
                                }
                                if(RT_CT == nil){
                                    objCons.RT_CT = ""
                                }else{
                                    objCons.RT_CT = RT_CT!
                                }
                                if(SD_1 == nil){
                                    objCons.SD_1 = ""
                                }else{
                                    objCons.SD_1 = SD_1!
                                }
                                if(TEA == nil){
                                    objCons.TEA = ""
                                }else{
                                    objCons.TEA = TEA!
                                }
                                if(SD_INT == nil){
                                    objCons.SD_INT = ""
                                }else{
                                    objCons.SD_INT = SD_INT!
                                }
                                if(TEL_NUMBER == nil){
                                    objCons.TEL_NUMBER = ""
                                }else{
                                    objCons.TEL_NUMBER = TEL_NUMBER!
                                }
                               
                                dataArray.append(objCons)
                            }
                        }
                       
                        if(countSetTable3 > 0){
                            for i in 0...countSetTable3 - 1  {
                                let objCons = ElectricBillUnitDetailsDisplay()
                                let CON_LD = accessorTable3[i]["CON_LD"].text
                                let CON_LOAD = accessorTable3[i]["CON_LOAD"].text
                                let CR_RD = accessorTable3[i]["CR_RD"].text
                                let PR_RD = accessorTable3[i]["PR_RD"].text
                                let CONS = accessorTable3[i]["CONS"].text
                                let MD = accessorTable3[i]["MD"].text
                                
                                let KWH_ON = accessorTable3[i]["KWH_ON"].text
                                let PF = accessorTable3[i]["PF"].text
                                let CONS_TEXT = accessorTable3[i]["CONS_TEXT"].text
                                let SOLAR_UNIT = accessorTable3[i]["SOLAR_UNIT"].text
                                let BILLING_TYPE = accessorTable3[i]["BILLING_TYPE"].text
                                let PURPOSE = accessorTable3[i]["PURPOSE"].text
                                let MF = accessorTable3[i]["MF"].text
                                let ASSD_UNIT = accessorTable3[i]["ASSD_UNIT"].text
                                let TOT_UNIT = accessorTable3[i]["TOT_UNIT"].text
                                let AVRG_QTY = accessorTable3[i]["AVRG_QTY"].text
                                if(CON_LD == nil){
                                    objCons.CON_LD = ""
                                }else{
                                    objCons.CON_LD = CON_LD!
                                }
                                if(CON_LOAD == nil){
                                    objCons.CON_LOAD = ""
                                }else{
                                    objCons.CON_LOAD = CON_LOAD!
                                }
                                if(CR_RD == nil){
                                    objCons.CR_RD = ""
                                }else{
                                    objCons.CR_RD = CR_RD!
                                }
                                if(PR_RD == nil){
                                    objCons.PR_RD = ""
                                }else{
                                    objCons.PR_RD = PR_RD!
                                }
                                if(CONS == nil){
                                    objCons.CONS = ""
                                }else{
                                    objCons.CONS = CONS!
                                }
                                if(MD == nil){
                                    objCons.MD = ""
                                }else{
                                    objCons.MD = MD!
                                }
                                if(KWH_ON == nil){
                                    objCons.KWH_ON = ""
                                }else{
                                    objCons.KWH_ON = KWH_ON!
                                }
                                if(PF == nil){
                                    objCons.PF = ""
                                }else{
                                    objCons.PF = PF!
                                }
                                if(CONS_TEXT == nil){
                                    objCons.CONS_TEXT = ""
                                }else{
                                    objCons.CONS_TEXT = CONS_TEXT!
                                }
                                if(SOLAR_UNIT == nil){
                                    objCons.SOLAR_UNIT = ""
                                }else{
                                    objCons.SOLAR_UNIT = SOLAR_UNIT!
                                }
                                if(BILLING_TYPE == nil){
                                    objCons.BILLING_TYPE = ""
                                }else{
                                    objCons.BILLING_TYPE = BILLING_TYPE!
                                }
                                if(PURPOSE == nil){
                                    objCons.PURPOSE = ""
                                }else{
                                    objCons.PURPOSE = PURPOSE!
                                }
                                if(MF == nil){
                                    objCons.MF = ""
                                }else{
                                    objCons.MF = MF!
                                }
                                if(ASSD_UNIT == nil){
                                    objCons.ASSD_UNIT = ""
                                }else{
                                    objCons.ASSD_UNIT = ASSD_UNIT!
                                }
                                if(TOT_UNIT == nil){
                                    objCons.TOT_UNIT = ""
                                }else{
                                    objCons.TOT_UNIT = TOT_UNIT!
                                }
                                if(AVRG_QTY == nil){
                                    objCons.AVRG_QTY = ""
                                }else{
                                    objCons.AVRG_QTY = AVRG_QTY!
                                }
                             
                                obj.electricUnitsDetailsDB.append(objCons)
                            }
                        }
                       if(countSetTable4 > 0){
                            for i in 0...countSetTable4 - 1  {
                                
                                let objCons = ElectricBillBPDataDetailsDisplay()
                                let EC_1 = accessorTable4[i]["EC_1"].text
                                let FIX_CH_1 = accessorTable4[i]["FIX_CH_1"].text
                                let FCA_1 = accessorTable4[i]["FCA_1"].text
                                let BP_NAME_QUOTES = accessorTable4[i]["BP_NAME_QUOTES"].text
                                let ADD1_QUOTES = accessorTable4[i]["ADD1_QUOTES"].text
                                let CESS_1 = accessorTable4[i]["CESS_1"].text
                                let ADD2_QUOTES = accessorTable4[i]["ADD2_QUOTES"].text
                                let MTR_1 = accessorTable4[i]["MTR_1"].text
                                let ADD3_QUOTES = accessorTable4[i]["ADD3_QUOTES"].text
                                let WTCS_1 = accessorTable4[i]["WTCS_1"].text
                                let DIVTXT_QUOTES = accessorTable4[i]["DIVTXT_QUOTES"].text
                                let ASD_1 = accessorTable4[i]["ASD_1"].text
                                let REB_1 = accessorTable4[i]["REB_1"].text
                                let ADJ_DMD_1 = accessorTable4[i]["ADJ_DMD_1"].text
                                let AVG_ADJ_1 = accessorTable4[i]["AVG_ADJ_1"].text
                                let MIS_1 = accessorTable4[i]["MIS_1"].text
                                let PRE_ARR_1 = accessorTable4[i]["PRE_ARR_1"].text
                                let TOT_BILL_1 = accessorTable4[i]["TOT_BILL_1"].text
                                let TOT_AMT_1 = accessorTable4[i]["TOT_AMT_1"].text
                                let AMT = accessorTable4[i]["AMT"].text
                                let SOLAR_RBT = accessorTable4[i]["SOLAR_RBT"].text
                                let SG400U_50PR = accessorTable4[i]["SG400U_50PR"].text
                                let NET_BILL = accessorTable4[i]["NET_BILL"].text
                                let ED_1 = accessorTable4[i]["ED_1"].text
                                if(ED_1 == nil){
                                    objCons.ED_1 = ""
                                }else{
                                    objCons.ED_1 = ED_1!
                                }
                                
                                if(EC_1 == nil){
                                    objCons.EC_1 = ""
                                }else{
                                    objCons.EC_1 = EC_1!
                                }
                                if(FIX_CH_1 == nil){
                                    objCons.FIX_CH_1 = ""
                                }else{
                                    objCons.FIX_CH_1 = FIX_CH_1!
                                }
                                if(FCA_1 == nil){
                                    objCons.FCA_1 = ""
                                }else{
                                    objCons.FCA_1 = FCA_1!
                                }
                                if(BP_NAME_QUOTES == nil){
                                    objCons.BP_NAME_QUOTES = ""
                                }else{
                                    objCons.BP_NAME_QUOTES = BP_NAME_QUOTES!
                                }
                                if(ADD1_QUOTES == nil){
                                    objCons.ADD1_QUOTES = ""
                                }else{
                                    objCons.ADD1_QUOTES = ADD1_QUOTES!
                                }
                                if(CESS_1 == nil){
                                    objCons.CESS_1 = ""
                                }else{
                                    objCons.CESS_1 = CESS_1!
                                }
                                if(ADD2_QUOTES == nil){
                                    objCons.ADD2_QUOTES = ""
                                }else{
                                    objCons.ADD2_QUOTES = ADD2_QUOTES!
                                }
                                if(MTR_1 == nil){
                                    objCons.MTR_1 = ""
                                }else{
                                    objCons.MTR_1 = MTR_1!
                                }
                                if(ADD3_QUOTES == nil){
                                    objCons.ADD3_QUOTES = ""
                                }else{
                                    objCons.ADD3_QUOTES = ADD3_QUOTES!
                                }
                                if(WTCS_1 == nil){
                                    objCons.WTCS_1 = ""
                                }else{
                                    objCons.WTCS_1 = WTCS_1!
                                }
                                if(DIVTXT_QUOTES == nil){
                                    objCons.DIVTXT_QUOTES = ""
                                }else{
                                    objCons.DIVTXT_QUOTES = DIVTXT_QUOTES!
                                }
                                if(ASD_1 == nil){
                                    objCons.ASD_1 = ""
                                }else{
                                    objCons.ASD_1 = ASD_1!
                                }
                                if(REB_1 == nil){
                                    objCons.REB_1 = ""
                                }else{
                                    objCons.REB_1 = REB_1!
                                }
                                if(ADJ_DMD_1 == nil){
                                    objCons.ADJ_DMD_1 = ""
                                }else{
                                    objCons.ADJ_DMD_1 = ADJ_DMD_1!
                                }
                                if(AVG_ADJ_1 == nil){
                                    objCons.AVG_ADJ_1 = ""
                                }else{
                                    objCons.AVG_ADJ_1 = AVG_ADJ_1!
                                }
                                if(MIS_1 == nil){
                                    objCons.MIS_1 = ""
                                }else{
                                    objCons.MIS_1 = MIS_1!
                                }
                                if(PRE_ARR_1 == nil){
                                    objCons.PRE_ARR_1 = ""
                                }else{
                                    objCons.PRE_ARR_1 = PRE_ARR_1!
                                }
                                if(TOT_BILL_1 == nil){
                                    objCons.TOT_BILL_1 = ""
                                }else{
                                    objCons.TOT_BILL_1 = TOT_BILL_1!
                                }
                                if(TOT_AMT_1 == nil){
                                    objCons.TOT_AMT_1 = ""
                                }else{
                                    objCons.TOT_AMT_1 = TOT_AMT_1!
                                }
                                if(AMT == nil){
                                    objCons.AMT = ""
                                }else{
                                    objCons.AMT = AMT!
                                }
                                if(SOLAR_RBT == nil){
                                    objCons.SOLAR_RBT = ""
                                }else{
                                    objCons.SOLAR_RBT = SOLAR_RBT!
                                }
                                if(SG400U_50PR == nil){
                                    objCons.SG400U_50PR = ""
                                }else{
                                    objCons.SG400U_50PR = SG400U_50PR!
                                }
                                if(NET_BILL == nil){
                                    objCons.NET_BILL = ""
                                }else{
                                    objCons.NET_BILL = NET_BILL!
                                }
                              
                                
                                
                                obj.electricBillBPDetailsDB.append(objCons)
                            }
                        }
                        if(countSetTable5 > 0){
                            for i in 0...countSetTable5 - 1  {
                                
                                let objCons = ElectricBillLNDetailsDisplay()
                                let TI1 = accessorTable5[i]["TI1"].text
                                let EN1 = accessorTable5[i]["EN1"].text
                                let PA1 = accessorTable5[i]["PA1"].text
                                let PD1 = accessorTable5[i]["PD1"].text
                                let OP1 = accessorTable5[i]["OP1"].text
                                let SL1 = accessorTable5[i]["SL1"].text
                                let LN1 = accessorTable5[i]["LN1"].text
                                
                               if(TI1 == nil){
                                   objCons.TI1 = ""
                               }else{
                                   objCons.TI1 = TI1!
                               }
                               if(EN1 == nil){
                                   objCons.EN1 = ""
                               }else{
                                   objCons.EN1 = EN1!
                               }
                               if(PA1 == nil){
                                   objCons.PA1 = ""
                               }else{
                                   objCons.PA1 = PA1!
                               }
                               if(PD1 == nil){
                                   objCons.PD1 = ""
                               }else{
                                   objCons.PD1 = PD1!
                               }
                               if(OP1 == nil){
                                   objCons.OP1 = ""
                               }else{
                                   objCons.OP1 = OP1!
                               }
                               if(SL1 == nil){
                                   objCons.SL1 = ""
                               }else{
                                   objCons.SL1 = SL1!
                               }
                               if(LN1 == nil){
                                   objCons.LN1 = ""
                               }else{
                                   objCons.LN1 = LN1!
                               }
                               
                               
                                
                                
                                obj.electricBillLNDetailsDB.append(objCons)
                            }
                        }
                        if(countSetTable7 > 0){
                            for i in 0...countSetTable7 - 1  {
                                
                                let objCons = ElectricOfficeDetailsDisplay()
                                let COKEY = accessorTable7[i]["COKEY"].text
                                let DCPHONE = accessorTable7[i]["DCPHONE"].text
                                let JEPHONE = accessorTable7[i]["JEPHONE"].text
                                let JENAME = accessorTable7[i]["JENAME"].text
                                let AENAME = accessorTable7[i]["AENAME"].text
                                let AEPHONE = accessorTable7[i]["AEPHONE"].text
                                if(COKEY == nil){
                                    objCons.COKEY = ""
                                }else{
                                    objCons.COKEY = COKEY!
                                }
                                if(DCPHONE == nil){
                                    objCons.DCPHONE = ""
                                }else{
                                    objCons.DCPHONE = DCPHONE!
                                }
                                if(JEPHONE == nil){
                                    objCons.JEPHONE = ""
                                }else{
                                    objCons.JEPHONE = JEPHONE!
                                }
                                if(JENAME == nil){
                                    objCons.JENAME = ""
                                }else{
                                    objCons.JENAME = JENAME!
                                }
                                if(AENAME == nil){
                                    objCons.AENAME = ""
                                }else{
                                    objCons.AENAME = AENAME!
                                }
                                if(AEPHONE == nil){
                                    objCons.AEPHONE = ""
                                }else{
                                    objCons.AEPHONE = AEPHONE!
                                }
                                
                                
                                obj.electricOfficeDetailsDB.append(objCons)
                            }
                        }
                      
                       
                        
                        if(countSet > 0){
                            for i in 0...5  {
                                
                                let objCons = ConsumptionDisplay()
                                let bm = accessor["BM" + String(i + 1)].text
                                let mr = accessor["MR" + String(i + 1)].text
                                let SM_CON = accessor["SM_CON" + String(i + 1)].text
                                let SM_MTR_STS = accessor["SM_MTR_STS" + String(i + 1)].text
                                if(bm == nil){
                                    objCons.BM = ""
                                }else{
                                    objCons.BM = bm!
                                }
                                if(mr == nil){
                                    objCons.MR = ""
                                }else{
                                    objCons.MR = mr!
                                }
                                if(SM_CON == nil){
                                    objCons.SM_CON = ""
                                }else{
                                    objCons.SM_CON = SM_CON!
                                }
                                if(SM_MTR_STS == nil){
                                    objCons.SM_MTR_STS = ""
                                }else{
                                    objCons.SM_MTR_STS = SM_MTR_STS!
                                }
                                
                                obj.consumptionDB.append(objCons)
                            }
                        }
                        if(countSetTable2 > 0){
                            for i in 0...countSetTable2 - 1  {
                                let objCons = ElectricBillDetailsDisplay()
                                let DOC_NO = accessorTable2[i]["DOC_NO"].text
                                let BILL_MNTH = accessorTable2[i]["BILL_MNTH"].text
                                let ISSUE_DT = accessorTable2[i]["ISSUE_DT"].text
                                let DUE_DT = accessorTable2[i]["DUE_DT"].text
                                let BP_FIRST = accessorTable2[i]["BP_FIRST"].text
                                let DUEDT_CHQ = accessorTable2[i]["DUEDT_CHQ"].text
                                if(DOC_NO == nil){
                                    objCons.DOC_NO = ""
                                }else{
                                    objCons.DOC_NO = DOC_NO!
                                }
                                if(BILL_MNTH == nil){
                                    objCons.BILL_MNTH = ""
                                }else{
                                    objCons.BILL_MNTH = BILL_MNTH!
                                }
                                if(ISSUE_DT == nil){
                                    objCons.ISSUE_DT = ""
                                }else{
                                    objCons.ISSUE_DT = ISSUE_DT!
                                }
                                if(DUE_DT == nil){
                                    objCons.DUE_DT = ""
                                }else{
                                    objCons.DUE_DT = DUE_DT!
                                }
                                if(DUEDT_CHQ == nil){
                                    objCons.DUEDT_CHQ = ""
                                }else{
                                    objCons.DUEDT_CHQ = DUEDT_CHQ!
                                }
                                if(BP_FIRST == nil){
                                    objCons.BP_FIRST = ""
                                }else{
                                    objCons.BP_FIRST = BP_FIRST!
                                }
                               
                                obj.electricBillDetailsDB.append(objCons)
                            }
                        }
                       
                        success(dataArray as AnyObject)

                      }

                    
                }
        else{

        }
        
        
    }
    
    
}
class ComplaintModelDataAPI
{
    
    var reachablty = try! Reachability()
    
    
    func serviceCalling( obj : ComplaintRegisterViewController ,bpNo : String, success:@escaping (AnyObject)-> Void)
    {
        
       // obj.startLoading()
        reachablty = try! Reachability()
        if(reachablty.connection != .unavailable)
        {
            obj.startLoadingPK(view: obj.view, text: "please wait...")
           
           
                 let dateFormatterInv = DateFormatter()
                 dateFormatterInv.timeZone = NSTimeZone.system
                 dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
            let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
              let invStrUnique = dateFormatterInv.string(from: Date())
              let request_no = invStrUnique + bpNo
              let message = "ZBAPI_BI_LTBILLDISP|\(bpNo)|||\(secID)|\(request_no)"
print(message)
                  let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
print(encode)


              AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_dataset", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
              // print("Request: \(response.value)")
               guard let respo = response.value else {
                obj.stopLoadingPK(view: obj.view)
                obj.getPaymentBillDetailsData(bpno: obj.cellData.lblPartner.text!)
                   return
               }
                obj.stopLoadingPK(view: obj.view)
                print(response.value)
               let xml = try! XML.parse(respo)
              let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table6"]
                let accessorTable2 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table2"]
                let accessorTable1 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table1"]
                 let accessorTable3 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table3"]
                
                let accessorTable4 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table4"]
                let accessorTable5 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table5"]
                 let accessorTable7 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table7"]
                
                
                
                var dataArray : [ElectricMainDetailsDisplay] = []
                guard let countSetTable1 = accessorTable1.all?.count else {
                 //obj.getPaymentBillDetailsData(bpno: obj.cellData.lblPartner.text!)
                                return
                }
                guard let countSetTable2 = accessorTable2.all?.count else {
                 // obj.getBillDetailsData(bpno: bpNo)
                                return
                }
                guard let countSetTable3 = accessorTable3.all?.count else {
                 // obj.getBillDetailsData(bpno: bpNo)
                                return
                }
                guard let countSetTable4 = accessorTable4.all?.count else {
                 // obj.getBillDetailsData(bpno: bpNo)
                                return
                }
                guard let countSetTable5 = accessorTable5.all?.count else {
                 // obj.getBillDetailsData(bpno: bpNo)
                                return
                }
                guard let countSetTable7 = accessorTable7.all?.count else {
                 // obj.getBillDetailsData(bpno: bpNo)
                                return
                }
                guard let countSet = accessor.all?.count else {
                  // obj.getBillDetailsData(bpno: bpNo)
                                 return
                 }
                
                if(countSetTable1 > 0){
                    for i in 0...countSetTable1 - 1  {
                        
                         let objCons = ElectricMainDetailsDisplay()
                         let RAOTXT = accessorTable1[i]["RAOTXT"].text
                         let DIVTXT = accessorTable1[i]["DIVTXT"].text
                         let BP_NO = accessorTable1[i]["BP_NO"].text
                         let BP_NAME = accessorTable1[i]["BP_NAME"].text
                         let ADD1 = accessorTable1[i]["ADD1"].text
                         let ADD2 = accessorTable1[i]["ADD2"].text
                         let ADD3 = accessorTable1[i]["ADD3"].text
                         let RT_CT = accessorTable1[i]["RT_CT"].text
                         let SD_1 = accessorTable1[i]["SD_1"].text
                         let TEA = accessorTable1[i]["TEA"].text
                         let SD_INT = accessorTable1[i]["SD_INT"].text
                         let TEL_NUMBER = accessorTable1[i]["TEL_NUMBER"].text
                        if(RAOTXT == nil){
                            objCons.RAOTXT = ""
                        }else{
                            objCons.RAOTXT = RAOTXT!
                        }
                        if(DIVTXT == nil){
                            objCons.DIVTXT = ""
                        }else{
                            objCons.DIVTXT = DIVTXT!
                        }
                        if(BP_NAME == nil){
                            objCons.BP_NAME = ""
                        }else{
                            objCons.BP_NAME = BP_NAME!
                        }
                        if(BP_NO == nil){
                            objCons.BP_NO = ""
                        }else{
                            objCons.BP_NO = BP_NO!
                        }
                        if(ADD1 == nil){
                            objCons.ADD1 = ""
                        }else{
                            objCons.ADD1 = ADD1!
                        }
                        if(ADD2 == nil){
                            objCons.ADD2 = ""
                        }else{
                            objCons.ADD2 = ADD2!
                        }
                        if(ADD3 == nil){
                            objCons.ADD3 = ""
                        }else{
                            objCons.ADD3 = ADD3!
                        }
                        if(RT_CT == nil){
                            objCons.RT_CT = ""
                        }else{
                            objCons.RT_CT = RT_CT!
                        }
                        if(SD_1 == nil){
                            objCons.SD_1 = ""
                        }else{
                            objCons.SD_1 = SD_1!
                        }
                        if(TEA == nil){
                            objCons.TEA = ""
                        }else{
                            objCons.TEA = TEA!
                        }
                        if(SD_INT == nil){
                            objCons.SD_INT = ""
                        }else{
                            objCons.SD_INT = SD_INT!
                        }
                        if(TEL_NUMBER == nil){
                            objCons.TEL_NUMBER = ""
                        }else{
                            objCons.TEL_NUMBER = TEL_NUMBER!
                        }
                       
                        dataArray.append(objCons)
                    }
                }
               
                if(countSetTable3 > 0){
                    for i in 0...countSetTable3 - 1  {
                        let objCons = ElectricBillUnitDetailsDisplay()
                        let CON_LD = accessorTable3[i]["CON_LD"].text
                        let CON_LOAD = accessorTable3[i]["CON_LOAD"].text
                        let CR_RD = accessorTable3[i]["CR_RD"].text
                        let PR_RD = accessorTable3[i]["PR_RD"].text
                        let CONS = accessorTable3[i]["CONS"].text
                        let MD = accessorTable3[i]["MD"].text
                        
                        let KWH_ON = accessorTable3[i]["KWH_ON"].text
                        let PF = accessorTable3[i]["PF"].text
                        let CONS_TEXT = accessorTable3[i]["CONS_TEXT"].text
                        let SOLAR_UNIT = accessorTable3[i]["SOLAR_UNIT"].text
                        let BILLING_TYPE = accessorTable3[i]["BILLING_TYPE"].text
                        let PURPOSE = accessorTable3[i]["PURPOSE"].text
                        let MF = accessorTable3[i]["MF"].text
                        let ASSD_UNIT = accessorTable3[i]["ASSD_UNIT"].text
                        let TOT_UNIT = accessorTable3[i]["TOT_UNIT"].text
                        let AVRG_QTY = accessorTable3[i]["AVRG_QTY"].text
                        if(CON_LD == nil){
                            objCons.CON_LD = ""
                        }else{
                            objCons.CON_LD = CON_LD!
                        }
                        if(CON_LOAD == nil){
                            objCons.CON_LOAD = ""
                        }else{
                            objCons.CON_LOAD = CON_LOAD!
                        }
                        if(CR_RD == nil){
                            objCons.CR_RD = ""
                        }else{
                            objCons.CR_RD = CR_RD!
                        }
                        if(PR_RD == nil){
                            objCons.PR_RD = ""
                        }else{
                            objCons.PR_RD = PR_RD!
                        }
                        if(CONS == nil){
                            objCons.CONS = ""
                        }else{
                            objCons.CONS = CONS!
                        }
                        if(MD == nil){
                            objCons.MD = ""
                        }else{
                            objCons.MD = MD!
                        }
                        if(KWH_ON == nil){
                            objCons.KWH_ON = ""
                        }else{
                            objCons.KWH_ON = KWH_ON!
                        }
                        if(PF == nil){
                            objCons.PF = ""
                        }else{
                            objCons.PF = PF!
                        }
                        if(CONS_TEXT == nil){
                            objCons.CONS_TEXT = ""
                        }else{
                            objCons.CONS_TEXT = CONS_TEXT!
                        }
                        if(SOLAR_UNIT == nil){
                            objCons.SOLAR_UNIT = ""
                        }else{
                            objCons.SOLAR_UNIT = SOLAR_UNIT!
                        }
                        if(BILLING_TYPE == nil){
                            objCons.BILLING_TYPE = ""
                        }else{
                            objCons.BILLING_TYPE = BILLING_TYPE!
                        }
                        if(PURPOSE == nil){
                            objCons.PURPOSE = ""
                        }else{
                            objCons.PURPOSE = PURPOSE!
                        }
                        if(MF == nil){
                            objCons.MF = ""
                        }else{
                            objCons.MF = MF!
                        }
                        if(ASSD_UNIT == nil){
                            objCons.ASSD_UNIT = ""
                        }else{
                            objCons.ASSD_UNIT = ASSD_UNIT!
                        }
                        if(TOT_UNIT == nil){
                            objCons.TOT_UNIT = ""
                        }else{
                            objCons.TOT_UNIT = TOT_UNIT!
                        }
                        if(AVRG_QTY == nil){
                            objCons.AVRG_QTY = ""
                        }else{
                            objCons.AVRG_QTY = AVRG_QTY!
                        }
                     
                        obj.electricUnitsDetailsDB.append(objCons)
                    }
                }
               if(countSetTable4 > 0){
                    for i in 0...countSetTable4 - 1  {
                        
                        let objCons = ElectricBillBPDataDetailsDisplay()
                        let EC_1 = accessorTable4[i]["EC_1"].text
                        let FIX_CH_1 = accessorTable4[i]["FIX_CH_1"].text
                        let FCA_1 = accessorTable4[i]["FCA_1"].text
                        let BP_NAME_QUOTES = accessorTable4[i]["BP_NAME_QUOTES"].text
                        let ADD1_QUOTES = accessorTable4[i]["ADD1_QUOTES"].text
                        let CESS_1 = accessorTable4[i]["CESS_1"].text
                        let ADD2_QUOTES = accessorTable4[i]["ADD2_QUOTES"].text
                        let MTR_1 = accessorTable4[i]["MTR_1"].text
                        let ADD3_QUOTES = accessorTable4[i]["ADD3_QUOTES"].text
                        let WTCS_1 = accessorTable4[i]["WTCS_1"].text
                        let DIVTXT_QUOTES = accessorTable4[i]["DIVTXT_QUOTES"].text
                        let ASD_1 = accessorTable4[i]["ASD_1"].text
                        let REB_1 = accessorTable4[i]["REB_1"].text
                        let ADJ_DMD_1 = accessorTable4[i]["ADJ_DMD_1"].text
                        let AVG_ADJ_1 = accessorTable4[i]["AVG_ADJ_1"].text
                        let MIS_1 = accessorTable4[i]["MIS_1"].text
                        let PRE_ARR_1 = accessorTable4[i]["PRE_ARR_1"].text
                        let TOT_BILL_1 = accessorTable4[i]["TOT_BILL_1"].text
                        let TOT_AMT_1 = accessorTable4[i]["TOT_AMT_1"].text
                        let AMT = accessorTable4[i]["AMT"].text
                        let SOLAR_RBT = accessorTable4[i]["SOLAR_RBT"].text
                        let SG400U_50PR = accessorTable4[i]["SG400U_50PR"].text
                        let NET_BILL = accessorTable4[i]["NET_BILL"].text
                        let ED_1 = accessorTable4[i]["ED_1"].text
                        if(ED_1 == nil){
                            objCons.ED_1 = ""
                        }else{
                            objCons.ED_1 = ED_1!
                        }
                        
                        if(EC_1 == nil){
                            objCons.EC_1 = ""
                        }else{
                            objCons.EC_1 = EC_1!
                        }
                        if(FIX_CH_1 == nil){
                            objCons.FIX_CH_1 = ""
                        }else{
                            objCons.FIX_CH_1 = FIX_CH_1!
                        }
                        if(FCA_1 == nil){
                            objCons.FCA_1 = ""
                        }else{
                            objCons.FCA_1 = FCA_1!
                        }
                        if(BP_NAME_QUOTES == nil){
                            objCons.BP_NAME_QUOTES = ""
                        }else{
                            objCons.BP_NAME_QUOTES = BP_NAME_QUOTES!
                        }
                        if(ADD1_QUOTES == nil){
                            objCons.ADD1_QUOTES = ""
                        }else{
                            objCons.ADD1_QUOTES = ADD1_QUOTES!
                        }
                        if(CESS_1 == nil){
                            objCons.CESS_1 = ""
                        }else{
                            objCons.CESS_1 = CESS_1!
                        }
                        if(ADD2_QUOTES == nil){
                            objCons.ADD2_QUOTES = ""
                        }else{
                            objCons.ADD2_QUOTES = ADD2_QUOTES!
                        }
                        if(MTR_1 == nil){
                            objCons.MTR_1 = ""
                        }else{
                            objCons.MTR_1 = MTR_1!
                        }
                        if(ADD3_QUOTES == nil){
                            objCons.ADD3_QUOTES = ""
                        }else{
                            objCons.ADD3_QUOTES = ADD3_QUOTES!
                        }
                        if(WTCS_1 == nil){
                            objCons.WTCS_1 = ""
                        }else{
                            objCons.WTCS_1 = WTCS_1!
                        }
                        if(DIVTXT_QUOTES == nil){
                            objCons.DIVTXT_QUOTES = ""
                        }else{
                            objCons.DIVTXT_QUOTES = DIVTXT_QUOTES!
                        }
                        if(ASD_1 == nil){
                            objCons.ASD_1 = ""
                        }else{
                            objCons.ASD_1 = ASD_1!
                        }
                        if(REB_1 == nil){
                            objCons.REB_1 = ""
                        }else{
                            objCons.REB_1 = REB_1!
                        }
                        if(ADJ_DMD_1 == nil){
                            objCons.ADJ_DMD_1 = ""
                        }else{
                            objCons.ADJ_DMD_1 = ADJ_DMD_1!
                        }
                        if(AVG_ADJ_1 == nil){
                            objCons.AVG_ADJ_1 = ""
                        }else{
                            objCons.AVG_ADJ_1 = AVG_ADJ_1!
                        }
                        if(MIS_1 == nil){
                            objCons.MIS_1 = ""
                        }else{
                            objCons.MIS_1 = MIS_1!
                        }
                        if(PRE_ARR_1 == nil){
                            objCons.PRE_ARR_1 = ""
                        }else{
                            objCons.PRE_ARR_1 = PRE_ARR_1!
                        }
                        if(TOT_BILL_1 == nil){
                            objCons.TOT_BILL_1 = ""
                        }else{
                            objCons.TOT_BILL_1 = TOT_BILL_1!
                        }
                        if(TOT_AMT_1 == nil){
                            objCons.TOT_AMT_1 = ""
                        }else{
                            objCons.TOT_AMT_1 = TOT_AMT_1!
                        }
                        if(AMT == nil){
                            objCons.AMT = ""
                        }else{
                            objCons.AMT = AMT!
                        }
                        if(SOLAR_RBT == nil){
                            objCons.SOLAR_RBT = ""
                        }else{
                            objCons.SOLAR_RBT = SOLAR_RBT!
                        }
                        if(SG400U_50PR == nil){
                            objCons.SG400U_50PR = ""
                        }else{
                            objCons.SG400U_50PR = SG400U_50PR!
                        }
                        if(NET_BILL == nil){
                            objCons.NET_BILL = ""
                        }else{
                            objCons.NET_BILL = NET_BILL!
                        }
                      
                        
                        
                        obj.electricBillBPDetailsDB.append(objCons)
                    }
                }
                if(countSetTable5 > 0){
                    for i in 0...countSetTable5 - 1  {
                        
                        let objCons = ElectricBillLNDetailsDisplay()
                        let TI1 = accessorTable5[i]["TI1"].text
                        let EN1 = accessorTable5[i]["EN1"].text
                        let PA1 = accessorTable5[i]["PA1"].text
                        let PD1 = accessorTable5[i]["PD1"].text
                        let OP1 = accessorTable5[i]["OP1"].text
                        let SL1 = accessorTable5[i]["SL1"].text
                        let LN1 = accessorTable5[i]["LN1"].text
                        
                       if(TI1 == nil){
                           objCons.TI1 = ""
                       }else{
                           objCons.TI1 = TI1!
                       }
                       if(EN1 == nil){
                           objCons.EN1 = ""
                       }else{
                           objCons.EN1 = EN1!
                       }
                       if(PA1 == nil){
                           objCons.PA1 = ""
                       }else{
                           objCons.PA1 = PA1!
                       }
                       if(PD1 == nil){
                           objCons.PD1 = ""
                       }else{
                           objCons.PD1 = PD1!
                       }
                       if(OP1 == nil){
                           objCons.OP1 = ""
                       }else{
                           objCons.OP1 = OP1!
                       }
                       if(SL1 == nil){
                           objCons.SL1 = ""
                       }else{
                           objCons.SL1 = SL1!
                       }
                       if(LN1 == nil){
                           objCons.LN1 = ""
                       }else{
                           objCons.LN1 = LN1!
                       }
                       
                       
                        
                        
                        obj.electricBillLNDetailsDB.append(objCons)
                    }
                }
                if(countSetTable7 > 0){
                    for i in 0...countSetTable7 - 1  {
                        
                        let objCons = ElectricOfficeDetailsDisplay()
                        let COKEY = accessorTable7[i]["COKEY"].text
                        let DCPHONE = accessorTable7[i]["DCPHONE"].text
                        let JEPHONE = accessorTable7[i]["JEPHONE"].text
                        let JENAME = accessorTable7[i]["JENAME"].text
                        let AENAME = accessorTable7[i]["AENAME"].text
                        let AEPHONE = accessorTable7[i]["AEPHONE"].text
                        if(COKEY == nil){
                            objCons.COKEY = ""
                        }else{
                            objCons.COKEY = COKEY!
                            cokey1 = COKEY!
                        }
                        if(DCPHONE == nil){
                            objCons.DCPHONE = ""
                        }else{
                            objCons.DCPHONE = DCPHONE!
                        }
                        if(JEPHONE == nil){
                            objCons.JEPHONE = ""
                        }else{
                            objCons.JEPHONE = JEPHONE!
                        }
                        if(JENAME == nil){
                            objCons.JENAME = ""
                        }else{
                            objCons.JENAME = JENAME!
                        }
                        if(AENAME == nil){
                            objCons.AENAME = ""
                        }else{
                            objCons.AENAME = AENAME!
                        }
                        if(AEPHONE == nil){
                            objCons.AEPHONE = ""
                        }else{
                            objCons.AEPHONE = AEPHONE!
                        }
                        
                        
                        obj.electricOfficeDetailsDB.append(objCons)
                    }
                }
              
               
                
                if(countSet > 0){
                    for i in 0...5  {
                        
                        let objCons = ConsumptionDisplay()
                        let bm = accessor["BM" + String(i + 1)].text
                        let mr = accessor["MR" + String(i + 1)].text
                        let SM_CON = accessor["SM_CON" + String(i + 1)].text
                        let SM_MTR_STS = accessor["SM_MTR_STS" + String(i + 1)].text
                        if(bm == nil){
                            objCons.BM = ""
                        }else{
                            objCons.BM = bm!
                        }
                        if(mr == nil){
                            objCons.MR = ""
                        }else{
                            objCons.MR = mr!
                        }
                        if(SM_CON == nil){
                            objCons.SM_CON = ""
                        }else{
                            objCons.SM_CON = SM_CON!
                        }
                        if(SM_MTR_STS == nil){
                            objCons.SM_MTR_STS = ""
                        }else{
                            objCons.SM_MTR_STS = SM_MTR_STS!
                        }
                        
                        obj.consumptionDB.append(objCons)
                    }
                }
                if(countSetTable2 > 0){
                    for i in 0...countSetTable2 - 1  {
                        let objCons = ElectricBillDetailsDisplay()
                        let DOC_NO = accessorTable2[i]["DOC_NO"].text
                        let BILL_MNTH = accessorTable2[i]["BILL_MNTH"].text
                        let ISSUE_DT = accessorTable2[i]["ISSUE_DT"].text
                        let DUE_DT = accessorTable2[i]["DUE_DT"].text
                        let BP_FIRST = accessorTable2[i]["BP_FIRST"].text
                        let DUEDT_CHQ = accessorTable2[i]["DUEDT_CHQ"].text
                        if(DOC_NO == nil){
                            objCons.DOC_NO = ""
                        }else{
                            objCons.DOC_NO = DOC_NO!
                        }
                        if(BILL_MNTH == nil){
                            objCons.BILL_MNTH = ""
                        }else{
                            objCons.BILL_MNTH = BILL_MNTH!
                        }
                        if(ISSUE_DT == nil){
                            objCons.ISSUE_DT = ""
                        }else{
                            objCons.ISSUE_DT = ISSUE_DT!
                        }
                        if(DUE_DT == nil){
                            objCons.DUE_DT = ""
                        }else{
                            objCons.DUE_DT = DUE_DT!
                        }
                        if(DUEDT_CHQ == nil){
                            objCons.DUEDT_CHQ = ""
                        }else{
                            objCons.DUEDT_CHQ = DUEDT_CHQ!
                        }
                        if(BP_FIRST == nil){
                            objCons.BP_FIRST = ""
                        }else{
                            objCons.BP_FIRST = BP_FIRST!
                        }
                       
                        obj.electricBillDetailsDB.append(objCons)
                    }
                }
               
                success(dataArray as AnyObject)

              }

            
        }
        else{

        }
        
        
    }
    
    
}
class ComplainTypeModelDataAPI
{
    
    var reachablty = try! Reachability()
    
    
    func serviceCalling( obj : ComplaintRegisterViewController , success:@escaping (AnyObject)-> Void)
    {
        
       // obj.startLoading()
        reachablty = try! Reachability()
        if(reachablty.connection != .unavailable)
        {
            obj.startLoadingPK(view: obj.view, text: "Please Wait ....")
        
           
                 let dateFormatterInv = DateFormatter()
                 dateFormatterInv.timeZone = NSTimeZone.system
                 dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
            let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
              let invStrUnique = dateFormatterInv.string(from: Date())
              let request_no = invStrUnique + mobile
              let message = "MA_getcompsubtyp|1 |1||\(secID)|\(request_no)"
print(message)
                  let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
print(encode)


              AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_datatable", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
                obj.stopLoadingPK(view: obj.view)
               guard let respo = response.value else {
                   return
               }
                print(response.value)
               let xml = try! XML.parse(respo)
              let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_datatableResponse"]["MA_cons_datatableResult"]["diffgr:diffgram"]["DocumentElement"]["MA_getcompsubtyp"]
                var dataArray : [ComplaintDisplay] = []
                guard let countSet = accessor.all?.count else {
                  // obj.getBillDetailsData(bpno: bpNo)
                                 return
                 }
                if(countSet > 0){
                    for i in 0...countSet - 1  {
                        let child = accessor.all![i]
                        let obj = ComplaintDisplay()
                        obj.name = child.childElements[1].text!
                        obj.id = child.childElements[2].text!
                        
                        
                        dataArray.append(obj)
                    }
                }
               
                success(dataArray as AnyObject)

              }

            
        }
        else{

        }
        
        
    }
    
    
}
class FeedbackModelDataAPI
{
    
    var reachablty = try! Reachability()
    
    
    func serviceCalling( obj : FeedbackViewController ,bpNo : String, success:@escaping (AnyObject)-> Void)
    {
        
       // obj.startLoading()
        reachablty = try! Reachability()
        if(reachablty.connection != .unavailable)
        {
                            obj.startLoadingPK(view: obj.view, text: "please wait...")
                           
                           
                                 let dateFormatterInv = DateFormatter()
                                 dateFormatterInv.timeZone = NSTimeZone.system
                                 dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
                            let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
                              let invStrUnique = dateFormatterInv.string(from: Date())
                              let request_no = invStrUnique + bpNo
                              let message = "ZBAPI_BI_LTBILLDISP|\(bpNo)|||\(secID)|\(request_no)"
                print(message)
                                  let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
                print(encode)


                              AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_dataset", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
                              // print("Request: \(response.value)")
                               guard let respo = response.value else {
                                obj.stopLoadingPK(view: obj.view)
                                obj.getPaymentBillDetailsData(bpno: bpNo)
                                   return
                               }
                                obj.stopLoadingPK(view: obj.view)
                                print(response.value)
                               let xml = try! XML.parse(respo)
                              let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table6"]
                                let accessorTable2 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table2"]
                                let accessorTable1 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table1"]
                                 let accessorTable3 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table3"]
                                
                                let accessorTable4 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table4"]
                                let accessorTable5 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table5"]
                                 let accessorTable7 = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table7"]
                                
                                
                                
                                var dataArray : [ElectricMainDetailsDisplay] = []
                                guard let countSetTable1 = accessorTable1.all?.count else {
        //                         obj.stopLoadingPK(view: obj.view)
        //                         obj.getDetailsData(bpno: obj.txtConsumerNo.text!)
                                                return
                                }
                                guard let countSetTable2 = accessorTable2.all?.count else {
                                 // obj.getBillDetailsData(bpno: bpNo)
                                                return
                                }
                                guard let countSetTable3 = accessorTable3.all?.count else {
                                 // obj.getBillDetailsData(bpno: bpNo)
                                                return
                                }
                                guard let countSetTable4 = accessorTable4.all?.count else {
                                 // obj.getBillDetailsData(bpno: bpNo)
                                                return
                                }
                                guard let countSetTable5 = accessorTable5.all?.count else {
                                 // obj.getBillDetailsData(bpno: bpNo)
                                                return
                                }
                                guard let countSetTable7 = accessorTable7.all?.count else {
                                 // obj.getBillDetailsData(bpno: bpNo)
                                                return
                                }
                                guard let countSet = accessor.all?.count else {
                                  // obj.getBillDetailsData(bpno: bpNo)
                                                 return
                                 }
                                
                                if(countSetTable1 > 0){
                                    for i in 0...countSetTable1 - 1  {
                                        
                                         let objCons = ElectricMainDetailsDisplay()
                                         let RAOTXT = accessorTable1[i]["RAOTXT"].text
                                         let DIVTXT = accessorTable1[i]["DIVTXT"].text
                                         let BP_NO = accessorTable1[i]["BP_NO"].text
                                         let BP_NAME = accessorTable1[i]["BP_NAME"].text
                                         let ADD1 = accessorTable1[i]["ADD1"].text
                                         let ADD2 = accessorTable1[i]["ADD2"].text
                                         let ADD3 = accessorTable1[i]["ADD3"].text
                                         let RT_CT = accessorTable1[i]["RT_CT"].text
                                         let SD_1 = accessorTable1[i]["SD_1"].text
                                         let TEA = accessorTable1[i]["TEA"].text
                                         let SD_INT = accessorTable1[i]["SD_INT"].text
                                         let TEL_NUMBER = accessorTable1[i]["TEL_NUMBER"].text
                                        if(RAOTXT == nil){
                                            objCons.RAOTXT = ""
                                        }else{
                                            objCons.RAOTXT = RAOTXT!
                                        }
                                        if(DIVTXT == nil){
                                            objCons.DIVTXT = ""
                                        }else{
                                            objCons.DIVTXT = DIVTXT!
                                        }
                                        if(BP_NAME == nil){
                                            objCons.BP_NAME = ""
                                        }else{
                                            objCons.BP_NAME = BP_NAME!
                                        }
                                        if(BP_NO == nil){
                                            objCons.BP_NO = ""
                                        }else{
                                            objCons.BP_NO = BP_NO!
                                        }
                                        if(ADD1 == nil){
                                            objCons.ADD1 = ""
                                        }else{
                                            objCons.ADD1 = ADD1!
                                        }
                                        if(ADD2 == nil){
                                            objCons.ADD2 = ""
                                        }else{
                                            objCons.ADD2 = ADD2!
                                        }
                                        if(ADD3 == nil){
                                            objCons.ADD3 = ""
                                        }else{
                                            objCons.ADD3 = ADD3!
                                        }
                                        if(RT_CT == nil){
                                            objCons.RT_CT = ""
                                        }else{
                                            objCons.RT_CT = RT_CT!
                                        }
                                        if(SD_1 == nil){
                                            objCons.SD_1 = ""
                                        }else{
                                            objCons.SD_1 = SD_1!
                                        }
                                        if(TEA == nil){
                                            objCons.TEA = ""
                                        }else{
                                            objCons.TEA = TEA!
                                        }
                                        if(SD_INT == nil){
                                            objCons.SD_INT = ""
                                        }else{
                                            objCons.SD_INT = SD_INT!
                                        }
                                        if(TEL_NUMBER == nil){
                                            objCons.TEL_NUMBER = ""
                                        }else{
                                            objCons.TEL_NUMBER = TEL_NUMBER!
                                        }
                                       
                                        dataArray.append(objCons)
                                    }
                                }
                               
                                if(countSetTable3 > 0){
                                    for i in 0...countSetTable3 - 1  {
                                        let objCons = ElectricBillUnitDetailsDisplay()
                                        let CON_LD = accessorTable3[i]["CON_LD"].text
                                        let CON_LOAD = accessorTable3[i]["CON_LOAD"].text
                                        let CR_RD = accessorTable3[i]["CR_RD"].text
                                        let PR_RD = accessorTable3[i]["PR_RD"].text
                                        let CONS = accessorTable3[i]["CONS"].text
                                        let MD = accessorTable3[i]["MD"].text
                                        
                                        let KWH_ON = accessorTable3[i]["KWH_ON"].text
                                        let PF = accessorTable3[i]["PF"].text
                                        let CONS_TEXT = accessorTable3[i]["CONS_TEXT"].text
                                        let SOLAR_UNIT = accessorTable3[i]["SOLAR_UNIT"].text
                                        let BILLING_TYPE = accessorTable3[i]["BILLING_TYPE"].text
                                        let PURPOSE = accessorTable3[i]["PURPOSE"].text
                                        let MF = accessorTable3[i]["MF"].text
                                        let ASSD_UNIT = accessorTable3[i]["ASSD_UNIT"].text
                                        let TOT_UNIT = accessorTable3[i]["TOT_UNIT"].text
                                        let AVRG_QTY = accessorTable3[i]["AVRG_QTY"].text
                                        if(CON_LD == nil){
                                            objCons.CON_LD = ""
                                        }else{
                                            objCons.CON_LD = CON_LD!
                                        }
                                        if(CON_LOAD == nil){
                                            objCons.CON_LOAD = ""
                                        }else{
                                            objCons.CON_LOAD = CON_LOAD!
                                        }
                                        if(CR_RD == nil){
                                            objCons.CR_RD = ""
                                        }else{
                                            objCons.CR_RD = CR_RD!
                                        }
                                        if(PR_RD == nil){
                                            objCons.PR_RD = ""
                                        }else{
                                            objCons.PR_RD = PR_RD!
                                        }
                                        if(CONS == nil){
                                            objCons.CONS = ""
                                        }else{
                                            objCons.CONS = CONS!
                                        }
                                        if(MD == nil){
                                            objCons.MD = ""
                                        }else{
                                            objCons.MD = MD!
                                        }
                                        if(KWH_ON == nil){
                                            objCons.KWH_ON = ""
                                        }else{
                                            objCons.KWH_ON = KWH_ON!
                                        }
                                        if(PF == nil){
                                            objCons.PF = ""
                                        }else{
                                            objCons.PF = PF!
                                        }
                                        if(CONS_TEXT == nil){
                                            objCons.CONS_TEXT = ""
                                        }else{
                                            objCons.CONS_TEXT = CONS_TEXT!
                                        }
                                        if(SOLAR_UNIT == nil){
                                            objCons.SOLAR_UNIT = ""
                                        }else{
                                            objCons.SOLAR_UNIT = SOLAR_UNIT!
                                        }
                                        if(BILLING_TYPE == nil){
                                            objCons.BILLING_TYPE = ""
                                        }else{
                                            objCons.BILLING_TYPE = BILLING_TYPE!
                                        }
                                        if(PURPOSE == nil){
                                            objCons.PURPOSE = ""
                                        }else{
                                            objCons.PURPOSE = PURPOSE!
                                        }
                                        if(MF == nil){
                                            objCons.MF = ""
                                        }else{
                                            objCons.MF = MF!
                                        }
                                        if(ASSD_UNIT == nil){
                                            objCons.ASSD_UNIT = ""
                                        }else{
                                            objCons.ASSD_UNIT = ASSD_UNIT!
                                        }
                                        if(TOT_UNIT == nil){
                                            objCons.TOT_UNIT = ""
                                        }else{
                                            objCons.TOT_UNIT = TOT_UNIT!
                                        }
                                        if(AVRG_QTY == nil){
                                            objCons.AVRG_QTY = ""
                                        }else{
                                            objCons.AVRG_QTY = AVRG_QTY!
                                        }
                                     
                                        obj.electricUnitsDetailsDB.append(objCons)
                                    }
                                }
                               if(countSetTable4 > 0){
                                    for i in 0...countSetTable4 - 1  {
                                        
                                        let objCons = ElectricBillBPDataDetailsDisplay()
                                        let EC_1 = accessorTable4[i]["EC_1"].text
                                        let FIX_CH_1 = accessorTable4[i]["FIX_CH_1"].text
                                        let FCA_1 = accessorTable4[i]["FCA_1"].text
                                        let BP_NAME_QUOTES = accessorTable4[i]["BP_NAME_QUOTES"].text
                                        let ADD1_QUOTES = accessorTable4[i]["ADD1_QUOTES"].text
                                        let CESS_1 = accessorTable4[i]["CESS_1"].text
                                        let ADD2_QUOTES = accessorTable4[i]["ADD2_QUOTES"].text
                                        let MTR_1 = accessorTable4[i]["MTR_1"].text
                                        let ADD3_QUOTES = accessorTable4[i]["ADD3_QUOTES"].text
                                        let WTCS_1 = accessorTable4[i]["WTCS_1"].text
                                        let DIVTXT_QUOTES = accessorTable4[i]["DIVTXT_QUOTES"].text
                                        let ASD_1 = accessorTable4[i]["ASD_1"].text
                                        let REB_1 = accessorTable4[i]["REB_1"].text
                                        let ADJ_DMD_1 = accessorTable4[i]["ADJ_DMD_1"].text
                                        let AVG_ADJ_1 = accessorTable4[i]["AVG_ADJ_1"].text
                                        let MIS_1 = accessorTable4[i]["MIS_1"].text
                                        let PRE_ARR_1 = accessorTable4[i]["PRE_ARR_1"].text
                                        let TOT_BILL_1 = accessorTable4[i]["TOT_BILL_1"].text
                                        let TOT_AMT_1 = accessorTable4[i]["TOT_AMT_1"].text
                                        let AMT = accessorTable4[i]["AMT"].text
                                        let SOLAR_RBT = accessorTable4[i]["SOLAR_RBT"].text
                                        let SG400U_50PR = accessorTable4[i]["SG400U_50PR"].text
                                        let NET_BILL = accessorTable4[i]["NET_BILL"].text
                                        let ED_1 = accessorTable4[i]["ED_1"].text
                                        if(ED_1 == nil){
                                            objCons.ED_1 = ""
                                        }else{
                                            objCons.ED_1 = ED_1!
                                        }
                                        
                                        if(EC_1 == nil){
                                            objCons.EC_1 = ""
                                        }else{
                                            objCons.EC_1 = EC_1!
                                        }
                                        if(FIX_CH_1 == nil){
                                            objCons.FIX_CH_1 = ""
                                        }else{
                                            objCons.FIX_CH_1 = FIX_CH_1!
                                        }
                                        if(FCA_1 == nil){
                                            objCons.FCA_1 = ""
                                        }else{
                                            objCons.FCA_1 = FCA_1!
                                        }
                                        if(BP_NAME_QUOTES == nil){
                                            objCons.BP_NAME_QUOTES = ""
                                        }else{
                                            objCons.BP_NAME_QUOTES = BP_NAME_QUOTES!
                                        }
                                        if(ADD1_QUOTES == nil){
                                            objCons.ADD1_QUOTES = ""
                                        }else{
                                            objCons.ADD1_QUOTES = ADD1_QUOTES!
                                        }
                                        if(CESS_1 == nil){
                                            objCons.CESS_1 = ""
                                        }else{
                                            objCons.CESS_1 = CESS_1!
                                        }
                                        if(ADD2_QUOTES == nil){
                                            objCons.ADD2_QUOTES = ""
                                        }else{
                                            objCons.ADD2_QUOTES = ADD2_QUOTES!
                                        }
                                        if(MTR_1 == nil){
                                            objCons.MTR_1 = ""
                                        }else{
                                            objCons.MTR_1 = MTR_1!
                                        }
                                        if(ADD3_QUOTES == nil){
                                            objCons.ADD3_QUOTES = ""
                                        }else{
                                            objCons.ADD3_QUOTES = ADD3_QUOTES!
                                        }
                                        if(WTCS_1 == nil){
                                            objCons.WTCS_1 = ""
                                        }else{
                                            objCons.WTCS_1 = WTCS_1!
                                        }
                                        if(DIVTXT_QUOTES == nil){
                                            objCons.DIVTXT_QUOTES = ""
                                        }else{
                                            objCons.DIVTXT_QUOTES = DIVTXT_QUOTES!
                                        }
                                        if(ASD_1 == nil){
                                            objCons.ASD_1 = ""
                                        }else{
                                            objCons.ASD_1 = ASD_1!
                                        }
                                        if(REB_1 == nil){
                                            objCons.REB_1 = ""
                                        }else{
                                            objCons.REB_1 = REB_1!
                                        }
                                        if(ADJ_DMD_1 == nil){
                                            objCons.ADJ_DMD_1 = ""
                                        }else{
                                            objCons.ADJ_DMD_1 = ADJ_DMD_1!
                                        }
                                        if(AVG_ADJ_1 == nil){
                                            objCons.AVG_ADJ_1 = ""
                                        }else{
                                            objCons.AVG_ADJ_1 = AVG_ADJ_1!
                                        }
                                        if(MIS_1 == nil){
                                            objCons.MIS_1 = ""
                                        }else{
                                            objCons.MIS_1 = MIS_1!
                                        }
                                        if(PRE_ARR_1 == nil){
                                            objCons.PRE_ARR_1 = ""
                                        }else{
                                            objCons.PRE_ARR_1 = PRE_ARR_1!
                                        }
                                        if(TOT_BILL_1 == nil){
                                            objCons.TOT_BILL_1 = ""
                                        }else{
                                            objCons.TOT_BILL_1 = TOT_BILL_1!
                                        }
                                        if(TOT_AMT_1 == nil){
                                            objCons.TOT_AMT_1 = ""
                                        }else{
                                            objCons.TOT_AMT_1 = TOT_AMT_1!
                                        }
                                        if(AMT == nil){
                                            objCons.AMT = ""
                                        }else{
                                            objCons.AMT = AMT!
                                        }
                                        if(SOLAR_RBT == nil){
                                            objCons.SOLAR_RBT = ""
                                        }else{
                                            objCons.SOLAR_RBT = SOLAR_RBT!
                                        }
                                        if(SG400U_50PR == nil){
                                            objCons.SG400U_50PR = ""
                                        }else{
                                            objCons.SG400U_50PR = SG400U_50PR!
                                        }
                                        if(NET_BILL == nil){
                                            objCons.NET_BILL = ""
                                        }else{
                                            objCons.NET_BILL = NET_BILL!
                                        }
                                      
                                        
                                        
                                        obj.electricBillBPDetailsDB.append(objCons)
                                    }
                                }
                                if(countSetTable5 > 0){
                                    for i in 0...countSetTable5 - 1  {
                                        
                                        let objCons = ElectricBillLNDetailsDisplay()
                                        let TI1 = accessorTable5[i]["TI1"].text
                                        let EN1 = accessorTable5[i]["EN1"].text
                                        let PA1 = accessorTable5[i]["PA1"].text
                                        let PD1 = accessorTable5[i]["PD1"].text
                                        let OP1 = accessorTable5[i]["OP1"].text
                                        let SL1 = accessorTable5[i]["SL1"].text
                                        let LN1 = accessorTable5[i]["LN1"].text
                                        
                                       if(TI1 == nil){
                                           objCons.TI1 = ""
                                       }else{
                                           objCons.TI1 = TI1!
                                       }
                                       if(EN1 == nil){
                                           objCons.EN1 = ""
                                       }else{
                                           objCons.EN1 = EN1!
                                       }
                                       if(PA1 == nil){
                                           objCons.PA1 = ""
                                       }else{
                                           objCons.PA1 = PA1!
                                       }
                                       if(PD1 == nil){
                                           objCons.PD1 = ""
                                       }else{
                                           objCons.PD1 = PD1!
                                       }
                                       if(OP1 == nil){
                                           objCons.OP1 = ""
                                       }else{
                                           objCons.OP1 = OP1!
                                       }
                                       if(SL1 == nil){
                                           objCons.SL1 = ""
                                       }else{
                                           objCons.SL1 = SL1!
                                       }
                                       if(LN1 == nil){
                                           objCons.LN1 = ""
                                       }else{
                                           objCons.LN1 = LN1!
                                       }
                                       
                                       
                                        
                                        
                                        obj.electricBillLNDetailsDB.append(objCons)
                                    }
                                }
                                if(countSetTable7 > 0){
                                    for i in 0...countSetTable7 - 1  {
                                        
                                        let objCons = ElectricOfficeDetailsDisplay()
                                        let COKEY = accessorTable7[i]["COKEY"].text
                                        let DCPHONE = accessorTable7[i]["DCPHONE"].text
                                        let JEPHONE = accessorTable7[i]["JEPHONE"].text
                                        let JENAME = accessorTable7[i]["JENAME"].text
                                        let AENAME = accessorTable7[i]["AENAME"].text
                                        let AEPHONE = accessorTable7[i]["AEPHONE"].text
                                        if(COKEY == nil){
                                            objCons.COKEY = ""
                                        }else{
                                            objCons.COKEY = COKEY!
                                        }
                                        if(DCPHONE == nil){
                                            objCons.DCPHONE = ""
                                        }else{
                                            objCons.DCPHONE = DCPHONE!
                                        }
                                        if(JEPHONE == nil){
                                            objCons.JEPHONE = ""
                                        }else{
                                            objCons.JEPHONE = JEPHONE!
                                        }
                                        if(JENAME == nil){
                                            objCons.JENAME = ""
                                        }else{
                                            objCons.JENAME = JENAME!
                                        }
                                        if(AENAME == nil){
                                            objCons.AENAME = ""
                                        }else{
                                            objCons.AENAME = AENAME!
                                        }
                                        if(AEPHONE == nil){
                                            objCons.AEPHONE = ""
                                        }else{
                                            objCons.AEPHONE = AEPHONE!
                                        }
                                        
                                        
                                        obj.electricOfficeDetailsDB.append(objCons)
                                    }
                                }
                              
                               
                                
                                if(countSet > 0){
                                    for i in 0...5  {
                                        
                                        let objCons = ConsumptionDisplay()
                                        let bm = accessor["BM" + String(i + 1)].text
                                        let mr = accessor["MR" + String(i + 1)].text
                                        let SM_CON = accessor["SM_CON" + String(i + 1)].text
                                        let SM_MTR_STS = accessor["SM_MTR_STS" + String(i + 1)].text
                                        if(bm == nil){
                                            objCons.BM = ""
                                        }else{
                                            objCons.BM = bm!
                                        }
                                        if(mr == nil){
                                            objCons.MR = ""
                                        }else{
                                            objCons.MR = mr!
                                        }
                                        if(SM_CON == nil){
                                            objCons.SM_CON = ""
                                        }else{
                                            objCons.SM_CON = SM_CON!
                                        }
                                        if(SM_MTR_STS == nil){
                                            objCons.SM_MTR_STS = ""
                                        }else{
                                            objCons.SM_MTR_STS = SM_MTR_STS!
                                        }
                                        
                                        obj.consumptionDB.append(objCons)
                                    }
                                }
                                if(countSetTable2 > 0){
                                    for i in 0...countSetTable2 - 1  {
                                        let objCons = ElectricBillDetailsDisplay()
                                        let DOC_NO = accessorTable2[i]["DOC_NO"].text
                                        let BILL_MNTH = accessorTable2[i]["BILL_MNTH"].text
                                        let ISSUE_DT = accessorTable2[i]["ISSUE_DT"].text
                                        let DUE_DT = accessorTable2[i]["DUE_DT"].text
                                        let BP_FIRST = accessorTable2[i]["BP_FIRST"].text
                                        let DUEDT_CHQ = accessorTable2[i]["DUEDT_CHQ"].text
                                        if(DOC_NO == nil){
                                            objCons.DOC_NO = ""
                                        }else{
                                            objCons.DOC_NO = DOC_NO!
                                        }
                                        if(BILL_MNTH == nil){
                                            objCons.BILL_MNTH = ""
                                        }else{
                                            objCons.BILL_MNTH = BILL_MNTH!
                                        }
                                        if(ISSUE_DT == nil){
                                            objCons.ISSUE_DT = ""
                                        }else{
                                            objCons.ISSUE_DT = ISSUE_DT!
                                        }
                                        if(DUE_DT == nil){
                                            objCons.DUE_DT = ""
                                        }else{
                                            objCons.DUE_DT = DUE_DT!
                                        }
                                        if(DUEDT_CHQ == nil){
                                            objCons.DUEDT_CHQ = ""
                                        }else{
                                            objCons.DUEDT_CHQ = DUEDT_CHQ!
                                        }
                                        if(BP_FIRST == nil){
                                            objCons.BP_FIRST = ""
                                        }else{
                                            objCons.BP_FIRST = BP_FIRST!
                                        }
                                       
                                        obj.electricBillDetailsDB.append(objCons)
                                    }
                                }
                               
                                success(dataArray as AnyObject)

                              }

                            
                        }
        else{

        }
        
        
    }
    
    
}
class ConsumptionModelDataAPI
{
    
    var reachablty = try! Reachability()
    
    
    func serviceCalling( obj : ConsumptionPatternViewController ,bpNo : String, success:@escaping (AnyObject)-> Void)
    {
        
       // obj.startLoading()
        reachablty = try! Reachability()
        if(reachablty.connection != .unavailable)
        {
            obj.startLoadingPK(view: obj.view, text: "Please Wait ....")
          
                 let dateFormatterInv = DateFormatter()
                 dateFormatterInv.timeZone = NSTimeZone.system
                 dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
            let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
              let invStrUnique = dateFormatterInv.string(from: Date())
              let request_no = invStrUnique + bpNo
              let message = "ZBAPI_BI_LTBILLDISP|\(bpNo)|||\(secID)|\(request_no)"
print(message)
                  let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
print(encode)


              AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_dataset", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
                obj.stopLoadingPK(view: obj.view)
               guard let respo = response.value else {
                   return
               }
                print(response.value)
               let xml = try! XML.parse(respo)
              let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table6"]
                var dataArray : [ConsumptionDisplay] = []
                guard let countSet = accessor.all?.count else {
                  // obj.getBillDetailsData(bpno: bpNo)
                                 return
                 }
                if(countSet > 0){
                    for i in 0...5  {
                        let child = accessor.all![0]
                        let objCons = ConsumptionDisplay()
                        let bm = accessor["BM" + String(i + 1)].text
                        let mr = accessor["MR" + String(i + 1)].text
                        let SM_CON = accessor["SM_CON" + String(i + 1)].text
                        let SM_MTR_STS = accessor["SM_MTR_STS" + String(i + 1)].text
                        if(bm == nil){
                            objCons.BM = ""
                        }else{
                            objCons.BM = bm!
                        }
                        if(mr == nil){
                            objCons.MR = ""
                        }else{
                            objCons.MR = mr!
                        }
                        if(SM_CON == nil){
                            objCons.SM_CON = ""
                        }else{
                            objCons.SM_CON = SM_CON!
                        }
                        if(SM_MTR_STS == nil){
                            objCons.SM_MTR_STS = ""
                        }else{
                            objCons.SM_MTR_STS = SM_MTR_STS!
                        }
                        
                        dataArray.append(objCons)
                    }
                }
               
                success(dataArray as AnyObject)

              }

            
        }
        else{

        }
        
        
    }
    
    
}

class PaymentHistoryModelDataAPI
{
    
    var reachablty = try! Reachability()
    
    
    func serviceCalling( obj : PaymentHistoryViewController ,bpNo : String, success:@escaping (AnyObject)-> Void)
    {
   
       // obj.startLoading()
        reachablty = try! Reachability()
        if(reachablty.connection != .unavailable)
        {
            if(obj.cntdata == 0){
            obj.startLoadingPK(view: obj.view, text: "Please Wait ....")
            }
          
                 let dateFormatterInv = DateFormatter()
                 dateFormatterInv.timeZone = NSTimeZone.system
                 dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
            let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
              let invStrUnique = dateFormatterInv.string(from: Date())
              let request_no = invStrUnique + bpNo
              let message = "ZBAPI_ISU_WEBPAYMENT_LT|\(bpNo)|\(secID)|\(request_no)"
print(message)
                  let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
print(encode)


              AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_dataset", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
              
               guard let respo = response.value else {
                obj.cntdata = obj.cntdata + 1
                if(obj.cntdata < 3){
                    obj.stopLoadingPK(view: obj.view)
                 obj.getPaymentDetailsData(bpno: bpNo)
                }
                   return
               }
                print(response.value)
               let xml = try! XML.parse(respo)
              let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table2"]
                var dataArray : [PaymentHistoryDisplay] = []
                guard let countSet = accessor.all?.count else {
                  // obj.getBillDetailsData(bpno: bpNo)
                    obj.cntdata = obj.cntdata + 1
                    if(obj.cntdata < 3){
                        obj.stopLoadingPK(view: obj.view)
                     obj.getPaymentDetailsData(bpno: bpNo)
                    }
                                 return
                 }
                obj.cntdata = 0
                  obj.stopLoadingPK(view: obj.view)
                if(countSet > 0){
                    for i in 0...5 {
                      
                        let objCons = PaymentHistoryDisplay()
                        let LVMONTH = accessor[i]["LVMONTH"].text
                        let LVPAYDOC_NO = accessor[i]["LVPAYDOC_NO"].text
                        let LV_SNO = accessor[i]["LV_SNO"].text
                        let LV_DOC_NO = accessor[i]["LV_DOC_NO"].text
                        let LVPAY = accessor[i]["LVPAY"].text
                        if(LVMONTH == nil){
                            objCons.LVMONTH = ""
                        }else{
                            objCons.LVMONTH = LVMONTH!
                        }
                        if(LVPAYDOC_NO == nil){
                            objCons.LVPAYDOC_NO = ""
                        }else{
                            objCons.LVPAYDOC_NO = LVPAYDOC_NO!
                        }
                        if(LV_SNO == nil){
                            objCons.LV_SNO = ""
                        }else{
                            objCons.LV_SNO = LV_SNO!
                        }
                        if(LV_DOC_NO == nil){
                            objCons.LV_DOC_NO = ""
                        }else{
                            objCons.LV_DOC_NO = LV_DOC_NO!
                        }
                        if(LVPAY == nil){
                            objCons.LVPAY = 0
                        }else{
                            objCons.LVPAY = Int(Double(LVPAY!)!)
                        }
                        if(LVMONTH != nil){
                        dataArray.append(objCons)
                        }
                    }
                }
               
                success(dataArray as AnyObject)

              }

            
        }
        else{

        }
        
        
    }
    
    
}
class RebateModelDataAPI
{
    
    var reachablty = try! Reachability()
    
    
    func serviceCalling( obj : RebateBijiliViewController ,bpNo : String, success:@escaping (AnyObject)-> Void)
    {
   
       // obj.startLoading()
        reachablty = try! Reachability()
        if(reachablty.connection != .unavailable)
        {
            obj.startLoadingPK(view: obj.view, text: "Please Wait ....")
          
                 let dateFormatterInv = DateFormatter()
                 dateFormatterInv.timeZone = NSTimeZone.system
                 dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
            let dateFormatterInv1 = DateFormatter()
            dateFormatterInv1.timeZone = NSTimeZone.system
            dateFormatterInv1.dateFormat = "yyyyMM"
            let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
            let monthYear = dateFormatterInv1.string(from: Date()) + "25"
              let invStrUnique = dateFormatterInv.string(from: Date())
              let request_no = invStrUnique + bpNo
              let message = "ZBAPI_MORBIJLI_APP_SR_REBATE|\(bpNo)|20190325|\(monthYear)|\(secID)|\(request_no)"
print(message)
                  let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
print(encode)


              AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_dataset", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
                obj.stopLoadingPK(view: obj.view)
               guard let respo = response.value else {
                   return
               }
                print(response.value)
               let xml = try! XML.parse(respo)
              let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_datasetResponse"]["MA_cons_datasetResult"]["diffgr:diffgram"]["NewDataSet"]["Table1"]
                var dataArray : [RebateDisplay] = []
                guard let countSet = accessor.all?.count else {
                  // obj.getBillDetailsData(bpno: bpNo)
                   return
                 }
                if(countSet > 0){
                    for i in 0...5 {
                     
                        let objCons = RebateDisplay()
                        let BUSPARTNER = accessor[(countSet - 1) - i]["BUSPARTNER"].text
                        let ZBUDAT = accessor[(countSet - 1) - i]["ZBUDAT"].text
                        let TARIF = accessor[(countSet - 1) - i]["TARIF"].text
                        let EC = accessor[(countSet - 1) - i]["EC"].text
                        let FC = accessor[(countSet - 1) - i]["FC"].text
                        let VCA = accessor[(countSet - 1) - i]["VCA"].text
                        var TOT_CHG = accessor[(countSet - 1) - i]["TOT_CHG"].text
                        let TOT_REBATE = accessor[(countSet - 1) - i]["TOT_REBATE"].text
                        if(TOT_REBATE != nil){
                            if(BUSPARTNER == nil){
                                objCons.BUSPARTNER = ""
                            }else{
                                objCons.BUSPARTNER = BUSPARTNER!
                            }
                            if(ZBUDAT == nil){
                                objCons.ZBUDAT = ""
                            }else{
                                objCons.ZBUDAT = ZBUDAT!
                            }
                            if(TARIF == nil){
                                objCons.TARIF = ""
                            }else{
                                objCons.TARIF = TARIF!
                            }
                            if(EC == nil){
                                objCons.EC = ""
                            }else{
                                objCons.EC = EC!
                            }
                            if(FC == nil){
                                objCons.FC = ""
                            }else{
                                objCons.FC = FC!
                            }
                            if(VCA == nil){
                                objCons.VCA = ""
                            }else{
                                objCons.VCA = VCA!
                            }
                            if(TOT_CHG == nil){
                                objCons.TOT_CHG = 0
                            }else{
                                let str = TOT_CHG
                                if str!.contains("-") {
                                    let r1 = str!.components(separatedBy: "-")
                                    TOT_CHG = r1[0]
                                   
                                }
                                
                                
                                
                                let doub = Double(TOT_CHG!)!
                                let round = Int((doub.roundToDecimal(0)))
                                objCons.TOT_CHG = round
                            }
                            if(TOT_REBATE == nil){
                                objCons.TOT_REBATE = 0
                            }else{
                                let totRebate = TOT_REBATE!.split(separator: "-")
                                let tot = totRebate[0]
                                
                                let doub = Double(tot)!
                                let round = Int((doub.roundToDecimal(0)))
                                objCons.TOT_REBATE = round
                            }
                       
                        dataArray.append(objCons)
                        }
                    }
                }
               
                success(dataArray as AnyObject)

              }

            
        }
        else{

        }
        
        
    }
    
    
}


class ComplaintDisplay :  NSObject {

   var name = String()
    var id = String()
  
    
}
class BillDisplay :  NSObject {

   var name = String()
    var mobile = String()
    var billno = String()
    var address = String()
       var amount = String()
    var lastDate = String()
    var lastMon = String()
    
}
class ConsumptionDisplay :  NSObject {

   var BM = String()
    var MR = String()
    var SM_CON = String()
    var SM_MTR_STS = String()
   
    
}
class PaymentHistoryDisplay :  NSObject {

   var LVMONTH = String()
    var LVPAYDOC_NO = String()
    var LV_SNO = String()
    var LV_DOC_NO = String()
    var LVPAY = Int()
   
    
}
class RebateDisplay :  NSObject {

   var BUSPARTNER = String()
    var ZBUDAT = String()
    var TARIF = String()
    var EC = String()
    var FC = String()
    var VCA = String()
    var TOT_CHG = Int()
    var TOT_REBATE = Int()
    
   
    
}

/// Electricity bill model

class ElectricBillDetailsDisplay :  NSObject {

   var DOC_NO = String()
    var BILL_MNTH = String()
    var ISSUE_DT = String()
    var DUE_DT = String()
    var BP_FIRST = String()
    var DUEDT_CHQ = String()
   
    
   
    
}
class ElectricMainDetailsDisplay :  NSObject {

   var RAOTXT = String()
    var DIVTXT = String()
    var BP_NO = String()
    var BP_NAME = String()
    var ADD1 = String()
    var ADD2 = String()
    var ADD3 = String()
   
    var RT_CT = String()
    var SD_1 = String()
    var TEA = String()
    var SD_INT = String()
    var TEL_NUMBER = String()
  
   
    
}
class ElectricBillUnitDetailsDisplay :  NSObject {

   var CON_LD = String()
    var CON_LOAD = String()
    var CR_RD = String()
    var PR_RD = String()
    var CONS = String()
    var MD = String()
    var KWH_ON = String()
    var PF = String()
    var CONS_TEXT = String()
    var SOLAR_UNIT = String()
    var BILLING_TYPE = String()
    var PURPOSE = String()
    
    var MF = String()
       var ASSD_UNIT = String()
       var TOT_UNIT = String()
       var AVRG_QTY = String()
  
   
    
}
class ElectricBillBPDataDetailsDisplay :  NSObject {

   var EC_1 = String()
    var FIX_CH_1 = String()
    var FCA_1 = String()
    var BP_NAME_QUOTES = String()
    var ED_1 = String()
    var ADD1_QUOTES = String()
    var CESS_1 = String()
    var ADD2_QUOTES = String()
    var MTR_1 = String()
    var ADD3_QUOTES = String()
    var WTCS_1 = String()
    var DIVTXT_QUOTES = String()
    
    var ASD_1 = String()
       var REB_1 = String()
       var ADJ_DMD_1 = String()
       var AVG_ADJ_1 = String()
       var MIS_1 = String()
    
    var PRE_ARR_1 = String()
    var TOT_BILL_1 = String()
    var TOT_AMT_1 = String()
    var AMT = String()
    var SOLAR_RBT = String()
    var SG400U_50PR = String()
    var NET_BILL = String()
   
    
}
class ElectricBillLNDetailsDisplay :  NSObject {

   var LN1 = String()
    var SL1 = String()
    var OP1 = String()
    var PD1 = String()
    var PA1 = String()
    var EN1 = String()
    var TI1 = String()
   
}
class ElectricOfficeDetailsDisplay :  NSObject {

   var COKEY = String()
    var DCPHONE = String()
    var JEPHONE = String()
    var JENAME = String()
    var AENAME = String()
    var AEPHONE = String()
  
   
}


extension Double {
    func roundToNearestValue(value: Double) -> Double {
        let remainder = self.truncatingRemainder(dividingBy: value)
        let shouldRoundUp = remainder >= value/2 ? true : false
        let multiple = floor(self / value)
        let returnValue = !shouldRoundUp ? value * multiple : value * multiple + value
        return returnValue
    }
    func roundToDecimal(_ fractionDigits: Int) -> Double {
        let multiplier = pow(10, Double(fractionDigits))
        return Darwin.round(self * multiplier) / multiplier
    }
}
