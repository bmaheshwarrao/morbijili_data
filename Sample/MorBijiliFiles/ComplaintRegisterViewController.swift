//
//  ComplaintRegisterViewController.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 12/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import AlamofireSoap
import SwiftyXMLParser
var cokey1 = String()
class ComplaintRegisterViewController: CommonVSClass {
   
    
 //MARK: ================== IBOutlets ================
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: ================== Properties ================
     let dropDown = DropDown()
    var BillDetailsAPI = ComplaintModelDataAPI()
    var billDetailsDB : [BillDisplay] = []
    var ComplaintDetailsAPI = ComplainTypeModelDataAPI()
    var cellData : MainMorPartnerTableViewCell!
    var complaintDetailsDB : [ComplaintDisplay] = []
    var complaintName : [String] = []
    var complaintID : [String] = []
    var complaintValueID = String()
     var titleSet = String()
    var PartnerDB:[Partner] = []
    var partnerBDValue : [String] = []
    var cellPhotoData : PhotoMeterTableViewCell!
    var cellDataText : MorDetailsTableViewCell!
    var cellDropData : MorDropDetailsTableViewCell!
    var cellTextData : MorDetailsTableViewCell!
    var consumptionDB : [ConsumptionDisplay] = []
    var electricBillDetailsDB : [ElectricBillDetailsDisplay] = []
    var electricMainDetailsDB : [ElectricMainDetailsDisplay] = []
    var electricUnitsDetailsDB : [ElectricBillUnitDetailsDisplay] = []
    var electricBillBPDetailsDB : [ElectricBillBPDataDetailsDisplay] = []
    var electricBillLNDetailsDB : [ElectricBillLNDetailsDisplay] = []
    var electricOfficeDetailsDB : [ElectricOfficeDetailsDisplay] = []
    let imagePicker = UIImagePickerController()
    var imageData : Data? = nil
    var cnt = 0
    var section1 : [String] = ["Name","Address","Actual Address","Mobile No","Complaint Type"]
    var section2 : [String] = ["Name","Address","Actual Address","Mobile No","Complaint Type"]
    var meterReadingType : [String] = ["गलत रीडिंग होना","समय पर रीडिंग न होना ","मीटर रीडिंग के समय घर बंद होना "]
    
    
    //MARK: ================== VC Life Cycle ================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        getPartnerData()
        getComplaintDetailsData()
        self.title = titleSet
        if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
                section1  = ["Name","Address"]
                section2 = ["Mobile No","Landmark \n(Optional)","Supply related \nComplaint Type"]
                }else{
            section1  = ["नाम","पता"]
            section2 = ["मोबाइल नंबर","विस्तृत पता \nवैकल्पिक","कंप्लेंट श्रेणी"]
                 }
        // Do any additional setup after loading the view.
    }
      //MARK: ================== IB ACtions ================
   
    @IBAction func btnSubmitMeterClicked(_ sender: UIButton) {
        
       // self.complaintAdd()
        
        if(cellTextData.textViewData.text == ""){
            self.view.makeToastHZL(enterMeter)
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {

                       let actionSheetController: UIAlertController = UIAlertController(title: "Alert", message: self.addSureMeter, preferredStyle: .actionSheet)

                       let camera = UIAlertAction(title: self.yess, style: .default, handler: { (alert) in
                                       self.complaintAdd()

                                   })

                       let cancel = UIAlertAction(title: self.nooo, style: .cancel, handler: { (alert) in

                                   })

                                   actionSheetController.addAction(camera)

                                   actionSheetController.addAction(cancel)
                                   self.present(actionSheetController, animated: true, completion: nil)


                               }
        }

        
        
    }
    var cokey = String()
    func complaintAdd(){
        self.startLoadingPK(view: self.view, text: "Please wait")
            let dateFormatterInv = DateFormatter()
            dateFormatterInv.timeZone = NSTimeZone.system
            dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
            let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
            let invStrUnique = dateFormatterInv.string(from: Date())
            let request_no = invStrUnique + mobile
        
     //   MA_cons_createcomplaint|BP No.|Mobile No.|Complaint Type|IMA|1||Landmark|DC Code |Latitude|Longitude|Sec_Key|request_id
        
       
        let message = "MA_cons_createcomplaint|\(cellData.lblPartner.text!)|\(mobile)|\(cellDropData.lblType.text!)|IMA|1||\(cellTextData.textViewData.text!)|\(cokey1)|0.00|0.00|\(secID)|\(request_no)"
        print(message)
            let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
            
            AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_string", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
             self.stopLoadingPK(view: self.view)
            guard let respo = response.value else {
                return
            }
             print(response.value)
            let xml = try! XML.parse(respo)
            let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_stringResponse"]["MA_cons_stringResult"].text
            if let result = accessor {
                
                var str = result
                var complaintResultID = String()
                if str.contains("|") {
                    let r1 = str.components(separatedBy: "|")
                    str = r1[0]
                    complaintResultID = r1[1]
                }
                
                let resultInt = Int(str)
                if(resultInt! > 0){
                    if(resultInt == 1){
                    self.showSingleButtonAlertWithAction(title: self.regComp + complaintResultID, buttonTitle: "OK") {
                        self.navigationController?.popViewController(animated: true)
                    }
                    }else if(resultInt == 2){
                    self.showSingleButtonAlertWithAction(title: self.regCompAlready + complaintResultID, buttonTitle: "OK") {
                        self.navigationController?.popViewController(animated: true)
                    }
                    }else{
                        self.showSingleButtonWithMessage(title: self.confirmation, message: self.compStrNot, buttonName: "OK")
                    }
                    
                    
                   
                }else{
                    self.showSingleButtonWithMessage(title: self.confirmation, message: self.compStrNot, buttonName: "OK")
                }
            }
            }
            
            
            
           
        
        

    }
    @IBAction func btnTypeClicked(_ sender: UIButton) {
        
        self.dropDown.anchorView = cellDropData.btnType // UIView or UIBarButtonItem
              self.dropDown.width = cellDropData.btnType.frame.size.width
              self.dropDown.bottomOffset = CGPoint(x: 0, y:(self.dropDown.anchorView?.plainView.bounds.height)!)
              self.dropDown.direction = .bottom
              // The list of items to display. Can be changed dynamically
              self.dropDown.dataSource = complaintName
              self.dropDown.show()
              self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                  print("Selected tem: \(item) at index: \(index)")
                  self.cellDropData.lblType.text = item
                self.complaintValueID = self.complaintID[index]
              }
    }
    
    @IBAction func btnPartnerClicked(_ sender: UIButton) {
        self.dropDown.anchorView = cellData.btnPartner // UIView or UIBarButtonItem
        self.dropDown.width = cellData.btnPartner.frame.size.width
        self.dropDown.bottomOffset = CGPoint(x: 0, y:(self.dropDown.anchorView?.plainView.bounds.height)!)
        self.dropDown.direction = .bottom
        // The list of items to display. Can be changed dynamically
        self.dropDown.dataSource = partnerBDValue
        self.dropDown.show()
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected tem: \(item) at index: \(index)")
            self.cellData.lblPartner.text = item
            self.getPaymentBillDetailsData(bpno: item)
           
        }
    }
    
    //MARK: ================== Custom Methods ================
    
    @objc func getPartnerData() {
    
            self.PartnerDB = [Partner]()
            do {
    
                self.PartnerDB = try context.fetch(Partner.fetchRequest())
                //self.refresh.endRefreshing()
                if(self.PartnerDB.count > 0) {
                    for i in 0...self.PartnerDB.count - 1 {
                        partnerBDValue.append(self.PartnerDB[i].partner!)
                    }
                   
                }else{
                   
               }
                self.tableView.reloadData()
    
            } catch {
                print("Fetching Failed")
            }
        }
        @objc func getComplaintDetailsData(){
            
         
            
            ComplaintDetailsAPI.serviceCalling(obj: self) { (dict) in
                self.complaintName = []
                self.complaintID = []
                if(dict.count > 0){
                    self.complaintDetailsDB = dict as! [ComplaintDisplay]
                    for i in 0...self.complaintDetailsDB.count - 1 {
                        self.complaintName.append( self.complaintDetailsDB[i].name)
                        self.complaintID.append(self.complaintDetailsDB[i].id)
                    }
                    self.tableView.reloadData()
                }
//
            }
            
            
        }
    
    @objc func getPaymentBillDetailsData(bpno : String){
        
     
        
        BillDetailsAPI.serviceCalling(obj: self , bpNo : bpno) { (dict) in
            
          if(dict.count > 0){
                           self.electricMainDetailsDB = dict as! [ElectricMainDetailsDisplay]
                           self.tableView.reloadData()
            }
            
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: ================== Extension ================
extension ComplaintRegisterViewController: UITableViewDelegate,UITableViewDataSource {
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        }
        func numberOfSections(in tableView: UITableView) -> Int {
            return 6
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if(section == 0 ){
            return 1
            }else if(section == 1){
                if(electricMainDetailsDB.count > 0){
                return 2
                }
                return 0
            }else {
                if(electricMainDetailsDB.count > 0){
                return 1
                }
                return 0
            }
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if(indexPath.section == 0){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MainMorPartnerTableViewCell
            cellData = cell
            if(PartnerDB.count > 0){
                
                if(cnt == 0){
                    cell.lblPartner.text = partnerBDValue[indexPath.row]
                getPaymentBillDetailsData(bpno: partnerBDValue[0])
                }
         
            }else{
               cell.lblPartner.text = "---"
            }
            return cell
            }else if(indexPath.section == 1 ){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cellData", for: indexPath) as! MorDetailsTableViewCell
               
                cell.textViewTitle.text = section1[indexPath.row]
                if(electricBillDetailsDB.count > 0){
               
                    if(cell.textViewTitle.text == "Name" || cell.textViewTitle.text == "नाम"){
                        cell.textViewData.text = electricMainDetailsDB[0].BP_NAME
                    }else if(cell.textViewTitle.text == "Mobile No" || cell.textViewTitle.text == "मोबाइल नंबर"){
                        cell.textViewData.text = electricMainDetailsDB[0].TEL_NUMBER
                    }else if(cell.textViewTitle.text == "Bill Number" || cell.textViewTitle.text == "बिल नंबर"){
                        cell.textViewData.text = billDetailsDB[0].billno
                    }else if(cell.textViewTitle.text == "Address" || cell.textViewTitle.text == "पता"){
                        cell.textViewData.text = electricMainDetailsDB[0].ADD1 + "," + electricMainDetailsDB[0].ADD2 + "," + electricMainDetailsDB[0].ADD3
                    }else if(cell.textViewTitle.text == "Bill Month" || cell.textViewTitle.text == "बिल माह"){
                        let datee = self.dateFormatChange(inputDateStr: billDetailsDB[0].lastMon, inputFormat: "yyyy/MM", outputFromat: "MMM yyyy")
                        cell.textViewData.text = datee
                    }
                    
                    
                    
                    
                }
                return cell
            }else if(indexPath.section == 3){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cellData", for: indexPath) as! MorDetailsTableViewCell
               
                cell.textViewTitle.text = section2[0]
                if(electricBillDetailsDB.count > 0){
               
                     if(cell.textViewTitle.text == "Mobile No" || cell.textViewTitle.text == "मोबाइल नंबर"){
                        cell.textViewData.text = electricMainDetailsDB[0].TEL_NUMBER
                    }
                    
                    
                    
                    
                }
                return cell
            }else if(indexPath.section == 4){
                   let cellDrop = tableView.dequeueReusableCell(withIdentifier: "cellDataTextRead", for: indexPath) as! MorDropDetailsTableViewCell
                    cellDrop.textViewTitle.text = section2[2]
                cellDrop.textViewTitle.textColor = UIColor.red
                   cellDropData = cellDrop
                if(complaintName.count > 0){
                    if(cnt == 0){
                     cellDrop.lblType.text = complaintName[0]
                     self.complaintValueID = self.complaintID[0]
                    }
                    cnt = 1
                }
                   return cellDrop
                    
                    
                }else if(indexPath.section == 2){
                  let cellText = tableView.dequeueReusableCell(withIdentifier: "cellDataText", for: indexPath) as! MorDetailsTableViewCell
                    cellText.textViewTitle.text = section2[1]
                   cellTextData = cellText
                cellText.textViewData.keyboardType = .default
                   return cellText
                }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellButton", for: indexPath) as! UITableViewCell
                
                return cell
            }
             
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            if(section == 2 || section == 5 || section == 4 || section == 3 ){
               return 10.0
           }
            return 0.0
         }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
              
           
              let header = tableView.dequeueReusableCell(withIdentifier: "header") as! UITableViewCell
            return header
              
          }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if(indexPath.section == 2 || indexPath.section == 5){
                return 100
            }else if(indexPath.section == 4 ){
                if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
                     return 60
                }else{
                     return 45
                }
                
            }
            return UITableViewAutomaticDimension
            
        }
}

