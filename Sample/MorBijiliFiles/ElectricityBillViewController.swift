//
//  ElectricityBillViewController.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 11/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import DropDown
import CoreData
import CoreGraphics
var baseColorGreen = "2E8B57"
class ElectricityBillViewController: CommonVSClass {
   
    
 //MARK: ================== IBOutlets ================
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: ================== Properties ================
     let dropDown = DropDown()
    var BillDetailsAPI = ElectricityDetailsModelDataAPI()
    var consumptionDB : [ConsumptionDisplay] = []
    var electricBillDetailsDB : [ElectricBillDetailsDisplay] = []
    var electricMainDetailsDB : [ElectricMainDetailsDisplay] = []
    var electricUnitsDetailsDB : [ElectricBillUnitDetailsDisplay] = []
    var electricBillBPDetailsDB : [ElectricBillBPDataDetailsDisplay] = []
    var electricBillLNDetailsDB : [ElectricBillLNDetailsDisplay] = []
    var electricOfficeDetailsDB : [ElectricOfficeDetailsDisplay] = []
    var cellData : MainMorPartnerTableViewCell!
    var PartnerDB:[Partner] = []
    var cnt = 0
    var partnerBDValue : [String] = []
    var section1 : [String] = ["DC / Zone","Bill Month","Cons No.(B.P No)"]
    var section2 : [String] = ["Name and Address","Tarif","Purpose","Sanc. Load","SD Held","Avg Cons.","MF"]
    var section3 : [String] = ["Current Reading","Prev. Reading","Consumption","PF","MD","Bill Type"]
    var section4 : [String] = ["Fixed Charge","Energy Charge","Duty","Cess","Meter Rent","LTCS / WTCS","Rebate","Adjustment","ASD Raised","SD Interest","VCA Charge","Total Bill" ,"SD Arrear","Prev. Arrear","Surcharge Arrear","Other Charges" ]
    var section5 : [String] = ["Net Amount","Total Amount"]
    var section6 : [String] = ["Cheque Last Date","Cash Last Date"]
    //var section2 : [String] = ["Name and Address of B.P No Holder","Tarif Category","AVAIL","Units","Security","Average Used","Dividend"]
    
    var titleSet = String()
  
    //MARK: ================== VC Life Cycle ================
    func screenshot() -> UIImage{
    var image = UIImage();
//        UIGraphicsBeginImageContextWithOptions(self.tableView.contentSize, false, UIScreen.main.scale)
//
//        // save initial values
//        let savedContentOffset = self.tableView.contentOffset;
//        let savedFrame = self.tableView.frame;
//        let savedBackgroundColor = self.tableView.backgroundColor
//
//        // reset offset to top left point
//        self.tableView.contentOffset = CGPoint(x: 0, y: 0);
//        // set frame to content size
//        self.tableView.frame = CGRect(x: 0, y: 0, width: self.tableView.contentSize.width, height: self.tableView.contentSize.height);
//        // remove background
//        self.tableView.backgroundColor = UIColor.clear
//
//        // make temp view with scroll view content size
//        // a workaround for issue when image on ipad was drawn incorrectly
//        let tempView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.contentSize.width, height: self.tableView.contentSize.height));
//
//        // save superview
//        let tempSuperView = self.tableView.superview
//        // remove scrollView from old superview
//        self.tableView.removeFromSuperview()
//        // and add to tempView
//        tempView.addSubview(self.tableView)
        
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: self.tableView.contentSize.width, height:  self.tableView.contentSize.height), false, 0.0)
        let context = UIGraphicsGetCurrentContext()
        let previousFrame = self.tableView.frame
        
        var frame = CGRect(x: 0, y:  0,width: self.tableView.contentSize.width, height: self.tableView.contentSize.height);
        self.tableView.layer.render(in: context!)
        
        frame = previousFrame
        
        let imagee: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return image;
        
        

        // render view
        // drawViewHierarchyInRect not working correctly
//        tempView.layer.render(in: UIGraphicsGetCurrentContext()!)
//        // and get image
//        image = UIGraphicsGetImageFromCurrentImageContext()!;
//
//        // and return everything back
//        tempView.subviews[0].removeFromSuperview()
//        tempSuperView?.addSubview(self.tableView)
//
//        // restore saved settings
//        self.tableView.contentOffset = savedContentOffset;
//        self.tableView.frame = savedFrame;
//        self.tableView.backgroundColor = savedBackgroundColor

        UIGraphicsEndImageContext();

        return image
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
      
        if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
            section1  = ["DC / Zone","Bill Month","Cons No.(B.P No)"]
             section2  = ["Name and Address","Tarif","Purpose","Sanc. Load","SD Held","Avg Cons.","MF"]
             section3 = ["Current Reading","Prev. Reading","Consumption","PF","MD","Bill Type"]
             section4  = ["Fixed Charge","Energy Charge","Duty","Cess","Meter Rent","LTCS / WTCS","Rebate","Adjustment","ASD Raised","SD Interest","VCA Charge","Total Bill" ,"SD Arrear","Prev. Arrear","Surcharge Arrear","Other Charges" ]
             section5  = ["Net Amount","Total Amount"]
             section6  = ["Cheque Last Date","Cash Last Date"]
        }else{
         section1  = ["जोन / वितरण केंद्र","बिल माह","उपभोक्ता क्र.(BP No.)"]
          section2  = ["उपभोक्ता का नाम एवं पता","टैरिफ श्रेणी","प्रयोजन","अनुबंद भार","सुरक्षा निधि","औसत खपत","गुणांक"]
          section3  = ["वर्तमान वाचन","पूर्व वाचन","विद्युत् खपत","पावर फैक्टर","बिल मांग","बिल आधार"]
          section4  = ["नियत / मांग प्रभार","ऊर्जा प्रभार","विद्युत् शुल्क","ऊर्जा विकास उपकर","मीटर किराया","पावर फैक्टर अधिभार / रियायत","विशेष रियायत","विकलन / आकलन समायोजन","अति. सुरक्षा निधि","सुरक्षा निधि ब्याज","वी. सी. ए.","कुल बिल" ,"पूर्व सुरक्षा निधि बकाया","पूर्व बकाया","पूर्व अधिभार","अन्य प्रभार" ]
          section5  = ["शुद्ध देयक","सकल देयक"]
          section6  = ["चेक हेतु अंतिम तिथि","नगद हेतु अंतिम तिथि"]
          }

        
        
        
        getPartnerData()
        self.title = titleSet
//        print(UIDevice.current.systemVersion)
        // Do any additional setup after loading the view.
    }
      //MARK: ================== IB ACtions ================
    @IBAction func btnPartnerClicked(_ sender: UIButton) {
        self.dropDown.anchorView = cellData.btnPartner // UIView or UIBarButtonItem
        self.dropDown.width = cellData.btnPartner.frame.size.width
        self.dropDown.bottomOffset = CGPoint(x: 0, y:(self.dropDown.anchorView?.plainView.bounds.height)!)
        self.dropDown.direction = .bottom
        // The list of items to display. Can be changed dynamically
        self.dropDown.dataSource = partnerBDValue
        self.dropDown.show()
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected tem: \(item) at index: \(index)")
            self.cellData.lblPartner.text = item
            self.electricOfficeDetailsDB = []
            self.electricBillLNDetailsDB = []
            self.electricBillBPDetailsDB = []
            self.electricUnitsDetailsDB = []
            self.electricMainDetailsDB = []
            self.electricBillDetailsDB = []
            self.cntdata = 0
            self.tableView.reloadData()
            self.getBillDetailsData(bpno: item)
           
        }
    }
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
           var storyBoard = UIStoryboard(name: "Main", bundle: nil)
           if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
                storyBoard = UIStoryboard(name: "Main", bundle: nil)
           }else{
                storyBoard = UIStoryboard(name: "MainHindi", bundle: nil)
           }
           let genVC = storyBoard.instantiateViewController(withIdentifier: "BillPaymentViewController") as! BillPaymentViewController
        genVC.titleSet = billPayment
        genVC.bpNo = self.cellData.lblPartner.text!
           self.navigationController?.pushViewController(genVC, animated: true)
       }
    @IBAction func btnOfficeClicked(_ sender: UIButton) {
        if(self.electricMainDetailsDB.count > 0){
        let phone = electricOfficeDetailsDB[0].DCPHONE
               let url1 = "tel://\(phone)"
               print(url1)
               if let url = URL(string: url1), UIApplication.shared.canOpenURL(url) {
                   if #available(iOS 10, *) {
                       UIApplication.shared.open(url)
                   } else {
                       UIApplication.shared.openURL(url)
                   }
               }
        }
    }
   
    @IBAction func btnStaff1Clicked(_ sender: UIButton) {
        if(self.electricMainDetailsDB.count > 0){
        let phone = electricOfficeDetailsDB[0].JEPHONE
               let url1 = "tel://\(phone)"
               print(url1)
               if let url = URL(string: url1), UIApplication.shared.canOpenURL(url) {
                   if #available(iOS 10, *) {
                       UIApplication.shared.open(url)
                   } else {
                       UIApplication.shared.openURL(url)
                   }
               }
        }
    }
    @IBAction func btnStaff2Clicked(_ sender: UIButton) {
        if(self.electricMainDetailsDB.count > 0){
        let phone = electricOfficeDetailsDB[0].AEPHONE
               let url1 = "tel://\(phone)"
               print(url1)
               if let url = URL(string: url1), UIApplication.shared.canOpenURL(url) {
                   if #available(iOS 10, *) {
                       UIApplication.shared.open(url)
                   } else {
                       UIApplication.shared.openURL(url)
                   }
               }
        }
    }
    
    //MARK: ================== Custom Methods ================
    
    @objc func getPartnerData() {
    
            self.PartnerDB = [Partner]()
            do {
    
                self.PartnerDB = try context.fetch(Partner.fetchRequest())
                //self.refresh.endRefreshing()
                if(self.PartnerDB.count > 0) {
                    for i in 0...self.PartnerDB.count - 1 {
                        partnerBDValue.append(self.PartnerDB[i].partner!)
                    }
                   
                }else{
                   
               }
                self.tableView.reloadData()
    
            } catch {
                print("Fetching Failed")
            }
        }
        
    var cntdata = 0
    @objc func getBillDetailsData(bpno : String){
        
     
        
        BillDetailsAPI.serviceCalling(obj: self , bpNo : bpno) { (dict) in
            
            if(dict.count > 0){
                self.electricMainDetailsDB = dict as! [ElectricMainDetailsDisplay]
                self.tableView.reloadData()
            }
            
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: ================== Extension ================
extension ElectricityBillViewController: UITableViewDelegate,UITableViewDataSource {
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if( self.electricMainDetailsDB.count > 0){
            if(section == 2 || section == 3 || section == 4 || section == 5 || section == 6 || section == 7 || section == 8 || section == 9){
               return 10.0
           }
        }
            return 0.0
         }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
              
           
              let header = tableView.dequeueReusableCell(withIdentifier: "header") as! UITableViewCell
            return header
              
          }
        func numberOfSections(in tableView: UITableView) -> Int {
            return 11
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if(section == 0){
            return 1
            }else if(section == 1){
                if( self.electricMainDetailsDB.count > 0){
                return section1.count
                }
                return 0
            }else if(section == 2){
               if( self.electricMainDetailsDB.count > 0){
               return section2.count
               }
               return 0
            }else if(section == 3){
               if( self.electricMainDetailsDB.count > 0){
               return section3.count
               }
               return 0
            }else if(section == 4){
               if( self.electricMainDetailsDB.count > 0){
               return section4.count
               }
               return 0
            }else if(section == 5){
               if( self.electricMainDetailsDB.count > 0){
               return section5.count
               }
               return 0
            }else if(section == 6){
               if( self.electricMainDetailsDB.count > 0){
               return section6.count
               }
               return 0
            }else{
                if( self.electricMainDetailsDB.count > 0){
                 return 1
                }
               return 0
            }
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if(indexPath.section == 0){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MainMorPartnerTableViewCell
            cellData = cell
            if(PartnerDB.count > 0){
                if(cnt == 0){
                    
                    cell.lblPartner.text = partnerBDValue[indexPath.row]
                getBillDetailsData(bpno: partnerBDValue[0])
                }
                cnt = 1
            }else{
               cell.lblPartner.text = "---"
            }
            return cell
            }else if(indexPath.section == 1){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cellData", for: indexPath) as! MorDetailsTableViewCell
                cell.textViewData.textColor = UIColor.black
                cell.textViewTitle.textColor = UIColor.black
                cell.textViewTitle.text = section1[indexPath.row]
                
                if(cell.textViewTitle.text == "DC / Zone" || cell.textViewTitle.text == "जोन / वितरण केंद्र"){
                    cell.textViewData.text = electricMainDetailsDB[0].DIVTXT
                }else if(cell.textViewTitle.text == "Bill Month" || cell.textViewTitle.text == "बिल माह"){
                    let datee = self.dateFormatChange(inputDateStr: electricBillDetailsDB[0].BILL_MNTH, inputFormat: "yyyy/MM", outputFromat: "MMM-yyyy")
                    cell.textViewData.text = datee
                    
                }else if(cell.textViewTitle.text == "Cons No.(B.P No)" || cell.textViewTitle.text == "उपभोक्ता क्र.(BP No.)"){
                    cell.textViewData.text = electricMainDetailsDB[0].BP_NO
                }
                return cell
            }else if(indexPath.section == 2){
               let cell = tableView.dequeueReusableCell(withIdentifier: "cellData", for: indexPath) as! MorDetailsTableViewCell
                cell.textViewData.textColor = UIColor.black
                cell.textViewTitle.textColor = UIColor.black
 
                cell.textViewTitle.text = section2[indexPath.row]
                if(cell.textViewTitle.text == "Name and Address" || cell.textViewTitle.text == "उपभोक्ता का नाम एवं पता"){
                    cell.textViewData.text = electricMainDetailsDB[0].BP_NAME + "," + electricMainDetailsDB[0].ADD1 + "," + electricMainDetailsDB[0].ADD2 + "," + electricMainDetailsDB[0].ADD3
                }else if(cell.textViewTitle.text == "Tarif" || cell.textViewTitle.text == "टैरिफ श्रेणी"){
                    cell.textViewData.text = electricMainDetailsDB[0].RT_CT.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "Purpose" || cell.textViewTitle.text == "प्रयोजन"){
                    cell.textViewData.text = electricUnitsDetailsDB[0].PURPOSE.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "Sanc. Load" || cell.textViewTitle.text == "अनुबंद भार"){
                    cell.textViewData.text = electricUnitsDetailsDB[0].CON_LD.trimmingCharacters(in: .whitespaces) + " W"
                }else if(cell.textViewTitle.text == "SD Held" || cell.textViewTitle.text == "सुरक्षा निधि"){
                    cell.textViewData.text = electricMainDetailsDB[0].SD_1.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "Avg Cons." || cell.textViewTitle.text == "औसत खपत"){
                    cell.textViewData.text = electricUnitsDetailsDB[0].AVRG_QTY.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "MF" || cell.textViewTitle.text == "गुणांक"){
                    cell.textViewData.text = electricUnitsDetailsDB[0].MF.trimmingCharacters(in: .whitespaces)
                }
                return cell
            }else if(indexPath.section == 3){
              
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellData", for: indexPath) as! MorDetailsTableViewCell
                cell.textViewData.textColor = UIColor.black
                cell.textViewTitle.textColor = UIColor.black
                cell.textViewTitle.text = section3[indexPath.row]
                
                 if(cell.textViewTitle.text == "Current Reading" || cell.textViewTitle.text == "वर्तमान वाचन"){
                    cell.textViewData.text = electricUnitsDetailsDB[0].CR_RD.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "Prev. Reading" || cell.textViewTitle.text == "पूर्व वाचन"){
                    cell.textViewData.text = electricUnitsDetailsDB[0].PR_RD.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "Consumption" || cell.textViewTitle.text == "विद्युत् खपत"){
                    cell.textViewData.text = electricUnitsDetailsDB[0].CONS.trimmingCharacters(in: .whitespaces)
                    cell.textViewData.textColor = UIColor.init(hexString: baseColorGreen, alpha: 1.0)
                    cell.textViewTitle.textColor = UIColor.init(hexString: baseColorGreen, alpha: 1.0)
                    
                }else if(cell.textViewTitle.text == "PF" || cell.textViewTitle.text == "पावर फैक्टर"){
                    cell.textViewData.text = electricUnitsDetailsDB[0].PF.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "MD" || cell.textViewTitle.text == "बिल मांग"){
                    cell.textViewData.text = electricUnitsDetailsDB[0].MD.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "Bill Type" || cell.textViewTitle.text == "बिल आधार"){
                    cell.textViewData.text = electricUnitsDetailsDB[0].BILLING_TYPE.trimmingCharacters(in: .whitespaces)
                }
                return cell
            }else if(indexPath.section == 4){
              
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellData", for: indexPath) as! MorDetailsTableViewCell
                cell.textViewData.textColor = UIColor.black
                cell.textViewTitle.textColor = UIColor.black
                cell.textViewTitle.text = section4[indexPath.row]
               
               
                
                 if(cell.textViewTitle.text == "Fixed Charge" || cell.textViewTitle.text == "नियत / मांग प्रभार"){
                    cell.textViewData.text = electricBillBPDetailsDB[0].FIX_CH_1.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "Energy Charge" || cell.textViewTitle.text == "ऊर्जा प्रभार"){
                    cell.textViewData.text = electricBillBPDetailsDB[0].EC_1.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "Duty" || cell.textViewTitle.text == "विद्युत् शुल्क"){
                    cell.textViewData.text = electricBillBPDetailsDB[0].ED_1.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "Cess" || cell.textViewTitle.text == "ऊर्जा विकास उपकर"){
                    cell.textViewData.text = electricBillBPDetailsDB[0].CESS_1.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "Meter Rent" || cell.textViewTitle.text == "मीटर किराया"){
                    cell.textViewData.text = electricBillBPDetailsDB[0].MTR_1.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "LTCS / WTCS" || cell.textViewTitle.text == "पावर फैक्टर अधिभार / रियायत"){
                    cell.textViewData.text = electricBillBPDetailsDB[0].WTCS_1.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "Rebate" || cell.textViewTitle.text == "विशेष रियायत"){
                    cell.textViewData.text = electricBillBPDetailsDB[0].SG400U_50PR.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "Adjustment" || cell.textViewTitle.text == "विकलन / आकलन समायोजन"){
                    cell.textViewData.text = electricBillBPDetailsDB[0].REB_1.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "ASD Raised" || cell.textViewTitle.text == "अति. सुरक्षा निधि"){
                    cell.textViewData.text = electricBillBPDetailsDB[0].ADJ_DMD_1.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "SD Interest" || cell.textViewTitle.text == "सुरक्षा निधि ब्याज"){
                    cell.textViewData.text = electricBillBPDetailsDB[0].AVG_ADJ_1.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "VCA Charge" || cell.textViewTitle.text == "वी. सी. ए."){
                    cell.textViewData.text = electricBillBPDetailsDB[0].FCA_1.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "Total Bill" || cell.textViewTitle.text == "कुल बिल"){
                    print(electricBillBPDetailsDB[0].NET_BILL)
                    cell.textViewData.text = electricBillBPDetailsDB[0].NET_BILL.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "SD Arrear" || cell.textViewTitle.text == "पूर्व सुरक्षा निधि बकाया"){
                   cell.textViewData.text = "---"
                }else if(cell.textViewTitle.text == "Prev. Arrear" || cell.textViewTitle.text == "पूर्व बकाया"){
                    cell.textViewData.text = electricBillBPDetailsDB[0].PRE_ARR_1.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "Surcharge Arrear" || cell.textViewTitle.text == "पूर्व अधिभार"){
//                    cell.textViewData.text = electricBillBPDetailsDB[0].MD.trimmingCharacters(in: .whitespaces)
                }else if(cell.textViewTitle.text == "Other Charges" || cell.textViewTitle.text == "अन्य प्रभार"){
                    cell.textViewData.text = electricBillBPDetailsDB[0].MIS_1.trimmingCharacters(in: .whitespaces)
                }
              
                return cell
            }else if(indexPath.section == 5){
                          
             let cell = tableView.dequeueReusableCell(withIdentifier: "cellData", for: indexPath) as! MorDetailsTableViewCell
                cell.textViewData.textColor = UIColor.init(hexString: "FF7F50", alpha: 1.0)
                cell.textViewTitle.textColor = UIColor.init(hexString: "FF7F50", alpha: 1.0)
                
                       
             cell.textViewTitle.text = section5[indexPath.row]
              if(cell.textViewTitle.text == "Net Amount Rs. " || cell.textViewTitle.text == "शुद्ध देयक"){
               cell.textViewData.text = electricBillBPDetailsDB[0].TOT_BILL_1.trimmingCharacters(in: .whitespaces) + " ₹."
               }else if(cell.textViewTitle.text == "Bill with Surcharge Rs. " || cell.textViewTitle.text == "सकल देयक"){
                  cell.textViewData.text = electricBillBPDetailsDB[0].AMT.trimmingCharacters(in: .whitespaces) + " ₹."
                }
                 return cell
               }else if(indexPath.section == 6){
               
               let cell = tableView.dequeueReusableCell(withIdentifier: "cellData", for: indexPath) as! MorDetailsTableViewCell
                cell.textViewData.textColor = UIColor.black
                cell.textViewTitle.textColor = UIColor.black

               cell.textViewTitle.text = section6[indexPath.row]
                if(cell.textViewTitle.text == "Cheque Last Date" || cell.textViewTitle.text == "चेक हेतु अंतिम तिथि"){
                  
                    let datee = self.dateFormatChange(inputDateStr: electricBillDetailsDB[0].DUEDT_CHQ.trimmingCharacters(in: .whitespaces), inputFormat: "yyyy-MM-dd", outputFromat: "dd MMM yyyy")
                    
                 cell.textViewData.text = datee
                    cell.textViewData.textColor = UIColor.init(hexString: baseColorGreen, alpha: 1.0)
                    cell.textViewTitle.textColor = UIColor.init(hexString: baseColorGreen, alpha: 1.0)
                 }else if(cell.textViewTitle.text == "Cash Last Date" || cell.textViewTitle.text == "नगद हेतु अंतिम तिथि"){
                   
                    let datee = self.dateFormatChange(inputDateStr: electricBillDetailsDB[0].DUE_DT.trimmingCharacters(in: .whitespaces), inputFormat: "yyyy-MM-dd", outputFromat: "dd MMM yyyy")
                    
                    cell.textViewData.text = datee
                    cell.textViewData.textColor = UIColor.init(hexString: baseColorGreen, alpha: 1.0)
                    cell.textViewTitle.textColor = UIColor.init(hexString: baseColorGreen, alpha: 1.0)
                  }
                
                              return cell
                 }else if(indexPath.section == 7){
                 
                 let cell = tableView.dequeueReusableCell(withIdentifier: "cellData", for: indexPath) as! MorDetailsTableViewCell
                cell.textViewData.textColor = UIColor.black
                cell.textViewTitle.textColor = UIColor.black
        if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
                 cell.textViewTitle.text = "Last Payment"
                }else{
                 cell.textViewTitle.text = "पिछला भुगतान"
                }
                  let datee = self.dateFormatChange(inputDateStr: electricBillLNDetailsDB[0].PD1, inputFormat: "yyyy-MM-dd", outputFromat: "dd MMM yyyy")
                 
                   cell.textViewData.text = electricBillLNDetailsDB[0].PA1.trimmingCharacters(in: .whitespaces) + " ₹. \(dated) -> " + datee
                   
                                return cell
                   }else if(indexPath.section == 8){
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! BillButtonTableViewCell
                
                cell.btnOffice.setTitle(electricOfficeDetailsDB[0].DCPHONE, for: .normal)
                cell.textViewStaff1.text = electricOfficeDetailsDB[0].JENAME
                 cell.btnStaff1.setTitle(electricOfficeDetailsDB[0].JEPHONE, for: .normal)
                cell.textViewStaff2.text = electricOfficeDetailsDB[0].AENAME
                cell.btnStaff2.setTitle(electricOfficeDetailsDB[0].AEPHONE, for: .normal)
                return cell
            }else if(indexPath.section == 9){
                     
                     let cell = tableView.dequeueReusableCell(withIdentifier: "cellData1", for: indexPath) as! MorDetailsTableViewCell
                cell.textViewData.textColor = UIColor.init(hexString: "720e9e", alpha: 1.0)
                cell.textViewData.textAlignment = .center
                cell.textViewData.font = UIFont(name: "RobotoSlab-Bold", size: 16.0)
                    cell.textViewTitle.textColor = UIColor.clear
//            if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
//                     cell.textViewTitle.text = "Last Payment"
//                    }else{
//                     cell.textViewTitle.text = "पिछला भुगतान"
//                    }
                      cell.textViewData.text = halfffYogna +  electricBillBPDetailsDB[0].SG400U_50PR.trimmingCharacters(in: .whitespaces)
                       
                                    return cell
                       }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellButton", for: indexPath) as! UITableViewCell
                
                return cell
            }
          
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if(indexPath.section == 10){
                return 130
            }
            return UITableViewAutomaticDimension
            
        }
}
