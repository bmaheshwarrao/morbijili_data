//
//  BillPaymentViewController.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 12/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import DropDown
import CoreData
import AlamofireSoap
import Alamofire
import SwiftyXMLParser
import Foundation
class BillPaymentViewController:  CommonVSClass,UITextViewDelegate {
    
    
    
    
   
    
   
    
 //MARK: ================== IBOutlets ================
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: ================== Properties ================
     let dropDown = DropDown()
    var BillDetailsAPI = BillPaymentModelDataAPI()
    var cellData : MainMorPartnerTableViewCell!
    var cellDataText : MorDetailsTableViewCell!
    var billDetailsDB : [BillDisplay] = []
    var PartnerDB:[Partner] = []
     var titleSet = String()
    var cnt = 0
    var bpNo = String()
    var bdvc =  BDViewController()
    var partnerBDValue : [String] = []
    var section1 : [String] = ["Name","Mobile No","Bill Number","Bill Month","Last Date of Bill Submission","Bill Amount"]
   var window: UIWindow?
    
    
    //MARK: ================== VC Life Cycle ================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        getPartnerData()
        self.title = titleSet
        if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
            section1  = ["Consumer No.(B.P No)","Name","Mobile No","Bill No","Bill Month","Due Date","Amount"]
             
        }else{
         section1  = ["उपभोगता क्र.(BP No.)","नाम","मोबाइल नंबर","बिल नंबर","बिल माह","बिल पेमेंट की आखरी तिथि","बिल राशि"]
          
          }
        NotificationCenter.default.addObserver(self, selector: #selector(self.callsync), name: NSNotification.Name(rawValue: "ShowTrans"), object: nil)
        

        
        // Do any additional setup after loading the view.
    }
    @objc func callsync(){
       let storyboard = UIStoryboard(name: "Main", bundle: nil)
      
    }
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(true)
          
          self.tabBarController?.tabBar.isHidden = true
           //self.title = FilterGraphStruct.title
        let nav = self.navigationController?.navigationBar
        nav?.isHidden = false
          nav?.tintColor = UIColor.white
          nav?.barTintColor = UIColor(hexString: baseColor, alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
          
      }
      //MARK: ================== IB ACtions ================
    
    @IBAction func btnPaymentClicked(_ sender: UIButton) {
        if(cellDataText.textViewData.text! != ""){
           let amt = Int(Double(billDetailsDB[0].amount)!)
            let amtWrite = Int(Double(cellDataText.textViewData.text)!)
        if(amtWrite < 10 ){
            self.view.makeToastHZL(invAmt)
        }else if(amtWrite < amt){
            self.view.makeToastHZL(billAmtPayyy)
        }else{
           sendToPay()
        }
        }
        
    }
   
    
    
    @IBAction func btnPartnerClicked(_ sender: UIButton) {
        self.dropDown.anchorView = cellData.btnPartner // UIView or UIBarButtonItem
        self.dropDown.width = cellData.btnPartner.frame.size.width
        self.dropDown.bottomOffset = CGPoint(x: 0, y:(self.dropDown.anchorView?.plainView.bounds.height)!)
        self.dropDown.direction = .bottom
        // The list of items to display. Can be changed dynamically
        self.dropDown.dataSource = partnerBDValue
        self.dropDown.show()
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected tem: \(item) at index: \(index)")
            self.cellData.lblPartner.text = item
            self.billDetailsDB = []
            self.tableView.reloadData()
            self.cntdata = 0
            self.getPaymentBillDetailsData(bpno: item)
           
        }
    }
    
    //MARK: ================== Custom Methods ================
    func payAtlast(){
           let amt_regex = "(?=.)^\\$?(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+)?(\\.[0-9]{1,2})?$"
                          let regex = "\\b([a-zA-Z0-9%_.+\\-]+)@([a-zA-Z0-9.\\-]+?\\.[a-zA-Z]{2,6})\\b"
                          let regextest = NSPredicate(format: "SELF MATCHES %@", amt_regex)
                          let emailtest = NSPredicate(format: "SELF MATCHES %@", regex)
                  if (cellDataText.textViewData.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 || cellDataText.textViewData.text! == nil) {
                              
                              let alertController = UIAlertController(title: "Sorry", message: invalidamt, preferredStyle: .alert)
                              alertController.addAction(UIAlertAction(title: self.okay, style: .cancel) { (action:UIAlertAction!) in
                                  print("Cancel")
                              })
                              self.present(alertController, animated: true, completion: nil)
                              
                          } else if !regextest.evaluate(with: cellDataText.textViewData.text!) {
                              
                              let alertController = UIAlertController(title: "Sorry", message: invalidamt, preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: self.okay, style: .cancel) { (action:UIAlertAction!) in
                                  print("Cancel")
                              })
                              self.present(alertController, animated: true, completion: nil)
                          }else{
                      let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
                              SHKActivityIndicator.current().displayActivity("Fetching Data")
                              let param1 = "amt=\(cellDataText.textViewData.text!)&email="
                              let str1 = "\(get_msg_token_string)"
                              print("url dfgf str \(str1) ")
                    
                    
                    
                    
                  
                    
                    let amt = Int(Double(cellDataText.textViewData.text!)!)
                 // let amt = 1
                    let respoStr1 = "CSEB|\(billDetailsDB[0].billno)|NA|\(amt)|NA|NA|NA|INR|NA|R|cseb|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|https://uat.billdesk.com/pgidsk/pgmerc/pg_dump.jsp"
                    let hmac_sha1 = respoStr1.hmac(algorithm: .sha256, key: "z8bKEaX1XEtB")
                     let respoStr = respoStr1 + "|\(hmac_sha1.uppercased())"
                    
                    var storyBoard = UIStoryboard(name: "Main", bundle: nil)
                       if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
                            storyBoard = UIStoryboard(name: "Main", bundle: nil)
                       }else{
                            storyBoard = UIStoryboard(name: "MainHindi", bundle: nil)
                       }
                       moveInt = 0
                          
                          let genVC = storyBoard.instantiateViewController(withIdentifier: "PaymentStatusNavViewController") as! PaymentStatusNavViewController
                    BillTrans.resp = respoStr
                    BillTrans.amt = Float(amt)
                    BillTrans.mobile = mobile
                    BillTrans.bpno = cellData.lblPartner.text!
                    BillTrans.billDis = billDetailsDB[0]
                         // self.navigationController?.pushViewController(genVC, animated: true)
                    genVC.modalPresentationStyle = .fullScreen
                    self.present(genVC, animated: false, completion: nil)
                   
                              
                          }
       }
       
       
       func sendToPay(){
            self.startLoadingPK(view: self.view, text: "paying....")
            let dateFormatterInv = DateFormatter()
            dateFormatterInv.timeZone = NSTimeZone.system
            dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
            let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
            let invStrUnique = dateFormatterInv.string(from: Date())
            let request_no = invStrUnique + mobile
       let amt = Int(Double(cellDataText.textViewData.text!)!)
       
        let respoStr1 = "CSEB|\(cellData.lblPartner.text!)|NA|\(amt)|NA|NA|NA|INR|NA|R|cseb|NA|NA|F|\(billDetailsDB[0].name)|\(billDetailsDB[0].billno)|CSPDCLIMA|NA|NA|NA|https://uat.billdesk.com/pgidsk/pgmerc/pg_dump.jsp"
        
      
        
        
        
        let hmac_sha1 = respoStr1.hmac(algorithm: .sha256, key: "z8bKEaX1XEtB")
         let strPGMsgfinal = respoStr1 + "|\(hmac_sha1.uppercased())"
        
        
        
        
            let message = "MA_pgmsgsub|\(strPGMsgfinal)|req|\(secID)"
            let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
           AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_string", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
            self.stopLoadingPK(view: self.view)
           guard let respo = response.value else {
               return
           }
            print(response.value)
           let xml = try! XML.parse(respo)
           let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_stringResponse"]["MA_cons_stringResult"].text
           if let result = accessor {
               let resultInt = Int(result)
               if(resultInt! > 0){
                   self.payAtlast()
                   
               }else{
               }
              
           }
           }

       }
    
    func sendRequest(){
                 // self.startLoadingPK(view: self.view, text: "paying....")
        let dateFormatterInv = DateFormatter()
        dateFormatterInv.timeZone = NSTimeZone.system
        dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
        let strPGMsgfinal =  "CS1006|CSEB|\(billDetailsDB[0].billno)|\(cellData.lblPartner.text!)|ALL|20130608121501|NA|NA|NA|409733 6988"
                 
                  let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
                  let invStrUnique = dateFormatterInv.string(from: Date())
                  let request_no = invStrUnique + mobile
               
                  let message = "MA_pgmsgsub|strPGMsgfinal|res|\(secID)|\(request_no)"
                  let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
                 AlamofireSoap.soapRequest(get_msg_token_string, soapmethod: "MA_cons_string", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
                  self.stopLoadingPK(view: self.view)
                 guard let respo = response.value else {
                     return
                 }
                  print(response.value)
                 let xml = try! XML.parse(respo)
                 let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_stringResponse"]["MA_cons_stringResult"].text
                 if let result = accessor {
                     let resultInt = Int(result)
                     if(resultInt! > 0){
                       self.navigationController?.popToRootViewController(animated: true)
                         
                     }else{
                     }
                    
                 }
                 }

             }
    
    
    func sendFromPay(){
               self.startLoadingPK(view: self.view, text: "paying....")
               let dateFormatterInv = DateFormatter()
               dateFormatterInv.timeZone = NSTimeZone.system
               dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
               let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
               let invStrUnique = dateFormatterInv.string(from: Date())
               let request_no = invStrUnique + mobile
            
               let message = "MA_pgmsgsub|strPGMsgfinal|res|\(secID)|\(request_no)"
               let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
              AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_string", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
               self.stopLoadingPK(view: self.view)
              guard let respo = response.value else {
                  return
              }
               print(response.value)
              let xml = try! XML.parse(respo)
              let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_stringResponse"]["MA_cons_stringResult"].text
              if let result = accessor {
                  let resultInt = Int(result)
                  if(resultInt! > 0){
                    self.navigationController?.popToRootViewController(animated: true)
                      
                  }else{
                  }
                 
              }
              }

          }
    var cntdata = 0
    @objc func getPartnerData() {
    
            self.PartnerDB = [Partner]()
            do {
    
                self.PartnerDB = try context.fetch(Partner.fetchRequest())
                //self.refresh.endRefreshing()
                if(self.PartnerDB.count > 0) {
                    for i in 0...self.PartnerDB.count - 1 {
                        partnerBDValue.append(self.PartnerDB[i].partner!)
                    }
                   
                }else{
                   
               }
                self.tableView.reloadData()
    
            } catch {
                print("Fetching Failed")
            }
        }
        
    var showDetailsAPI = BillPaymentModelDataAPI()
    @objc func getPaymentBillDetailsData(bpno : String){
        
     
        
        BillDetailsAPI.serviceCalling(obj: self , bpNo : bpno) { (dict) in
            
            if(dict.count > 0){
                self.billDetailsDB = dict as! [BillDisplay]
                self.tableView.reloadData()
            }
            
        }
        
        
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return textView.text.count + (text.count - range.length) <= 8
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: ================== Extension ================
extension BillPaymentViewController: UITableViewDelegate,UITableViewDataSource {
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        }
        func numberOfSections(in tableView: UITableView) -> Int {
            return 3
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if(section == 0 ){
            return 1
            }else if(section == 2){
                if(billDetailsDB.count > 0){
                return 1
                }
                return 0
            }else{
                if(billDetailsDB.count > 0){
                return section1.count
                }
                return 0
            }
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if(indexPath.section == 0){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MainMorPartnerTableViewCell
            cellData = cell
            if(PartnerDB.count > 0){
                
                if(cnt == 0){
                   
                    if(bpNo == ""){
                        cell.lblPartner.text = partnerBDValue[indexPath.row]
                    getPaymentBillDetailsData(bpno: partnerBDValue[0])
                        }else{
                            cell.lblPartner.text = bpNo
                            getPaymentBillDetailsData(bpno: bpNo)
                        }
                }
                cnt = 1
            }else{
               cell.lblPartner.text = "---"
            }
            return cell
            }else if(indexPath.section == 1){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cellData", for: indexPath) as! MorDetailsTableViewCell
             
                cell.textViewTitle.text = section1[indexPath.row]
                if(cell.textViewTitle.text == "Name" || cell.textViewTitle.text == "नाम"){
                    cell.textViewData.text = billDetailsDB[0].name
                }else if(cell.textViewTitle.text == "Consumer No.(B.P No)" || cell.textViewTitle.text == "उपभोगता क्र.(BP No.)"){
                    if(bpNo == ""){
                        cell.textViewData.text = partnerBDValue[0]
                   
                        }else{
                            cell.textViewData.text = bpNo
                        }
                    
                    
                    
                }else if(cell.textViewTitle.text == "Mobile No" || cell.textViewTitle.text == "मोबाइल नंबर"){
                    cell.textViewData.text = billDetailsDB[0].mobile
                }else if(cell.textViewTitle.text == "Bill No" || cell.textViewTitle.text == "बिल नंबर"){
                    cell.textViewData.text = billDetailsDB[0].billno
                }else if(cell.textViewTitle.text == "Bill Month" || cell.textViewTitle.text == "बिल माह"){
                    let datee = self.dateFormatChange(inputDateStr: billDetailsDB[0].lastMon, inputFormat: "yyyy/MM", outputFromat: "MMM yyyy")
                    cell.textViewData.text = datee
                }else if(cell.textViewTitle.text == "Due Date" || cell.textViewTitle.text == "बिल पेमेंट की आखरी तिथि"){
                    let datee = self.dateFormatChange(inputDateStr: billDetailsDB[0].lastDate, inputFormat: "yyyy-MM-dd", outputFromat: "dd MMM yyyy")
                    cell.textViewData.text = datee
                }else if(cell.textViewTitle.text == "Amount" || cell.textViewTitle.text == "बिल राशि"){
                    let cell1 = tableView.dequeueReusableCell(withIdentifier: "cellDataText", for: indexPath) as! MorDetailsTableViewCell
                    cellDataText = cell1
                    cell1.textViewTitle.text = section1[indexPath.row]
                    cell1.textViewData.text = String(Int(Double(billDetailsDB[0].amount)!))
                    cell1.textViewData.backgroundColor = UIColor.init(hexString: "F1F3F4", alpha: 1.0)
                    cell1.textViewData.keyboardType = .numberPad
                    cell1.textViewData.textColor = UIColor.black
                    cell1.textViewData.delegate = self
                    return cell1
                }
                return cell
            }else{
               let cell = tableView.dequeueReusableCell(withIdentifier: "cellButton", for: indexPath) as! UITableViewCell
                
                return cell
            }
           
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if(indexPath.section == 2){
                return 100
            }
            return UITableViewAutomaticDimension
            
        }
    
}


enum HmacAlgorithm {
    case sha1, md5, sha256, sha384, sha512, sha224
    var algorithm: CCHmacAlgorithm {
        var alg = 0
        switch self {
        case .sha1:
            alg = kCCHmacAlgSHA1
        case .md5:
            alg = kCCHmacAlgMD5
        case .sha256:
            alg = kCCHmacAlgSHA256
        case .sha384:
            alg = kCCHmacAlgSHA384
        case .sha512:
            alg = kCCHmacAlgSHA512
        case .sha224:
            alg = kCCHmacAlgSHA224
        }
        return CCHmacAlgorithm(alg)
    }
    var digestLength: Int {
        var len: Int32 = 0
        switch self {
        case .sha1:
            len = CC_SHA1_DIGEST_LENGTH
        case .md5:
            len = CC_MD5_DIGEST_LENGTH
        case .sha256:
            len = CC_SHA256_DIGEST_LENGTH
        case .sha384:
            len = CC_SHA384_DIGEST_LENGTH
        case .sha512:
            len = CC_SHA512_DIGEST_LENGTH
        case .sha224:
            len = CC_SHA224_DIGEST_LENGTH
        }
        return Int(len)
    }
}

extension String {
    func hmac(algorithm: HmacAlgorithm, key: String) -> String {
        var digest = [UInt8](repeating: 0, count: algorithm.digestLength)
        CCHmac(algorithm.algorithm, key, key.count, self, self.count, &digest)
        let data = Data(bytes: digest)
        return data.map { String(format: "%02hhx", $0) }.joined()
    }
}
 struct BillTrans {
  
    
    static var resp = String()
    static var amt = Float()
    static var mobile = String()
     static var bpno = String()
    static var billDis = BillDisplay()
    
}
struct BillTransEnd {
  
    
    static var tip = String()
    static var status = String()
    static var refno = String()
     static var paytype = String()
    static var amt = String()
    static var bpno = String()
    static var date = String()
}
