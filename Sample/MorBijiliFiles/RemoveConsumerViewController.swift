//
//  RemoveConsumerViewController.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 13/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import AlamofireSoap
import SwiftyXMLParser
import CoreData
class RemoveConsumerViewController: CommonVSClass {
   
    
 //MARK: ================== IBOutlets ================
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: ================== Properties ================
     let dropDown = DropDown()
    var RemoveDetailsAPI = ElectricityRemoveModelDataAPI()
    var ComplaintDetailsAPI = ComplainTypeModelDataAPI()
    var cellData : MainMorPartnerTableViewCell!
    var billDetailsDB : [BillDisplay] = []
    var PartnerDB:[String] = []
    var PartnerDBData:[Partner] = []
    var partnerBDValue : [String] = []
    var cellPhotoData : PhotoMeterTableViewCell!
    var cellDataText : MorDetailsTableViewCell!
    var cellDropData : MorDropDetailsTableViewCell!
    var cellTextData : MorDetailsTableViewCell!
    var PartnerSharingAPI = ElectricityRemovePartnerModelDataAPI()
    var consumptionDB : [ConsumptionDisplay] = []
    var electricBillDetailsDB : [ElectricBillDetailsDisplay] = []
    var electricMainDetailsDB : [ElectricMainDetailsDisplay] = []
    var electricUnitsDetailsDB : [ElectricBillUnitDetailsDisplay] = []
    var electricBillBPDetailsDB : [ElectricBillBPDataDetailsDisplay] = []
    var electricBillLNDetailsDB : [ElectricBillLNDetailsDisplay] = []
    var electricOfficeDetailsDB : [ElectricOfficeDetailsDisplay] = []
    let imagePicker = UIImagePickerController()
    var imageData : Data? = nil
    var cnt = 0
    var section1 : [String] = ["Name","Address"]
   var titleSet = String()
    
    
    //MARK: ================== VC Life Cycle ================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        getPartnerData()
        self.title = titleSet
        if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
        section1  = ["Name","Address","Mobile No"]
        }else{
        section1  = ["नाम","पता","मोबाइल नंबर"]
         }
        
        // Do any additional setup after loading the view.
    }
    
 
      //MARK: ================== IB ACtions ================
   
    @IBAction func btnRemoveClicked(_ sender: UIButton) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                        
            let actionSheetController: UIAlertController = UIAlertController(title: self.confirmation, message: self.removeSure + self.cellData.lblPartner.text! + self.morbijRem, preferredStyle: .actionSheet)
                        
            let camera = UIAlertAction(title: self.remove, style: .default, handler: { (alert) in
                            self.remove()
                            
                        })
                        
            let cancel = UIAlertAction(title: self.cancel, style: .cancel, handler: { (alert) in
                            
                        })
                        
                        actionSheetController.addAction(camera)
                        
                        actionSheetController.addAction(cancel)
                        self.present(actionSheetController, animated: true, completion: nil)
                        
                        
                    }
        
        
    }
    
   
    
    
    @IBAction func btnPartnerClicked(_ sender: UIButton) {
        self.dropDown.anchorView = cellData.btnPartner // UIView or UIBarButtonItem
        self.dropDown.width = cellData.btnPartner.frame.size.width
        self.dropDown.bottomOffset = CGPoint(x: 0, y:(self.dropDown.anchorView?.plainView.bounds.height)!)
        self.dropDown.direction = .bottom
        // The list of items to display. Can be changed dynamically
        self.dropDown.dataSource = partnerBDValue
        self.dropDown.show()
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected tem: \(item) at index: \(index)")
            self.cellData.lblPartner.text = item
            self.getBillDetailsData(bpno: item)
           
        }
    }
    
    //MARK: ================== Custom Methods ================
    @objc func remove(){
           startLoadingPK(view: self.view, text: "Removing .....")
           let dateFormatterInv = DateFormatter()
                      dateFormatterInv.timeZone = NSTimeZone.system
                      dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
                      let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
                      let invStrUnique = dateFormatterInv.string(from: Date())
                      let request_no = invStrUnique + mobile
                      let message = "MA_add_remove_mobileno|\(cellData.lblPartner.text!)|\(mobile)|DEL|\(secID)|\(request_no)"
                  print(message)
                      let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
                     AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_string", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
                         print("Request: \(response.value)")
                       
                     
                      
                         guard let respo = response.value else {
                           self.stopLoadingPK(view: self.view)
                             return
                         }
                         self.stopLoadingPK(view: self.view)
                         let xml = try! XML.parse(respo)
                         let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_stringResponse"]["MA_cons_stringResult"].text
                         if let result = accessor {
                             let resultInt = Int(result)
                             if(resultInt! > 0){
                                // self.showSingleButtonWithMessage(title: "Confirmation", message: "Consumer removed successfully.", buttonName: "OK")
                                self.showSingleButtonAlertWithAction(title: self.removeSure, buttonTitle: self.okay) {
                                   self.navigationController?.popViewController(animated: true)
                               }
                               
                              
                             }else{
                                self.showSingleButtonWithMessage(title: self.confirmation, message: "Not able to remove consumer.", buttonName: "OK")
                             }
                         }
                         
                     }
       }
    @objc func getPartnerSharingData(){
             
          
             
        PartnerSharingAPI.serviceCalling( obj: self) { (dict) in
              self.deleteParnterData()
               self.PartnerDB = dict as! [String]
              if(self.PartnerDB.count > 0){
                  for i in 0...self.PartnerDB.count - 1 {
                      self.saveDirPathData(name: self.PartnerDB[i])
                  }
              }
                 
             }
             
             
         }
       func saveDirPathData(name:String){
              
              let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
              
              let tasks = Partner(context: context)
              tasks.partner = name
              (UIApplication.shared.delegate as! AppDelegate).saveContext()
          }
          func deleteParnterData()
          {
            
              
              
              let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
              
              let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Partner")
              let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
              
              do {
                  try context.execute(batchDeleteRequest)
                  
              } catch {
                  // Error Handling
              }
          }
    @objc func getPartnerData() {
   
            self.PartnerDBData = [Partner]()
            do {
    
                self.PartnerDBData = try context.fetch(Partner.fetchRequest())
                //self.refresh.endRefreshing()
                if(self.PartnerDBData.count > 0) {
                    for i in 0...self.PartnerDBData.count - 1 {
                        partnerBDValue.append(self.PartnerDBData[i].partner!)
                    }
                   
                }else{
                   
               }
                self.tableView.reloadData()
    
            } catch {
                print("Fetching Failed")
            }
        }
      
    
    @objc func getBillDetailsData(bpno : String){
           
        
           
           RemoveDetailsAPI.serviceCalling(obj: self , bpNo : bpno) { (dict) in
               
               if(dict.count > 0){
                   self.electricMainDetailsDB = dict as! [ElectricMainDetailsDisplay]
                   self.tableView.reloadData()
               }
               
           }
           
           
       }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: ================== Extension ================
extension RemoveConsumerViewController: UITableViewDelegate,UITableViewDataSource {
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        }
        func numberOfSections(in tableView: UITableView) -> Int {
            return 3
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if(section == 0 ){
            return 1
            }else if(section == 1){
                if(electricBillDetailsDB.count > 0){
                return section1.count
                }
                return 0
            }else {
                if(electricBillDetailsDB.count > 0){
                return 1
                }
                return 0
            }
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if(indexPath.section == 0){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MainMorPartnerTableViewCell
            cellData = cell
            if(PartnerDBData.count > 0){
                
                if(cnt == 0){
                    cell.lblPartner.text = partnerBDValue[indexPath.row]
                getBillDetailsData(bpno: partnerBDValue[0])
                }
                cnt = 1
            }else{
               cell.lblPartner.text = "---"
            }
            return cell
            }else if(indexPath.section == 1){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cellData", for: indexPath) as! MorDetailsTableViewCell
               
                cell.textViewTitle.text = section1[indexPath.row]
                if(electricBillDetailsDB.count > 0){
                     
                               if(cell.textViewTitle.text == "Name" || cell.textViewTitle.text == "नाम"){
                                   cell.textViewData.text = electricMainDetailsDB[0].BP_NAME
                               }else if(cell.textViewTitle.text == "Mobile No" || cell.textViewTitle.text == "मोबाइल नंबर" ){
                                   cell.textViewData.text = electricMainDetailsDB[0].TEL_NUMBER
                               }else if(cell.textViewTitle.text == "Address" || cell.textViewTitle.text == "पता"){
                                   cell.textViewData.text = electricMainDetailsDB[0].ADD1 + "," + electricMainDetailsDB[0].ADD2 + "," + electricMainDetailsDB[0].ADD3
                               }else if(cell.textViewTitle.text == "Bill Month" || cell.textViewTitle.text == "बिल माह"){
                                  let datee = self.dateFormatChange(inputDateStr: electricBillDetailsDB[0].BILL_MNTH, inputFormat: "yyyy/MM", outputFromat: "MMM yyyy")
                                   cell.textViewData.text = datee
                               }
                }
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellButton", for: indexPath) as! UITableViewCell
                
                return cell
            }
             
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            if(section == 2 ){
               return 10.0
           }
            return 0.0
         }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
              
           
              let header = tableView.dequeueReusableCell(withIdentifier: "header") as! UITableViewCell
            return header
              
          }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if(indexPath.section == 2){
                return 100
            }
            return UITableViewAutomaticDimension
            
        }
}
