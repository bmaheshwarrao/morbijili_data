//
//  PaymentStatusViewController.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 20/04/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//


import UIKit
import DropDown
import CoreData
import AlamofireSoap
import Alamofire
import SwiftyXMLParser
import Foundation
var moveInt = 0
class PaymentStatusViewController: CommonVSClass {
var respoStr  = String()
    var amt = Float()
    var mobile = String()
    var bdvc =  BDViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = billPayment
        
        self.bdvc = BDViewController.init(message: BillTrans.resp , andToken: nil, andEmail: nil, andMobile: BillTrans.mobile, andAmount: BillTrans.amt)
                             print(respoStr)
                           self.bdvc.delegate = self
       
        SHKActivityIndicator.current().displayCompleted("")
        self.bdvc.navigationController?.navigationBar.isHidden = true
       
        self.navigationController?.pushViewController(self.bdvc, animated: true)
     //   moveInt = 1
        // Do any additional setup after loading the view.
//        NotificationCenter.default.addObserver(self, selector: #selector(self.callsync), name: NSNotification.Name(rawValue: "ShowTrans"), object: nil)
    }
//    @objc func callsync(){
//       let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if(moveInt == 1){
            self.dismiss(animated: true, completion: nil)
        }else{
            moveInt = 1
        }
        self.tabBarController?.tabBar.isHidden = true
         //self.title = FilterGraphStruct.title
      let nav = self.navigationController?.navigationBar
      nav?.isHidden = false
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: baseColor, alpha: 1.0)
      nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
    }
    
    func sendToPay(messageReturn : String,status : String){
              self.startLoadingPK(view: self.view, text: "please wait....")
              let dateFormatterInv = DateFormatter()
              dateFormatterInv.timeZone = NSTimeZone.system
              dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
              let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
              let invStrUnique = dateFormatterInv.string(from: Date())
              let request_no = invStrUnique + mobile
       
          
          
          
//          CSEB|070012674895|SIC48727897067|011213169330|00000010.00|IC4|NA|10|INR|DIRECT|NA|NA|0.00|21-04-2020 13:52:23|0300|NA|NA|NA|NA|NA|NA|NA|NA|NA|Y|AD80C83699627053E76BB9B3129C8294616D880F0D332F094273018596F3D765\n<script type=\"text/javascript\">\n\tfunction getMsg(){\n\t\tvar msg = \"CSEB|070012674895|SIC48727897067|011213169330|00000010.00|IC4|NA|10|INR|DIRECT|NA|NA|0.00|21-04-2020 13:52:23|0300|NA|NA|NA|NA|NA|NA|NA|NA|NA|Y|AD80C83699627053E76BB9B3129C8294616D880F0D332F094273018596F3D765\";\n\t\tAndroidFunction.gotMsg(msg);\n\t}\n\tgetMsg();\n</script>
//
//
//        MA_pgmsgsub| CSEB|1002105828|RHMP8580266268|036361|280.00|HMP|489377|02|INR|DIRECT|05-NA|NA|00000000.00|03-03-2020 07:27:23|0300|NA|SMT- SARSWATI GHRITLAHARE .|702038750038|CSPDCLIMA|NA|NA|NA|NA|NA|PGS10001-Success|BB4544D063A7D5B21473EA18F196EE3D4184A3A3F74AA13F263DA92F516DAEED
//        |res|Sec_Key|request_id
        var responseComponents = messageReturn.components(separatedBy: "|")
        var respoStrw = messageReturn.components(separatedBy: "\n")
        let respoStr1 = respoStrw[0]
        let transID = responseComponents[2]
        let transID1 = responseComponents[3]
        let amt = responseComponents[4]
        let typee = responseComponents[5]
        let date = responseComponents[13]
        let yess = responseComponents[24]
         let statUP = responseComponents[15]
        let keydata = responseComponents[25].components(separatedBy: "\n")
        
        BillTransEnd.tip = yess
        if(statUP == "0300"){
          BillTransEnd.status = "Success"
        }else{
            BillTransEnd.status = "Fail"
        }
        BillTransEnd.date = date
        BillTransEnd.refno = transID
        BillTransEnd.tip = yess
        BillTransEnd.paytype = typee
        BillTransEnd.bpno = BillTrans.bpno
        BillTransEnd.amt = String(BillTrans.amt)
     //   let respoStr1 = "CSEB|\(BillTrans.bpno)|\(transID)|\(transID1)|\(amt)|\(typee)|HMP|489377|02|INR|DIRECT|05-NA|NA|00000000.00|\(date)|0300|NA|\(BillTrans.billDis.name)|\(BillTrans.billDis.billno)|CSPDCLIMA|NA|NA|NA|NA|NA|PGS10001-\(BillTransEnd.status)|\(keydata[0])"
        
        let hmac_sha1 = respoStr1.hmac(algorithm: .sha256, key: "z8bKEaX1XEtB")
        let strPGMsgfinal = respoStr1 + "|\(hmac_sha1.uppercased())"
          
              let message = "MA_pgmsgsub|\(respoStr1)|res|\(secID)"
              let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
             AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_string", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
              self.stopLoadingPK(view: self.view)
             guard let respo = response.value else {
                 return
             }
              print(response.value)
             let xml = try! XML.parse(respo)
             let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_stringResponse"]["MA_cons_stringResult"].text
             if let result = accessor {
                 let resultInt = Int(result)
//                 if(resultInt! > 0){
//                    self.showSingleButtonAlertWithActionMessage(title: "Payment Status", message: "", buttonTitle: "OK") {
//                                               self.dismiss(animated: true, completion: nil)
//
//                                           }
//
//                 }else{
//
//                 }
                self.showSingleButtonAlertWithActionMessage(title: "Payment Status", message: yess, buttonTitle: "OK") {
                   // self.dismiss(animated: true, completion: nil)
                     
                  var storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
                         storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    }else{
                         storyBoard = UIStoryboard(name: "MainHindi", bundle: nil)
                    }
                   
                                         
                                             
                                             let genVC = storyBoard.instantiateViewController(withIdentifier: "PaymentRecieptNavViewController") as! PaymentRecieptNavViewController
                                 
                                       genVC.modalPresentationStyle = .fullScreen
                                       self.present(genVC, animated: false, completion: nil)
                }
                
             }
             }

         }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PaymentStatusViewController:  LibraryPaymentStatusProtocol {
     // MARK : - Payment status protocol method
       func paymentStatus(_ message: String!) {
        moveInt = 0
        navigationController?.popToViewController(self, animated: true)
        
           print("payment status response [\(message)]")
           //[self showAlert:message];
           var responseComponents = message.components(separatedBy: "|")
           if responseComponents.count >= 25 {
               var statusCode = responseComponents[14]
            sendToPay(messageReturn: message, status: "Success")
            
           } else {
               
            
            self.showSingleButtonAlertWithActionMessage(title: "Payment Status", message: "Something went wrong", buttonTitle: "OK") {
                self.dismiss(animated: true, completion: nil)
                 
            }
           }
       
       }
       
       func onError(_ exception: NSException?) {
           if let anException = exception {
               print("Exception got in Merchant App \(anException)")
           }
       }
       
       func tryAgain() {
           print("Try again method in Merchant App")
       }
       
       func cancelTransaction() {
           print("Cancel Transaction method in Merchant App")
       }
       
}
