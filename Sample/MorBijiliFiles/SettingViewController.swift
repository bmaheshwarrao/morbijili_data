//
//  SettingViewController.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 13/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import CoreData
class SettingViewController: CommonVSClass {
   
    
 //MARK: ================== IBOutlets ================
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: ================== Properties ================
   var settingArray = ["Language","Feedback","Privacy"]
  var settingImgArray = ["language","feedback","legal"]
    
     var titleSet = String()
    //MARK: ================== VC Life Cycle ================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.title = titleSet
       settingArray = [langg,feedb,privacyy]
        // Do any additional setup after loading the view.
    }
      //MARK: ================== IB ACtions ================
   
    @IBAction func btnPrivacyClicked(_ sender: UIButton) {
        
        
    }
    @IBAction func btnFeedbackClicked(_ sender: UIButton) {
        
        
    }
    @IBAction func btnLanguageClicked(_ sender: UIButton) {
        var storyBoard = UIStoryboard(name: "Main", bundle: nil)
               if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
                    storyBoard = UIStoryboard(name: "Main", bundle: nil)
               }else{
                    storyBoard = UIStoryboard(name: "MainHindi", bundle: nil)
               }
               
        if(sender.tag == 0){
        
                     let genVC = storyBoard.instantiateViewController(withIdentifier: "ChangeLanguageViewController") as! ChangeLanguageViewController
            genVC.titleSet = langg
                     self.navigationController?.pushViewController(genVC, animated: true)
        }else if(sender.tag == 1){
            
            let genVC = storyBoard.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
            genVC.titleSet = feedb
            self.navigationController?.pushViewController(genVC, animated: true)
        }else{
            
        }
    }
    
    @IBAction func btnLogoutClicked(_ sender: Any) {
        callExist()
    }
    
   
    
    //MARK: ================== Custom Methods ================
     func callExist(){
            
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                
                let actionSheetController: UIAlertController = UIAlertController(title: self.confirmation, message: self.loggOut, preferredStyle: .actionSheet)
                
                let camera = UIAlertAction(title: self.loggOu1, style: .default, handler: { (alert) in
                    
                    UserDefaults.standard.set("hi", forKey: "Language")
                    UserDefaults.standard.set(false, forKey: "isLogOut")
                
                    UserDefaults.standard.set("", forKey: "mobile")
                   UserDefaults.standard.set(false, forKey: "isLogin")
                    
                    
//                    let systemSoundID: SystemSoundID = 1315
//                    UserDefaults.standard.set(systemSoundID, forKey: "soundFileEnable")
                    
                    self.deleteAllLocalData()
                   
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let mainVC = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    mainVC.modalPresentationStyle = .fullScreen
                    self.present(mainVC, animated: true, completion: nil)
                    
                })
                
                let cancel = UIAlertAction(title: self.cancel, style: .cancel, handler: { (alert) in
                    
                })
                
                actionSheetController.addAction(camera)
                
                actionSheetController.addAction(cancel)
                self.present(actionSheetController, animated: true, completion: nil)
                
                
            }
            
            
        }
      
        var localDataArray = [String]()
        func deleteAllLocalData()
        {
            
        
          
            localDataArray = [String]()
          localDataArray = ["Partner"]
            
            for i in 0..<localDataArray.count{
                
                let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: localDataArray[i])
                let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
                
                do {
                    try context.execute(batchDeleteRequest)
                    
                } catch {
                    // Error Handling
                }
                
            }
        }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: ================== Extension ================
extension SettingViewController: UITableViewDelegate,UITableViewDataSource {
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        }
        func numberOfSections(in tableView: UITableView) -> Int {
            return 2
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if(section == 1){
                return settingArray.count
            }
            return 1
            
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if(indexPath.section == 0){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SettingTableViewCell
            let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
                cell.lblMobile.text = mobile
            return cell
            }else{
              let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! SettingDataTableViewCell
                cell.btnTitle.tag = indexPath.row
                cell.lblTitle.text = settingArray[indexPath.row]
                cell.img.image = UIImage(named: settingImgArray[indexPath.row])
                cell.img.setImageColor(color: UIColor.init(hexString: baseColor, alpha: 1.0)!)
                return cell
            }
             
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            if(section == 1 ){
               return 2.0
           }
            return 0.0
         }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
              
           
              let header = tableView.dequeueReusableCell(withIdentifier: "header") as! UITableViewCell
            return header
              
          }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if(indexPath.section == 0){
                return 230
            }else if(indexPath.section == 2){
                return UIScreen.main.bounds.height - (250 + (100 * 4))
            }else{
                return 70
            }
            //return UITableViewAutomaticDimension
            
        }
}
extension UIImageView {
  func setImageColor(color: UIColor) {
    let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
    self.image = templateImage
    self.tintColor = color
  }
}
