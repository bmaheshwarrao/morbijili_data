//
//  OTPViewController.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 19/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import AlamofireSoap
import Alamofire
import CryptoSwift
import SwiftyXMLParser
class OTPViewController: CommonVSClass {

    @IBOutlet weak var lblVeri: UILabel!
    @IBOutlet weak var widthResend: NSLayoutConstraint!
    @IBOutlet weak var btnResendOTP: UIButton!
    @IBOutlet weak var txtOTP: DesignableUITextField!
    @IBOutlet weak var lblOtp: UILabel!
    @IBOutlet weak var btnVerify: UIButton!
    var mobile = String()
    var bpNo = String()
    var invStrno = String()
    var reqNo = String()
    var add = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        lblVeri.text = verifyyOTP
        lblOtp.textAlignment = .center
        txtOTP.placeholder = self.enterOtp
        verify = 0
        btnResendOTP.setTitle(resendOtp, for: .normal)
        txtOTP.keyboardType = .numberPad
        btnVerify.setTitle(self.verifyy, for: .normal)
          self.startTimer()
         self.timer = SwiftTimer.repeaticTimer(interval: .seconds(1)) { timer in
                                                    self.startTimer()
                                                }
                                                self.timer.start()
        // Do any additional setup after loading the view.
    }
   
       var totalTime = 60
    var verify = 0
       @objc func startTimer(){
           btnResendOTP.isHidden = true
           widthResend.constant = 0
        if(totalTime == 60){
             lblOtp.text = "\(totalTime)" + " " + secCount
        }else{
           lblOtp.text = "\(timeFormatted(totalTime))" + " " + secCount
        }
           lblOtp.isHidden = false
          verify = 0
        btnVerify.setTitle(self.verifyy, for: .normal)
           if totalTime != 0 {
               totalTime -= 1
           } else {
               endTimer()
            verify = 1
               
               
              
                lblOtp.isHidden = true
              
           }
       }
       func endTimer() {
        btnResendOTP.isHidden = false
        widthResend.constant = 120
              timer.suspend()
          }
          func timeFormatted(_ totalSeconds: Int) -> String {
              let seconds: Int = totalSeconds % 60
              let minutes: Int = (totalSeconds / 60) % 60
              //     let hours: Int = totalSeconds / 3600
              return String(format: "%02d",  seconds)
          }
    
    @IBAction func btnCloseClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func verifyOTP(_ sender: UIButton) {
        
        
            verifyOTP()
     
       }
    
    @IBAction func btnResendOTPClicked(_ sender: Any) {
        
            sendOTP()
        
    }
    
    func sendOTP(){
        self.startLoadingPK(view: self.view,text : "Sending OTP")
           let dateFormatterInv = DateFormatter()
               dateFormatterInv.timeZone = NSTimeZone.system
               dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
           
               let  invStrUnique = dateFormatterInv.string(from: Date())
           let request_no = invStrUnique + mobile
           var message = String()
        if(add == "1"){
            "MA_getotp|\(mobile)|600|3|\(invStrno)|\(secID)|\(request_no)"
        }else{
        if(bpNo == ""){
            
            message = "MA_getotp|\(mobile)|600|1|\(invStrno)|\(secID)|\(request_no)"
        }else{
        message = "MA_getotpconsregis|\(mobile)|600|2|\(invStrno)|\(bpNo)|\(secID)|\(request_no)"
            
            
        }
        }
        
        
            print(message)

            let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
            print(encode)
            
           
        AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_string", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
            print("Request: \(response.value)")
            print("Request: \(response.value)")
            guard let respo = response.value else {
                return
            }
            self.stopLoadingPK(view: self.view)
            let xml = try! XML.parse(respo)
            let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_stringResponse"]["MA_cons_stringResult"].text
            if let result = accessor {
                let resultInt = Int(result)
                if(resultInt! > 0){
                    self.btnVerify.setTitle(self.verifyy, for: .normal)
//                    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(OTPViewController.startTimer), userInfo: nil, repeats: true);
                    self.totalTime = 60
                    self.startTimer()
                     self.timer = SwiftTimer.repeaticTimer(interval: .seconds(1)) { timer in
                                           self.startTimer()
                                       }
                                       self.timer.start()
                    
                    
                    
                }else{
                    self.showSingleButtonWithMessage(title: "सूचना", message: "कुछ तकनीकी कारणों की वजह से जानकारी उपलब्ध नहीं हो पा रही है या फिर आपका मोबाइल नंबर किसी भी उपभोगता क्र. से लिंक नहीं है , कृपया अपना मोबाइल नंबर लिंक करिये |", buttonName: "OK")
                }
            }
            
        }

    }
    var timer = SwiftTimer(interval: .seconds(1)) {_ in
           print("fire")
       }
    func verifyOTP(){
        if(txtOTP.text == ""){
                   self.view.makeToastHZL(putOTp)
               }else{
            self.startLoadingPK(view: self.view,text : "Verifying OTP")
         let dateFormatterInv = DateFormatter()
             dateFormatterInv.timeZone = NSTimeZone.system
             dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
         
             let  invStrUnique = dateFormatterInv.string(from: Date())
         let request_no = invStrUnique + mobile
         var message = String()
            if(add == "1"){
                message = "MA_verifyotp|\(mobile)|\(txtOTP.text!)|\(invStrno)|3|\(secID)|\(request_no)"
            }else{
         if(bpNo == ""){
              message = "MA_verifyotp|\(mobile)|\(txtOTP.text!)|\(invStrno)|1|\(secID)|\(request_no)"
         }else{
         message = "MA_verifyotpconsregis|\(mobile)|\(txtOTP.text!)|\(invStrno)|2|\(bpNo)|\(secID)|\(request_no)"
         }
            }
            
            

            let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
            
            print(message)
           
        AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_string", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
         print("Request: \(response.value)")
         guard let respo = response.value else {
             return
         }
            self.stopLoadingPK(view: self.view)
         let xml = try! XML.parse(respo)
         let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_stringResponse"]["MA_cons_stringResult"].text
         if let result = accessor {
             let resultInt = Int(result)
             if(resultInt! > 0){
                if(self.add == "1"){
                    self.dismiss(animated: true, completion: nil)
                    NotificationCenter.default.post(name: NSNotification.Name.init("AddConsumer"), object: nil)
                }else{
                UserDefaults.standard.set("hi", forKey: "Language")
                  UserDefaults.standard.set(true, forKey: "isLogin")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let homeVC  =  storyboard.instantiateViewController(withIdentifier: "HomeNavViewController") as! HomeNavViewController
                homeVC.modalPresentationStyle = .fullScreen
                self.present(homeVC, animated: true, completion: nil)
                }
             }else{
                self.showSingleButtonWithMessage(title: self.confirmation, message: self.mismatchOTP, buttonName: "OK")
             }
         }
         
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
