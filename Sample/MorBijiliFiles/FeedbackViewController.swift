//
//  FeedbackViewController.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 12/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import DropDown
import CoreData
import Alamofire
import AlamofireSoap
import SwiftyXMLParser
class FeedbackViewController: CommonVSClass {
   
    
 //MARK: ================== IBOutlets ================
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: ================== Properties ================
     let dropDown = DropDown()
    var BillDetailsAPI = FeedbackModelDataAPI()
    var billDetailsDB : [BillDisplay] = []
    var ComplaintDetailsAPI = ComplainTypeModelDataAPI()
    var cellData : MainMorPartnerTableViewCell!
    var cellDataRate : MorRateTableViewCell!
    var rateValue = String()
     var titleSet = String()
    var PartnerDB:[Partner] = []
    var partnerBDValue : [String] = []
    var cellPhotoData : PhotoMeterTableViewCell!
    var cellDataText : MorDetailsTableViewCell!
    var cellDropData : MorDropDetailsTableViewCell!
    var cellTextData : MorDetailsTableViewCell!
    var consumptionDB : [ConsumptionDisplay] = []
      var electricBillDetailsDB : [ElectricBillDetailsDisplay] = []
      var electricMainDetailsDB : [ElectricMainDetailsDisplay] = []
      var electricUnitsDetailsDB : [ElectricBillUnitDetailsDisplay] = []
      var electricBillBPDetailsDB : [ElectricBillBPDataDetailsDisplay] = []
      var electricBillLNDetailsDB : [ElectricBillLNDetailsDisplay] = []
      var electricOfficeDetailsDB : [ElectricOfficeDetailsDisplay] = []
    let imagePicker = UIImagePickerController()
    var imageData : Data? = nil
    var cnt = 0
    var section1 : [String] = ["Name","Mobile No","Address","Feedback"]
    
    
    
    //MARK: ================== VC Life Cycle ================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        getPartnerData()
        self.title = titleSet
        
        if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
        section1  = ["Name","Mobile No","Address","Feedback"]
        }else{
        section1  = ["नाम","मोबाइल नंबर","पता","फीडबैक"]
         }
        // Do any additional setup after loading the view.
    }
      //MARK: ================== IB ACtions ================
   
    /// Rate Click Event --- Start
    
    @IBAction func btnRate1Clicked(_ sender: UIButton) {
        rate(count: 1)
    }
    @IBAction func btnRate2Clicked(_ sender: UIButton) {
        rate(count: 2)
       }
    @IBAction func btnRate3Clicked(_ sender: UIButton) {
        rate(count: 3)
       }
    @IBAction func btnRate4Clicked(_ sender: UIButton) {
        rate(count: 4)
       }
    @IBAction func btnRate5Clicked(_ sender: UIButton) {
        rate(count: 5)
       }
    
    
    
    
    
    /// --- End
    @IBAction func btnSubmitFeedbackClicked(_ sender: UIButton) {
        if(cellTextData.textViewData.text == ""){
            self.view.makeToastHZL("Please Enter Feedback")
        }else if(rateValue == ""){
            self.view.makeToastHZL("Please rate")
        }else{
            startLoadingPK(view: self.view, text: "Sending Feedback")
            let dateFormatterInv = DateFormatter()
            dateFormatterInv.timeZone = NSTimeZone.system
            dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
            let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
            let invStrUnique = dateFormatterInv.string(from: Date())
            let request_no = invStrUnique + mobile
            print(cellData.lblPartner.text!)
           // print(billDetailsDB[0].name)
            print(cellTextData.textViewData.text!)
            print(self.rateValue)
           
            let message = "MA_feedback|\(cellData.lblPartner.text!)|\(mobile)|\(electricMainDetailsDB[0].BP_NAME)|\(cellTextData.textViewData.text!)|\(self.rateValue)|\(secID)|\(request_no)"
            let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
            
            AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_string", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
                self.stopLoadingPK(view: self.view)
            guard let respo = response.value else {
                return
            }
             print(response.value)
            let xml = try! XML.parse(respo)
            let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_stringResponse"]["MA_cons_stringResult"].text
            if let result = accessor {
                let resultInt = Int(result)
                if(resultInt! > 0){
                   
                    self.showSingleButtonAlertWithAction(title: self.feedCons, buttonTitle: self.okay) {
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                    
                   
                }else{
                    self.showSingleButtonWithMessage(title: self.confirmation, message: self.NotfeedCons, buttonName: self.okay)
                }
            }
            }
         
        }
     
    }
    
    @IBAction func btnPartnerClicked(_ sender: UIButton) {
        self.dropDown.anchorView = cellData.btnPartner // UIView or UIBarButtonItem
        self.dropDown.width = cellData.btnPartner.frame.size.width
        self.dropDown.bottomOffset = CGPoint(x: 0, y:(self.dropDown.anchorView?.plainView.bounds.height)!)
        self.dropDown.direction = .bottom
        // The list of items to display. Can be changed dynamically
        self.dropDown.dataSource = partnerBDValue
        self.dropDown.show()
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected tem: \(item) at index: \(index)")
            self.cellData.lblPartner.text = item
            self.getPaymentBillDetailsData(bpno: item)
           
        }
    }
    
    //MARK: ================== Custom Methods ================
       @objc func rate(count : Int){
        rateValue = String(count)
        cellDataRate.btnRate1.setImage(UIImage(named: "starDataGray"), for: .normal)
        cellDataRate.btnRate2.setImage(UIImage(named: "starDataGray"), for: .normal)
        cellDataRate.btnRate3.setImage(UIImage(named: "starDataGray"), for: .normal)
        cellDataRate.btnRate4.setImage(UIImage(named: "starDataGray"), for: .normal)
        cellDataRate.btnRate5.setImage(UIImage(named: "starDataGray"), for: .normal)
        if(count == 1){
        cellDataRate.btnRate1.setImage(UIImage(named: "starDataPink"), for: .normal)
        }else if(count == 2){
        cellDataRate.btnRate1.setImage(UIImage(named: "starDataPink"), for: .normal)
        cellDataRate.btnRate2.setImage(UIImage(named: "starDataPink"), for: .normal)
        }else if(count == 3){
        cellDataRate.btnRate1.setImage(UIImage(named: "starDataPink"), for: .normal)
        cellDataRate.btnRate2.setImage(UIImage(named: "starDataPink"), for: .normal)
        cellDataRate.btnRate3.setImage(UIImage(named: "starDataPink"), for: .normal)
        }else if(count == 4){
        cellDataRate.btnRate1.setImage(UIImage(named: "starDataPink"), for: .normal)
        cellDataRate.btnRate2.setImage(UIImage(named: "starDataPink"), for: .normal)
        cellDataRate.btnRate3.setImage(UIImage(named: "starDataPink"), for: .normal)
        cellDataRate.btnRate4.setImage(UIImage(named: "starDataPink"), for: .normal)
        }else if(count == 5){
        cellDataRate.btnRate1.setImage(UIImage(named: "starDataPink"), for: .normal)
        cellDataRate.btnRate2.setImage(UIImage(named: "starDataPink"), for: .normal)
        cellDataRate.btnRate3.setImage(UIImage(named: "starDataPink"), for: .normal)
        cellDataRate.btnRate4.setImage(UIImage(named: "starDataPink"), for: .normal)
        cellDataRate.btnRate5.setImage(UIImage(named: "starDataPink"), for: .normal)
        }
    }
    @objc func getPartnerData() {
    
            self.PartnerDB = [Partner]()
            do {
    
                self.PartnerDB = try context.fetch(Partner.fetchRequest())
                //self.refresh.endRefreshing()
                if(self.PartnerDB.count > 0) {
                    for i in 0...self.PartnerDB.count - 1 {
                        partnerBDValue.append(self.PartnerDB[i].partner!)
                    }
                   
                }else{
                   
               }
                self.tableView.reloadData()
    
            } catch {
                print("Fetching Failed")
            }
        }
       
    @objc func getPaymentBillDetailsData(bpno : String){
        
     
        
        BillDetailsAPI.serviceCalling(obj: self , bpNo : bpno) { (dict) in
            
            if(dict.count > 0){
                self.electricMainDetailsDB = dict as! [ElectricMainDetailsDisplay]
                self.tableView.reloadData()
            }
            
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: ================== Extension ================
extension FeedbackViewController: UITableViewDelegate,UITableViewDataSource {
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        }
        func numberOfSections(in tableView: UITableView) -> Int {
            return 5
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if(section == 0 ){
            return 1
            }else if(section == 1){
                if(electricMainDetailsDB.count > 0){
                return 3
                }
                return 0
            }else {
                if(electricMainDetailsDB.count > 0){
                return 1
                }
                return 0
            }
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if(indexPath.section == 0){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MainMorPartnerTableViewCell
            cellData = cell
            if(PartnerDB.count > 0){
                
                if(cnt == 0){
                    cell.lblPartner.text = partnerBDValue[indexPath.row]
                getPaymentBillDetailsData(bpno: partnerBDValue[0])
                }
         
            }else{
               cell.lblPartner.text = "---"
            }
            return cell
            }else if(indexPath.section == 1){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cellData", for: indexPath) as! MorDetailsTableViewCell
               
                cell.textViewTitle.text = section1[indexPath.row]
                if(cell.textViewTitle.text == "Name" || cell.textViewTitle.text == "नाम"){
                    cell.textViewData.text = electricMainDetailsDB[0].BP_NAME
                }else if(cell.textViewTitle.text == "Mobile No" || cell.textViewTitle.text == "मोबाइल नंबर"){
                    cell.textViewData.text = electricMainDetailsDB[0].TEL_NUMBER
                }else if(cell.textViewTitle.text == "Address" || cell.textViewTitle.text == "पता"){
                    cell.textViewData.text = electricMainDetailsDB[0].ADD1 + "," + electricMainDetailsDB[0].ADD2 + "," + electricMainDetailsDB[0].ADD3
                }
                return cell
            }else if(indexPath.section == 2){
                  let cellText = tableView.dequeueReusableCell(withIdentifier: "cellDataText", for: indexPath) as! MorDetailsTableViewCell
                    cellText.textViewTitle.text = section1[3]
                   cellTextData = cellText
                cellText.textViewData.keyboardType = .default
                   return cellText
                }else if(indexPath.section == 3){
                  let cellRate = tableView.dequeueReusableCell(withIdentifier: "cellRate", for: indexPath) as! MorRateTableViewCell
                    
                   cellDataRate = cellRate
               
                   return cellRate
                }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellButton", for: indexPath) as! UITableViewCell
                
                return cell
            }
             
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            if(section == 2 || section == 4 || section == 3){
               return 10.0
           }
            return 0.0
         }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
              
           
              let header = tableView.dequeueReusableCell(withIdentifier: "header") as! UITableViewCell
            return header
              
          }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if(indexPath.section == 4 || indexPath.section == 3 || indexPath.section == 2){
                return 100
            }
            return UITableViewAutomaticDimension
            
        }
}

