//
//  ChangeLanguageViewController.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 16/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import DLRadioButton
import Localize_Swift
class ChangeLanguageViewController: CommonVSClass {

    @IBOutlet weak var btnLang: UIButton!
    @IBOutlet weak var btnHin: DLRadioButton!
    @IBOutlet weak var btnEng: DLRadioButton!
     var titleSet = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        if(UserDefaults.standard.value(forKey: "Language") as! String == "en"){
            btnEng.isSelected = true
        }else{
            btnHin.isSelected = true
        }
        btnLang.setTitle(langg, for: .normal)
        self.title =   titleSet 
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnLanguageClicked(_ sender: UIButton) {
        
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                        
                        let actionSheetController: UIAlertController = UIAlertController(title: "Alert", message: self.bhasha, preferredStyle: .actionSheet)
                        
                        let camera = UIAlertAction(title: self.langCall, style: .default, handler: { (alert) in
                            
                            
                            self.changeLang()
                            
                        })
                        
                        let cancel = UIAlertAction(title: self.cancel, style: .cancel, handler: { (alert) in
                            
                        })
                        
                        actionSheetController.addAction(camera)
                        
                        actionSheetController.addAction(cancel)
                        self.present(actionSheetController, animated: true, completion: nil)
                        
                        
                    }
        
        
        
      
                   

    }
    func changeLang(){
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
              
                         if(langCall == "English" || langCall == "अंग्रेजी"){
                             
                             
                             UserDefaults.standard.set("en", forKey: "Language")
                              storyboard = UIStoryboard(name: "Main", bundle: nil)
                         }else{
                            
                           
                             UserDefaults.standard.set("hi", forKey: "Language")
                            
                            
                              storyboard = UIStoryboard(name: "MainHindi", bundle: nil)
                         }
                         
                          
                                
                                let loginVC  =  storyboard.instantiateViewController(withIdentifier: "HomeNavViewController") as! HomeNavViewController
                         
                         loginVC.modalPresentationStyle = .fullScreen
                         //self.present(loginVC, animated: true, completion: nil)
                         let transition: UIView.AnimationOptions = .transitionFlipFromLeft
                         let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                                 rootviewcontroller.rootViewController = loginVC
                                 let mainwindow = (UIApplication.shared.delegate?.window!)!
                                 mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
                         mainwindow.makeKeyAndVisible()
    }
    var window: UIWindow?
    var langCall = String()
    @IBAction func radioButtonClicked(_ sender : DLRadioButton){
        for button in sender.selectedButtons() {
            langCall = (button.titleLabel!.text!)
         }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
