//
//  AddConsumerViewController.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 17/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import AlamofireSoap
import SwiftyXMLParser
import CoreData
import EzPopup
class AddConsumerViewController: CommonVSClass {
   
    
 //MARK: ================== IBOutlets ================
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: ================== Properties ================
     var titleSet = String()
     let dropDown = DropDown()
    var AddDetailsAPI = AddConsumerModelDataAPI()
    var PartnerDB : [String] = []
    var PartnerDBData:[Partner] = []
    var ComplaintDetailsAPI = ComplainTypeModelDataAPI()
    var cellData : MainMorPartnerTableViewCell!
    var billDetailsDB : [BillDisplay] = []
    var consumptionDB : [ConsumptionDisplay] = []
       var electricBillDetailsDB : [ElectricBillDetailsDisplay] = []
       var electricMainDetailsDB : [ElectricMainDetailsDisplay] = []
       var electricUnitsDetailsDB : [ElectricBillUnitDetailsDisplay] = []
       var electricBillBPDetailsDB : [ElectricBillBPDataDetailsDisplay] = []
       var electricBillLNDetailsDB : [ElectricBillLNDetailsDisplay] = []
       var electricOfficeDetailsDB : [ElectricOfficeDetailsDisplay] = []
    var partnerBDValue : [String] = []
    var cellPhotoData : PhotoMeterTableViewCell!
    var cellDataText : MorDetailsTableViewCell!
    var cellDropData : MorDropDetailsTableViewCell!
    var cellTextData : MorDetailsTableViewCell!
    var PartnerSharingAPI = ElectricityAddPartnerModelDataAPI()
    
    @IBOutlet weak var txtConsumerNo: DesignableUITextField!
    var cnt = 0
    var section1 : [String] = ["Name","Address"]
  
    
    
    //MARK: ================== VC Life Cycle ================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        txtConsumerNo.keyboardType = .numberPad
        self.title = titleSet
        getPartnerData()
        if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
            section1  = ["Name","Address"]
            txtConsumerNo.placeholder = "Enter Consumer No.(BP No.)"
        }else{
         section1  = ["नाम","पता"]
          txtConsumerNo.placeholder = "उपभोगता क्र. दर्ज करे"
          }
    
         NotificationCenter.default.addObserver(self, selector: #selector(self.add), name: NSNotification.Name(rawValue: "AddConsumer"), object: nil)
        // Do any additional setup after loading the view.
    }
      //MARK: ================== IB ACtions ================
   @objc func getPartnerData() {
    
             self.PartnerDBData = [Partner]()
             do {
     
                 self.PartnerDBData = try context.fetch(Partner.fetchRequest())
                 //self.refresh.endRefreshing()
                 if(self.PartnerDBData.count > 0) {
                     for i in 0...self.PartnerDBData.count - 1 {
                         partnerBDValue.append(self.PartnerDBData[i].partner!)
                     }
                    
                 }else{
                    
                }
                 
     
             } catch {
                 print("Fetching Failed")
             }
         }
    @IBAction func btnAddClicked(_ sender: UIButton) {
         if(self.electricMainDetailsDB.count > 0){
        
       DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                              
        let actionSheetController: UIAlertController = UIAlertController(title: self.confirmation, message: self.addSure + self.txtConsumerNo.text! + self.addSureRem, preferredStyle: .actionSheet)
                              
        let camera = UIAlertAction(title: self.addd, style: .default, handler: { (alert) in
                                  self.sendOTP()
                                  
                              })
                              
                              let cancel = UIAlertAction(title: self.cancel, style: .cancel, handler: { (alert) in
                                  
                              })
                              
                              actionSheetController.addAction(camera)
                              
                              actionSheetController.addAction(cancel)
                              self.present(actionSheetController, animated: true, completion: nil)
                              
                              
                          }
        
        
        
        }
    }
   
    @IBAction func btnClearlicked(_ sender: UIButton) {
        txtConsumerNo.text = ""
        self.billDetailsDB = []
         consumptionDB = []
         electricBillDetailsDB  = []
         electricMainDetailsDB  = []
         electricUnitsDetailsDB  = []
         electricBillBPDetailsDB  = []
         electricBillLNDetailsDB  = []
         electricOfficeDetailsDB = []
        cellDataVal.btnJoin.setTitle(self.addd, for: .normal)
        self.tableView.reloadData()
              
           
       }
    @IBAction func btnSearchlicked(_ sender: UIButton) {
        if(txtConsumerNo.text != ""){
            
            var se = 0
            if(PartnerDBData.count > 0){
                for i in 0...PartnerDBData.count - 1{
                    if(txtConsumerNo.text! == PartnerDBData[i].partner!){
                        se = 1
                        break;
                    }
                }
            }
            
            
            
            
            if(se == 1){
                self.view.makeToastHZL(addConsAlready)
            }else{
            
            
           getDetailsData(bpno: txtConsumerNo.text!)
            }
        }else{
            self.view.makeToastHZL(toastConsumer)
        }
              
          }
    
    
   var mobile = String()
    
    //MARK: ================== Custom Methods ================
    func sendOTP(){
           self.startLoadingPK(view: self.view,text : "Sending OTP")
              let dateFormatterInv = DateFormatter()
                  dateFormatterInv.timeZone = NSTimeZone.system
                  dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
              
                  let  invStrUnique = dateFormatterInv.string(from: Date())
              let request_no = invStrUnique + mobile
              var message = "MA_getotp|\(mobile)|600|3|\(invStrUnique)|\(secID)|\(request_no)"
           
               
           
           
           
               print(message)

               let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
               print(encode)
               
              
           AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_string", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
               print("Request: \(response.value)")
               print("Request: \(response.value)")
               guard let respo = response.value else {
                   return
               }
               self.stopLoadingPK(view: self.view)
               let xml = try! XML.parse(respo)
               let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_stringResponse"]["MA_cons_stringResult"].text
               if let result = accessor {
                   let resultInt = Int(result)
                   if(resultInt! > 0){
//                       self.btnVerify.setTitle(self.verifyy, for: .normal)
//   //                    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(OTPViewController.startTimer), userInfo: nil, repeats: true);
//                       self.totalTime = 60
//                       self.startTimer()
//                        self.timer = SwiftTimer.repeaticTimer(interval: .seconds(1)) { timer in
//                                              self.startTimer()
//                                          }
//                                          self.timer.start()
                    
                    
                       var storyboard = UIStoryboard(name: "Main", bundle: nil)
                       if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
                             storyboard = UIStoryboard(name: "Main", bundle: nil)
                       }else{
                          storyboard = UIStoryboard(name: "MainHindi", bundle: nil)
                         }
                       
                       let ZIVC = storyboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                                              ZIVC.mobile = self.mobile
                                              ZIVC.invStrno = invStrUnique
                    ZIVC.add = "1"
                                          
                                              
                                                   let popupVC = PopupViewController(contentController: ZIVC, position: .bottom(0.0), popupWidth: UIScreen.main.bounds.width, popupHeight: 350 )
                                                   popupVC.cornerRadius = 0
                                                   popupVC.canTapOutsideToDismiss = false
                                              self.present(popupVC, animated: true, completion: nil)
                       
                       
                   }else{
                    self.showSingleButtonWithMessage(title: self.confirmation, message: self.linkOTPerror, buttonName: self.okay)
                   }
               }
               
           }

       }
    @objc func add(){
         if(txtConsumerNo.text == ""){
            self.view.makeToastHZL(toastConsumer)
         }else{
              startLoadingPK(view: self.view, text: "Adding .....")
              let dateFormatterInv = DateFormatter()
                         dateFormatterInv.timeZone = NSTimeZone.system
                         dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
                         let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
                         let invStrUnique = dateFormatterInv.string(from: Date())
                         let request_no = invStrUnique + mobile
                         let message = "MA_add_remove_mobileno|\(txtConsumerNo.text!)|\(mobile)|ADD|\(secID)|\(request_no)"
                     print(message)
                         let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
                        AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_string", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
                            print("Request: \(response.value)")
                          
                        
                         
                            guard let respo = response.value else {
                              self.stopLoadingPK(view: self.view)
                                return
                            }
                            self.stopLoadingPK(view: self.view)
                            let xml = try! XML.parse(respo)
                            let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_stringResponse"]["MA_cons_stringResult"].text
                            if let result = accessor {
                                let resultInt = Int(result)
                                if(resultInt! > 0){
                                   
                                    self.showSingleButtonAlertWithAction(title: self.addCons, buttonTitle: self.okay) {
                                    self.getPartnerSharingData()
                                       self.navigationController?.popViewController(animated: true)
                                   }
                                 self.navigationController?.popViewController(animated: true)
                                }else{
                                    self.showSingleButtonWithMessage(title: self.confirmation, message: self.addNot, buttonName: self.okay)
                                }
                            }
                            
                        }
        }
          }
    @objc func getPartnerSharingData(){
          
       
          
       PartnerSharingAPI.serviceCalling( obj: self) { (dict) in
           self.deleteParnterData()
            self.PartnerDB = dict as! [String]
           if(self.PartnerDB.count > 0){
               for i in 0...self.PartnerDB.count - 1 {
                self.saveDirPathData(name: self.PartnerDB[i])
               }
           }
              
          }
          
          
      }
    func saveDirPathData(name:String){
           
           let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
           
           let tasks = Partner(context: context)
           tasks.partner = name
           (UIApplication.shared.delegate as! AppDelegate).saveContext()
       }
       func deleteParnterData()
       {
         
           
           
           let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
           
           let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Partner")
           let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
           
           do {
               try context.execute(batchDeleteRequest)
               
           } catch {
               // Error Handling
           }
       }
    @objc func getDetailsData(bpno : String){
        
     
        
        AddDetailsAPI.serviceCalling(obj: self , bpNo : bpno) { (dict) in
            
            if(dict.count > 0){
                self.electricMainDetailsDB = dict as! [ElectricMainDetailsDisplay]
                self.tableView.reloadData()
            }
            
        }
        
        
    }
    var cellDataVal : ButtonsTableViewCell!
    func replace(myString: String) -> String {
        var chars = Array(myString)
        // gets an array of characters
        for i in 2...7{
        chars[i] = "*"
        }
        let modifiedString = String(chars)
        return modifiedString
    }
    var gg = 0
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: ================== Extension ================
extension AddConsumerViewController: UITableViewDelegate,UITableViewDataSource {
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        }
        func numberOfSections(in tableView: UITableView) -> Int {
            if(self.electricMainDetailsDB.count > 0){
                if(self.electricMainDetailsDB[0].TEL_NUMBER != ""){
                    if(self.electricMainDetailsDB[0].TEL_NUMBER.count == 10){
                        return 3
                    }
                }
            }
            return 2
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if(section == 0 ){
                return section1.count
            }else{
                return 1
            }
        }
   
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if(indexPath.section == 0){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cellData", for: indexPath) as! MorDetailsTableViewCell
                cell.textViewTitle.text = section1[indexPath.row]
                  if(electricBillDetailsDB.count > 0){
                                             if(cell.textViewTitle.text == "Name" || cell.textViewTitle.text == "नाम"){
                                                 cell.textViewData.text = electricMainDetailsDB[0].BP_NAME
                                             }else if(cell.textViewTitle.text == "Address" || cell.textViewTitle.text == "पता"){
                                                 cell.textViewData.text = electricMainDetailsDB[0].ADD1 + "," + electricMainDetailsDB[0].ADD2 + "," + electricMainDetailsDB[0].ADD3
                                             }
                  }else{
                    cell.textViewData.text = "---"
                }
                return cell
            }else if(indexPath.section == 1){
                if(self.electricMainDetailsDB.count > 0){
                    if(self.electricMainDetailsDB[0].TEL_NUMBER != ""){
                        if(self.electricMainDetailsDB[0].TEL_NUMBER.count == 10){
                     let cell = tableView.dequeueReusableCell(withIdentifier: "cellData1", for: indexPath) as! MorDetailsTableViewCell
                      cell.textViewData.textColor = UIColor.init(hexString: "720e9e", alpha: 1.0)
                                            cell.textViewData.textAlignment = .center
                                            cell.textViewData.font = UIFont(name: "RobotoSlab-Bold", size: 16.0)
                                                cell.textViewTitle.textColor = UIColor.clear
                            mobile = electricMainDetailsDB[0].TEL_NUMBER
                            let mo = replace(myString: mobile)
                            
                            
                            cell.textViewData.text = add111 + mo + add112 + mo + add113
                            gg = 1
                            //            if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
                            //                     cell.textViewTitle.text = "Last Payment"
                            //                    }else{
                            //                     cell.textViewTitle.text = "पिछला भुगतान"
                            //                    }
                                                //  cell.textViewData.text = ""
                                                                return cell
                        }
                    }
                }
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellButton", for: indexPath) as! ButtonsTableViewCell
                if(gg == 0){
                    cell.btnJoin.setTitle(self.addd, for: .normal)
                }else{
                cell.btnJoin.setTitle(self.sendOtp, for: .normal)
                }
                    cellDataVal = cell
                
                               return cell
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellButton", for: indexPath) as! ButtonsTableViewCell
                if(gg == 0){
                    cell.btnJoin.setTitle(self.addd, for: .normal)
                }else{
                cell.btnJoin.setTitle(self.sendOtp, for: .normal)
                }
                cellDataVal = cell
                return cell
            }
             
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            if(section == 1 ){
               return 10.0
           }
            return 0.0
         }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
              
           
              let header = tableView.dequeueReusableCell(withIdentifier: "header") as! UITableViewCell
            return header
              
          }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if(indexPath.section == 1 || indexPath.section == 2){
                return 100
            }
            return UITableViewAutomaticDimension
            
        }
}
extension String {
    mutating func insert(string:String,ind:Int) {
        self.insert(contentsOf: string, at:self.index(self.startIndex, offsetBy: ind) )
    }
}
