//
//  MorRateTableViewCell.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 12/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class MorRateTableViewCell: UITableViewCell {

    @IBOutlet weak var btnRate1: UIButton!
    @IBOutlet weak var btnRate2: UIButton!
    @IBOutlet weak var btnRate3: UIButton!
    @IBOutlet weak var btnRate4: UIButton!
    @IBOutlet weak var btnRate5: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
