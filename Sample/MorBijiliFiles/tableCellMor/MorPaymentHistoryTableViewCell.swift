//
//  MorPaymentHistoryTableViewCell.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 13/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class MorPaymentHistoryTableViewCell: UITableViewCell {
    @IBOutlet weak var lblSino: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblSino.font =   UIFont(name: "RobotoSlab-Regular", size: 11)!
        lblDate.font =   UIFont(name: "RobotoSlab-Regular", size: 11)!
        lblAmount.font =   UIFont(name: "RobotoSlab-Regular", size: 11)!
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
