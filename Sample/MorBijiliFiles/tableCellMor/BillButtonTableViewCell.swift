//
//  BillButtonTableViewCell.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 14/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class BillButtonTableViewCell: UITableViewCell {

  
    @IBOutlet weak var btnStaff1: UIButton!
    @IBOutlet weak var btnStaff2: UIButton!
    @IBOutlet weak var textViewStaff2: UITextView!
    @IBOutlet weak var textViewStaff1: UITextView!
    @IBOutlet weak var btnOffice: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
