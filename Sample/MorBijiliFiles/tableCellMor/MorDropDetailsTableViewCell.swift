//
//  MorDropDetailsTableViewCell.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 12/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class MorDropDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var btnType: UIButton!
       @IBOutlet weak var viewType: UIView!
       @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var textViewTitle: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewType.layer.borderColor = UIColor.black.cgColor
        viewType.layer.borderWidth = 1.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
