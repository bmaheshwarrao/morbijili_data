//
//  PhotoMeterTableViewCell.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 12/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class PhotoMeterTableViewCell: UITableViewCell {

    @IBOutlet weak var btnImage: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var textViewTitle: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
