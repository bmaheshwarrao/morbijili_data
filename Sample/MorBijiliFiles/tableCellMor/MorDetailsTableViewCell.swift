//
//  MorDetailsTableViewCell.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 11/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class MorDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var textViewData: UITextView!
    @IBOutlet weak var textViewTitle: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
