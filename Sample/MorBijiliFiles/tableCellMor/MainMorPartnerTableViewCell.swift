//
//  MainMorPartnerTableViewCell.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 11/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class MainMorPartnerTableViewCell: UITableViewCell {

    @IBOutlet weak var btnPartner: UIButton!
    @IBOutlet weak var viewPartner: UIView!
    @IBOutlet weak var lblPartner: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewPartner.layer.borderColor = UIColor.black.cgColor
        viewPartner.layer.borderWidth = 1.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
