//
//  MorGraphConsuptionTableViewCell.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 12/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import Charts
class MorGraphConsuptionTableViewCell: UITableViewCell {
    @IBOutlet weak var lblYAxis: UILabel!
       @IBOutlet weak var view1: UIView!
       @IBOutlet weak var view2: UIView!
       @IBOutlet weak var view3: CardView!
       @IBOutlet weak var combinedChart: CombinedChartView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblYAxis.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
        lblYAxis.font =  UIFont(name: "RobotoSlab-Regular", size: 13)!
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
class MorConsuptionTableViewCell: UITableViewCell {
    @IBOutlet weak var lblBillMonth: UILabel!
    @IBOutlet weak var lblMeterReading: UILabel!
    @IBOutlet weak var lblPowerConsumption: UILabel!
    @IBOutlet weak var lblBillAAdhar: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblBillMonth.font =   UIFont(name: "RobotoSlab-Regular", size: 11)!
         lblMeterReading.font =   UIFont(name: "RobotoSlab-Regular", size: 11)!
         lblPowerConsumption.font =   UIFont(name: "RobotoSlab-Regular", size: 11)!
         lblBillAAdhar.font =   UIFont(name: "RobotoSlab-Regular", size: 11)!
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
class MorGraphRebateTableViewCell: UITableViewCell {
    @IBOutlet weak var lblYAxis: UILabel!
      @IBOutlet weak var lblMon1: UILabel!
    @IBOutlet weak var lblMon2: UILabel!
    @IBOutlet weak var lblMon3: UILabel!
    @IBOutlet weak var lblMon4: UILabel!
    @IBOutlet weak var lblMon5: UILabel!
    @IBOutlet weak var lblMon6: UILabel!
       @IBOutlet weak var combinedChart: CombinedChartView!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblYAxis.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
        lblYAxis.font =  UIFont(name: "RobotoSlab-Regular", size: 13)!
        
        lblMon1.font =   UIFont(name: "RobotoSlab-Regular", size: 8)!
        lblMon2.font =   UIFont(name: "RobotoSlab-Regular", size: 8)!
        lblMon3.font =   UIFont(name: "RobotoSlab-Regular", size: 8)!
        lblMon4.font =   UIFont(name: "RobotoSlab-Regular", size: 8)!
        lblMon5.font =   UIFont(name: "RobotoSlab-Regular", size: 8)!
        lblMon6.font =   UIFont(name: "RobotoSlab-Regular", size: 8)!
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
