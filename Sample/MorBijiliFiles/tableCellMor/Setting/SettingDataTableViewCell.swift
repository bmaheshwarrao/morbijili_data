//
//  SettingDataTableViewCell.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 18/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class SettingDataTableViewCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
