//
//  PaymentRecieptViewController.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 21/04/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import UIScreenCapture
class PaymentRecieptViewController: CommonVSClass {

    
 //MARK: ================== IBOutlets ================
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: ================== Properties ================
    
  
    var section1 : [String] = ["Name","Mobile No","Bill Number","Bill Month","Last Date of Bill Submission","Bill Amount"]
   var window: UIWindow?
    
    
    //MARK: ================== VC Life Cycle ================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
   
      //  self.title = titleSet
        if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
            section1  = ["B.P Number","Amount Paid","Payment Type","Payment Reference Number","Payment Status","Date and Time of Payment","Tip"]
             
        }else{
         section1  = ["उपभोक्ता क्र.(BP No.)","भुगतान राशि रु.","भुगतान का प्रकार","पेमेंट रिफरेन्स नंबर","पेमेंट स्टेटस","भुगतान दिनांक और समय","टिप"]
          
          }
        tableView.delegate = self
        tableView.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnDoneClicked(_ sender: Any) {
        var storyBoard = UIStoryboard(name: "Main", bundle: nil)
               if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
                    storyBoard = UIStoryboard(name: "Main", bundle: nil)
               }else{
                    storyBoard = UIStoryboard(name: "MainHindi", bundle: nil)
               }
        
        let loginVC  =  storyBoard.instantiateViewController(withIdentifier: "HomeNavViewController") as! HomeNavViewController
                          
                          loginVC.modalPresentationStyle = .fullScreen
                          //self.present(loginVC, animated: true, completion: nil)
                          let transition: UIView.AnimationOptions = .transitionFlipFromLeft
                          let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                                  rootviewcontroller.rootViewController = loginVC
                                  let mainwindow = (UIApplication.shared.delegate?.window!)!
                                  mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
                          mainwindow.makeKeyAndVisible()
        
    }
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(true)
          
          self.tabBarController?.tabBar.isHidden = true
           //self.title = FilterGraphStruct.title
        let nav = self.navigationController?.navigationBar
        nav?.isHidden = false
          nav?.tintColor = UIColor.white
          nav?.barTintColor = UIColor(hexString: baseColor, alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
          
      }
      //MARK: ================== IB ACtions ================
   
    
    @IBAction func btnShareClicked(_ sender: UIBarButtonItem) {
        
        let imageShare =    self.takeScreenshot1(save: false)
        let activityItem: [AnyObject] = [imageShare as AnyObject]
        let activityViewController = UIActivityViewController(activityItems: activityItem as [AnyObject], applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = self.tableView
        activityViewController.popoverPresentationController?.sourceRect = self.tableView.bounds
        
        self.present(activityViewController, animated: true, completion: nil)
        
        activityViewController.completionWithItemsHandler = { activity, success, items, error in
            
            if success == true && error == nil
            {
                self.screenshotSharer()?.dismissSharerViewController(true)
//                let ac = UIAlertController(title: self.confirmation, message: self.RecSend, preferredStyle: .alert)
//                ac.addAction(UIAlertAction(title: self.okay, style: .default))
//                self.present(ac, animated: true)
            }
        }
    }
    @IBAction func btnSaveClicked(_ sender: UIBarButtonItem) {
        let imageShare =   self.takeScreenshot1(save: true)
        
    }
    func takeScreenshot1(save : Bool) -> UIImage {
        let size = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height + 20)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        self.view.drawHierarchy(in: UIScreen.main.bounds , afterScreenUpdates: true)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        if(save == true){
        do{
        try UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        }catch{
            
            }
            
        }
//        UIGraphicsBeginImageContext(view.frame.size)
        
//        let image = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
       // UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        
        return image
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: self.confirmation, message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: self.okay, style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: self.confirmation, message: self.RecSave, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: self.okay, style: .default))
            present(ac, animated: true)
        }
    }
   
    
   
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: ================== Extension ================
extension PaymentRecieptViewController: UITableViewDelegate,UITableViewDataSource {
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        }
        func numberOfSections(in tableView: UITableView) -> Int {
            return 4
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if(section == 0 ){
            return 1
            }else if(section == 1){
                if(section1.count > 0){
                    return section1.count
                }
                return 0
            }else if(section == 2){
                
                return 1
            }else {
                if(BillTransEnd.status == "Y"){
                return 1
                }
                return 0
            }
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if(indexPath.section == 0){
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
           
            return cell
            }else if(indexPath.section == 1){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cellData", for: indexPath) as! MorDetailsTableViewCell
           
                cell.textViewTitle.text = section1[indexPath.row]
                if(cell.textViewTitle.text == "B.P Number" || cell.textViewTitle.text == "उपभोक्ता क्र.(BP No.)"){
                    cell.textViewData.text = BillTransEnd.bpno
                }else if(cell.textViewTitle.text == "Amount Paid" || cell.textViewTitle.text == "भुगतान राशि रु."){
                    cell.textViewData.text = BillTransEnd.amt
                }else if(cell.textViewTitle.text == "Payment Status" || cell.textViewTitle.text == "पेमेंट स्टेटस"){
                    cell.textViewData.text = BillTransEnd.status
                }else if(cell.textViewTitle.text == "Payment Type" || cell.textViewTitle.text == "भुगतान का प्रकार"){
                    cell.textViewData.text = BillTransEnd.paytype
                }else if(cell.textViewTitle.text == "Payment Reference Number" || cell.textViewTitle.text == "पेमेंट रिफरेन्स नंबर"){
                   
                    cell.textViewData.text = BillTransEnd.refno
                }else if(cell.textViewTitle.text == "Payment Reference Number" || cell.textViewTitle.text == "पेमेंट रिफरेन्स नंबर"){
                   
                    cell.textViewData.text = BillTransEnd.refno
                }else if(cell.textViewTitle.text == "Date and Time of Payment" || cell.textViewTitle.text == "भुगतान दिनांक और समय"){
                    let datee = self.dateFormatChange(inputDateStr: BillTransEnd.date, inputFormat: "dd-MM-yyyy HH:mm:ss", outputFromat: "dd MMM yyyy hh:mm a")
                    cell.textViewData.text = datee
                }else if(cell.textViewTitle.text == "Tip" || cell.textViewTitle.text == "टिप"){
                   
                    
                     cell.textViewData.text = BillTransEnd.refno
                  
                }
                return cell
            }else if(indexPath.section == 2){
               let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! UITableViewCell
                
                return cell
            }else {
               let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! UITableViewCell
                
                return cell
            }
           
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if(indexPath.section == 2 || indexPath.section == 3){
                return 100
            }
            return UITableViewAutomaticDimension
            
        }
}
