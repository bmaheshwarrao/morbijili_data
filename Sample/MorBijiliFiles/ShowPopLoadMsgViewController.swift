//
//  ShowPopLoadMsgViewController.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 20/04/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class ShowPopLoadMsgViewController: CommonVSClass {

    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var btnInfo: UIButton!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnOK.setTitle(okay, for: .normal)
        btnInfo.setTitle(notice, for: .normal)
        tableView.delegate = self
              tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnClickedOK(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ShowPopLoadMsgViewController: UITableViewDelegate,UITableViewDataSource {
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        }
   
        func numberOfSections(in tableView: UITableView) -> Int {
            var cnt = 0
            if(ShowMsg.messageEng != ""){
                cnt = 1
            }
            if(ShowMsg.messageHin != ""){
                cnt = cnt + 1
            }
            return cnt
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
            return 1
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
            if(indexPath.section == 0){
                var msg = String()
                if(ShowMsg.messageEng != ""){
                    msg = ShowMsg.messageEng
                }else{
                if(ShowMsg.messageHin != ""){
                   msg = ShowMsg.messageHin
                }
                }
                
                 let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TarifBaseTableViewCell
                    let paragraph1 = NSMutableParagraphStyle()
                                                          paragraph1.alignment = .left
                                                          paragraph1.lineSpacing = 10
                                                   let titleStr = NSMutableAttributedString(string: msg, attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "RobotoSlab-Bold", size: 15.0)])
                                                   titleStr.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraph1, range: NSRange(location: 0, length: titleStr.length))
                                                          cell.textViewBase1.attributedText = titleStr
                                                          
                               return cell
                
            }else{
                var msg = String()
                if(ShowMsg.messageHin != ""){
                   msg = ShowMsg.messageHin
                }
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TarifBaseTableViewCell
                    
                
                
                let paragraph1 = NSMutableParagraphStyle()
                       paragraph1.alignment = .left
                       paragraph1.lineSpacing = 10
                let titleStr = NSMutableAttributedString(string: msg, attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "RobotoSlab-Bold", size: 15.0)])
                titleStr.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraph1, range: NSRange(location: 0, length: titleStr.length))
                       cell.textViewBase1.attributedText = titleStr
                       
                
                
                return cell
            }
                
            
          
        }

        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableViewAutomaticDimension
           

        }
}
