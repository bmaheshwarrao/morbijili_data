//
//  RebateBijiliViewController.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 13/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import DropDown
import Charts
var rebateDetailsDB : [RebateDisplay] = []
class RebateBijiliViewController: CommonVSClass,ChartViewDelegate {
   
    
 //MARK: ================== IBOutlets ================
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: ================== Properties ================
     let dropDown = DropDown()
    var rebateDetailsAPI = RebateModelDataAPI()
     var titleSet = String()
    var ComplaintDetailsAPI = ComplainTypeModelDataAPI()
    var cellData : MainMorPartnerTableViewCell!
    var cellDataRate : MorRateTableViewCell!
    var rateValue = String()
    var combinedChartView : MorGraphRebateTableViewCell!
    var PartnerDB:[Partner] = []
    var partnerBDValue : [String] = []
    var cellPhotoData : PhotoMeterTableViewCell!
    var cellDataText : MorDetailsTableViewCell!
    var cellDropData : MorDropDetailsTableViewCell!
    var cellTextData : MorDetailsTableViewCell!
    let imagePicker = UIImagePickerController()
    var imageData : Data? = nil
    var cnt = 0
    var section1 : [String] = ["Name","Mobile No","Address"]
    
    
    
    //MARK: ================== VC Life Cycle ================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        getPartnerData()
        self.title = titleSet
        
        // Do any additional setup after loading the view.
    }
      //MARK: ================== IB ACtions ================
   
   
    @IBAction func btnPartnerClicked(_ sender: UIButton) {
        self.dropDown.anchorView = cellData.btnPartner // UIView or UIBarButtonItem
        self.dropDown.width = cellData.btnPartner.frame.size.width
        self.dropDown.bottomOffset = CGPoint(x: 0, y:(self.dropDown.anchorView?.plainView.bounds.height)!)
        self.dropDown.direction = .bottom
        // The list of items to display. Can be changed dynamically
        self.dropDown.dataSource = partnerBDValue
        self.dropDown.show()
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected tem: \(item) at index: \(index)")
            rebateDetailsDB = []
            self.tableView.reloadData()
            self.cellData.lblPartner.text = item
            self.getPaymentDetailsData(bpno: item)
           
        }
    }
    
    //MARK: ================== Custom Methods ================
     @objc func setChartData() {
           
           let data = CombinedChartData()
           
      
           data.barData = generateBarData()
           
           let numberFormatter = NumberFormatter()
           numberFormatter.numberStyle = .decimal
           numberFormatter.locale = Locale.current
           let valuesNumberFormatter = ChartValueFormatter(numberFormatter: numberFormatter)
           data.setValueFormatter(valuesNumberFormatter)
           
        let xAxisPadding = 0.9
        combinedChartView.combinedChart.xAxis.axisMinimum = xAxisPadding
        combinedChartView.combinedChart.xAxis.axisMaximum =  data.barData.xMax
           
          
           combinedChartView.combinedChart.data = data
       }
   
    func generateBarData() -> BarChartData {
         var valuesBarChart : [Int] = []
         var valuesBarChart1 : [Int] = []
        if(rebateDetailsDB.count > 0){
            valuesBarChart = [0]
            valuesBarChart1 = [0]
            
            for i in 0...rebateDetailsDB.count - 1 {
                valuesBarChart.append(rebateDetailsDB[(rebateDetailsDB.count - 1) - i].TOT_CHG)
                valuesBarChart1.append(rebateDetailsDB[(rebateDetailsDB.count - 1) - i].TOT_REBATE)
                let datee = self.dateFormatChange(inputDateStr: rebateDetailsDB[(rebateDetailsDB.count - 1) - i].ZBUDAT, inputFormat: "yyyy-MM-dd", outputFromat: "MMM yyyy")
                if(i == 0){
                combinedChartView.lblMon1.text =  datee
                }else if(i == 1){
                combinedChartView.lblMon2.text =  datee
                }else if(i == 2){
                combinedChartView.lblMon3.text =  datee
                }else if(i == 3){
                combinedChartView.lblMon4.text =  datee
                }else if(i == 4){
                combinedChartView.lblMon5.text =  datee
                }else{
                   combinedChartView.lblMon6.text =  datee
                }
            }
         
            valuesBarChart.append(0)
            valuesBarChart1.append(0)
        }
       
        let yVals = (0..<valuesBarChart.count ).map { (i) -> BarChartDataEntry in
            
            let val = Double(valuesBarChart[i])
            
            return BarChartDataEntry(x: Double(i), y: val)
        }
       
        let yVals1 = (0..<valuesBarChart1.count ).map { (i) -> BarChartDataEntry in
                   
                   let val = Double(valuesBarChart1[i])
                   
                   return BarChartDataEntry(x: Double(i), y: val)
               }
        
        var set1: BarChartDataSet! = nil
        
        set1 = BarChartDataSet(values: yVals, label: "")
        set1.colors = [UIColor(hexString: "2f8fe9", alpha: 1.0)] as! [NSUIColor]
        combinedChartView.view1.backgroundColor = UIColor(hexString: "2f8fe9", alpha: 1.0)
        set1.drawValuesEnabled = true
       // set1.roundedCorners = [.topRight,.topLeft]
        
        
        var set2: BarChartDataSet! = nil
         set2 = BarChartDataSet(values: yVals1, label: "")
       // set2.roundedCorners = [.topRight,.topLeft]
        set2.colors = [UIColor.green
               ]
        combinedChartView.view2.backgroundColor = UIColor.green
        set2.drawValuesEnabled = true
               set2.axisDependency = .left
               
        let groupSpace = 0.16
        let barSpace = 0.02 // x2 dataset
        let barWidth = 0.40 // x2 dataset
               // (0.45 + 0.02) * 2 + 0.06 = 1.00 -> interval per "group"
               
               let data = BarChartData(dataSets: [set1, set2])
               data.barWidth = barWidth
               
               // make this BarData object grouped
               data.groupBars(fromX: 0, groupSpace: groupSpace, barSpace: barSpace)
        
        
       
     
        
        return data
    }
    @objc func getPartnerData() {
    
            self.PartnerDB = [Partner]()
            do {
    
                self.PartnerDB = try context.fetch(Partner.fetchRequest())
                //self.refresh.endRefreshing()
                if(self.PartnerDB.count > 0) {
                    for i in 0...self.PartnerDB.count - 1 {
                        partnerBDValue.append(self.PartnerDB[i].partner!)
                    }
                   
                }else{
                   
               }
                self.tableView.reloadData()
    
            } catch {
                print("Fetching Failed")
            }
        }
       
    @objc func getPaymentDetailsData(bpno : String){
        
     
        
        rebateDetailsAPI.serviceCalling(obj: self , bpNo : bpno) { (dict) in
            
            if(dict.count > 0){
                rebateDetailsDB = dict as! [RebateDisplay]
                self.tableView.reloadData()
            }
            
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: ================== Extension ================
extension RebateBijiliViewController: UITableViewDelegate,UITableViewDataSource {
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        }
        func numberOfSections(in tableView: UITableView) -> Int {
            return 3
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            return 1
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if(indexPath.section == 0){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MainMorPartnerTableViewCell
            cellData = cell
            if(PartnerDB.count > 0){
                
                if(cnt == 0){
                    cell.lblPartner.text = partnerBDValue[indexPath.row]
                getPaymentDetailsData(bpno: partnerBDValue[0])
                }
         cnt = 1
            }else{
               cell.lblPartner.text = "---"
            }
            return cell
            }else if(indexPath.section == 1){
                 let cell = tableView.dequeueReusableCell(withIdentifier: "cellImage", for: indexPath) as! UITableViewCell
               
               return cell
               }else {
                if(rebateDetailsDB.count == 0){
                           let cell = tableView.dequeueReusableCell(withIdentifier: "cellNoData", for: indexPath) as! NoDataTableViewCell
                          
                           return cell
                }else{
                
                
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "graph", for: indexPath) as! MorGraphRebateTableViewCell
                    combinedChartView = cell
                    let formato:RebateBarFormatter = RebateBarFormatter()
                    cell.combinedChart.delegate = self
                    cell.combinedChart.drawValueAboveBarEnabled = true
                    cell.combinedChart.chartDescription?.enabled = false
                    cell.combinedChart.drawGridBackgroundEnabled = false
                    cell.combinedChart.drawBarShadowEnabled = false
                    cell.combinedChart.highlightFullBarEnabled = false
                    //cell.combinedChart.isUserInteractionEnabled = false
                    
                    cell.combinedChart.drawOrder = [DrawOrder.bar.rawValue,
                                                    
                                                    DrawOrder.line.rawValue,
                    ]
                    
                    let l = cell.combinedChart.legend
                    l.wordWrapEnabled = true
                    l.horizontalAlignment = .center
                    l.verticalAlignment = .bottom
                    l.orientation = .horizontal
                    l.drawInside = false
                    l.enabled = false
                    //        cell.combinedChart.legend = l
                    
                    let rightAxis = cell.combinedChart.rightAxis
                    rightAxis.axisMinimum = 0
                    rightAxis.drawLabelsEnabled = false
                rightAxis.labelFont = UIFont(name: "RobotoSlab-Regular", size: 9)!
                    //        rightAxis.drawBottomYLabelEntryEnabled = true
                    rightAxis.drawGridLinesEnabled = false
                    rightAxis.drawAxisLineEnabled = false
                    let leftAxis = cell.combinedChart.leftAxis
                    leftAxis.axisMinimum = 0
                    var lineChart : [Int] = []
                
                        var limitVal = Int()
                        if(rebateDetailsDB.count > 0){

                            for i in 0...rebateDetailsDB.count - 1 {
                               
                                lineChart.append(rebateDetailsDB[i].TOT_CHG)

                            }

                            limitVal = Int(lineChart.reduce(lineChart[0]) { $0 > $1 ? $0 : $1 })

                        }
                    
                    let ll1 = ChartLimitLine(limit: Double(0), label: "")
                    ll1.lineWidth = 0
                    ll1.valueFont =  UIFont(name: "RobotoSlab-Regular", size: 11)!
                    ll1.lineColor = UIColor.clear
                    ll1.lineDashLengths = [0, 0]
                   // ll1.labelPosition = .rightTop
                    ll1.valueFont = UIFont(name: "RobotoSlab-Bold", size: 9)!
                   // ll1.valueTextColor = UIColor(hexString: "4a8e21", alpha: 1.0)!
                    leftAxis.removeAllLimitLines()
                    leftAxis.addLimitLine(ll1)
                    leftAxis.axisMinimum = 0
                    let extramax = IntegerData(strInteger: limitVal)
                    leftAxis.axisMaximum = Double(limitVal + extramax )
                    leftAxis.drawLabelsEnabled = true
                    leftAxis.drawGridLinesEnabled = false
                    let xAxis = cell.combinedChart.xAxis
                    xAxis.drawLabelsEnabled = false
                    xAxis.drawGridLinesEnabled = false
                    xAxis.labelPosition = .bottom
                    xAxis.axisMinimum = 0
                    xAxis.granularity = 1
                    xAxis.valueFormatter = formato
                    xAxis.labelFont =  UIFont(name: "RobotoSlab-Regular", size: 8)!
                    self.setChartData()
                    let marker = BalloonMarker(color: UIColor(white: 180/255, alpha: 1),
                                               font: UIFont(name: "RobotoSlab-Bold", size: 11)!,
                                               textColor: .white,
                                               insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8))
                    marker.chartView  = cell.combinedChart
                    marker.minimumSize = CGSize(width: 80, height: 40)
                    cell.combinedChart.marker = marker
                cell.separatorInset = UIEdgeInsets(top: 0, left: 10000, bottom: 0, right: 0);
                    cell.combinedChart.doubleTapToZoomEnabled = false
                    cell.combinedChart.pinchZoomEnabled = false
                     cell.combinedChart.dragEnabled = false
                cell.combinedChart.dragXEnabled = false
                cell.combinedChart.dragYEnabled = false
                    return cell
            
            }
            }
            
             
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            if(section == 2 ){
               return 20.0
           }
            return 0.0
         }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
              
           
              let header = tableView.dequeueReusableCell(withIdentifier: "header") as! UITableViewCell
            return header
              
          }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
              if(indexPath.section == 1){
                return 150
            }else if(indexPath.section == 2){
                return 290
            }else if(indexPath.section == 0){
                return 220
            }
            return UITableViewAutomaticDimension
            
        }
}

extension RebateBijiliViewController: IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        if(rebateDetailsDB.count > 0){
        return rebateDetailsDB[Int(value)].ZBUDAT
        }
        return ""


    }
}

