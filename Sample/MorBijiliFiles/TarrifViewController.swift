//
//  TarrifViewController.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 20/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class TarrifViewController: UIViewController {

var titleSet = String()
    @IBOutlet weak var tableView: UITableView!
   var section1 = ["यूनिट स्लैब","ऊर्जा प्रभार (₹ / यूनिट)","नियत प्रभार (₹ / यूनिट)","0-100","1.00","2.40","101-200","1.10","2.50","201-400","1.70","3.20","401-600","2.00","3.50","600 और इससे भी ज्यादा","2.45","4.85","न्यूनतम मासिक प्रभार :- एक फेज - ₹ 40/- , तीन फेज - ₹ 120/-","ड्यूटी @ 8 % (ऊर्जा प्रभार + व्ही.सी.ए)","सेस @ 10 पैसे / यूनिट"]
    var setSection : [[String]] = [[]]
    var setSectionLV2: [[String]] = []
    var setSectionLV3: [[String]] = []
    var setSectionLV4: [[String]] = []
    var setSectionLV5: [[String]] = []
    var setSectionLV6: [[String]] = []
    var setSectionLV7: [[String]] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setSection = []
        setSection.append(["यूनिट स्लैब","ऊर्जा प्रभार (₹ / यूनिट)","नियत प्रभार (₹ / यूनिट)"])
        setSection.append(["0-100","1.00","2.40"])
        setSection.append(["101-200","1.10","2.50"])
        setSection.append(["201-400","1.70","3.20"])
        setSection.append(["401-600","2.00","3.50"])
         setSection.append(["600 और इससे भी ज्यादा","2.45","4.85"])
        setSection.append(["न्यूनतम मासिक प्रभार :- एक फेज - ₹ 40/- , तीन फेज - ₹ 120/-"])
        setSection.append(["ड्यूटी @ 8 % (ऊर्जा प्रभार + व्ही.सी.ए)"])
        setSection.append(["सेस @ 10 पैसे / यूनिट"])
        setSectionLV2.append(["LV-2.1 सिंगल फेज"])
        setSectionLV2.append(["यूनिट स्लैब","ऊर्जा प्रभार (₹ / यूनिट)","नियत प्रभार (₹ / यूनिट)"])
        setSectionLV2.append(["0-100","5.40","50"])
        setSectionLV2.append(["101-400","6.50","50"])
        setSectionLV2.append(["401 and above all units","7.90","50"])
        setSectionLV2.append(["LV-2.2(A) तीन फेज 15 किलो वाट तक"])
        setSectionLV2.append(["0-400","6.50","120"])
        setSectionLV2.append(["401 and above all units","7.80","120"])
        setSectionLV2.append(["LV-2.2(A) तीन फेज 15 किलो वाट से ज्यादा "])
        setSectionLV2.append(["All units","7.25","180"])
        setSectionLV2.append(["ड्यूटी @ 12 % (ऊर्जा प्रभार + व्ही.सी.ए)"])
        setSectionLV2.append(["सेस @ 10 पैसे / यूनिट"])
        setSectionLV3.append(["उपभोक्ता की श्रेणी","ऊर्जा प्रभार (₹ / यूनिट)","नियत प्रभार (₹ / अश्व शक्ति(HP))"])
        setSectionLV3.append(["कृषि(Agriculture)","4.40","80"])
        setSectionLV3.append(["ड्यूटी @ 12 % (ऊर्जा प्रभार + व्ही.सी.ए)"])
        setSectionLV3.append(["सेस @ 10 पैसे / यूनिट"])
        
        setSectionLV4.append(["उपभोक्ता की श्रेणी","ऊर्जा प्रभार (₹ / यूनिट)","नियत प्रभार"])
        setSectionLV4.append(["LV-4.1(A) : 25 HP तक","4.40","80 ₹/HP"])
        setSectionLV4.append(["LV-4.2(B) : 25 HP से ज्यादा 150 HP तक","5.20","90 ₹/HP"])
        setSectionLV4.append(["LV-4.2 : मांग आधारित (Demand Based) 15 किलो वाट से 112.5 किलो वाट तक","5.10","180 ₹/किलो वाट"])
        setSectionLV4.append(["ड्यूटी @ 6 % (ऊर्जा प्रभार + व्ही.सी.ए)"])
        setSectionLV4.append(["सेस @ 10 पैसे / यूनिट"])
        
        
        setSectionLV5.append(["LV-5.1 उधोग (Industry)"])
        setSectionLV5.append(["उपभोक्ता की श्रेणी","ऊर्जा प्रभार (₹ / यूनिट)","मांग प्रभार (₹/किलो वाट)"])
        setSectionLV5.append(["LV-5.1 : आटा चक्की,Hullers,बिजली करघे आदि,25 HP तक","3.60","65"])
        setSectionLV5.append(["LV-5.1(a) : बस्तर सरगुजा क्षेत्र","3.20","65"])
        setSectionLV5.append(["LV-5.2 अन्य उधोग (Other Industries)"])
        
        setSectionLV5.append(["LV-5.2.1 : 25 HP तक","4.75","100"])
        setSectionLV5.append(["LV-5.2.1(a) : बस्तर सरगुजा क्षेत्र","3.75","80"])
        setSectionLV5.append(["LV-5.2.2 : 25 HP से ज्यादा 150 HP तक","5.50","110"])
        setSectionLV5.append(["LV-5.2.2(a) : बस्तर सरगुजा क्षेत्र","5.00","90"])
        setSectionLV5.append(["ड्यूटी % (ऊर्जा प्रभार + व्ही.सी.ए) \n@ 3%(25 HP तक,पॉवरलूम,फ्लोर मिल)\n@ 4%(25 HP - 75 HP)\n@ 5%(75 HP - 100 HP)\n@ 6%(100 HP - 150 HP)\n@ 10%(150 HP से ज्यादा ,स्टोन क्रशर,टेक्सटाइल मिल)\n15%(सीमेंट)\n40%(माइनिंग)"])
        setSectionLV5.append(["सेस @ 10 पैसे / यूनिट"])
        
        
        setSectionLV6.append(["उपभोक्ता की श्रेणी","ऊर्जा प्रभार (₹ / यूनिट)","नियत प्रभार"])
        setSectionLV6.append(["LV-6 सार्वजानिक सुविधाएं","5.65","125 ₹/HP"])
        setSectionLV6.append(["ड्यूटी @ 0 % (ऊर्जा प्रभार + व्ही.सी.ए)"])
        setSectionLV6.append(["सेस @ 10 पैसे / यूनिट"])
        
        
        setSectionLV7.append(["उपभोक्ता की श्रेणी","ऊर्जा प्रभार (₹ / यूनिट)","नियत प्रभार"])
        setSectionLV7.append(["सुचना प्रौद्योगीकी उधोग","4.50","---"])
        setSectionLV7.append(["न्यूनतम मासिक प्रभार 1500 ₹/-"])
        setSectionLV7.append(["सेस @ 10 पैसे / यूनिट"])
        
        print(setSection)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.title = titleSet
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
var ii = 0
}
//MARK: ================== Extension ================
extension TarrifViewController: UITableViewDelegate,UITableViewDataSource {
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
            return 30.0
         }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
              
           
              let header = tableView.dequeueReusableCell(withIdentifier: "header") as! TarifBaseTableViewCell
        if(section == 0){
         header.textViewBase1.text = ""
        }else if(section == 1){
         header.textViewBase1.text = "LV-1 घरेलु कनेक्शन (DLF)"
        }else if(section == 2){
          header.textViewBase1.text = "LV-2 गैर घरेलु (NDLF)"
        }else if(section == 3){
          header.textViewBase1.text = "LV-3 कृषि(Agriculture)"
        }else if(section == 4){
          header.textViewBase1.text = "LV-4 कृषि संभंद गतिविधि (Agri Allied activities)"
        }else if(section == 5){
          header.textViewBase1.text = "LV-5 उधोग (Industry)"
        }else if(section == 6){
          header.textViewBase1.text = "LV-6 सार्वजानिक सुविधाएं (Public Utilities)"
        }else if(section == 7){
          header.textViewBase1.text = "LV-7 सुचना प्रौद्योगीकी उधोग (IT Industries)"
        }
        header.textViewBase1.font = UIFont(name: "RobotoSlab-Bold", size: 15.0)
        header.textViewBase1.textColor = UIColor(hexString: baseColor, alpha: 1.0)
        header.textViewBase1.textAlignment = .left
            return header
              
          }
        func numberOfSections(in tableView: UITableView) -> Int {
            return 8
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if(section == 0 ){
                return 1
            }else if(section == 1 ){
                return 11
            }else if section == 2{
                return setSectionLV2.count + 2
            }else if section == 3{
                return setSectionLV3.count + 2
            }else if section == 4{
                return setSectionLV4.count + 2
            }else if section == 5{
                return setSectionLV5.count + 2
            }else if section == 6{
                return setSectionLV6.count + 2
            }else if section == 7{
                return setSectionLV7.count + 2
            }
            return 0
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           if(indexPath.section == 0){
                
                        if(UserDefaults.standard.value(forKey: "Language") as! String == "en"){
                          let cell = tableView.dequeueReusableCell(withIdentifier: "cells", for: indexPath) as! UITableViewCell
                            return cell
                        }else{
                          let cell = tableView.dequeueReusableCell(withIdentifier: "cells1", for: indexPath) as! UITableViewCell
                           return cell
                        }
              
                
            }else if(indexPath.section == 1){
                
                
                if(indexPath.row == 0 || indexPath.row == 10 ){
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! TarifLineTableViewCell
                    cell.viewColor.backgroundColor = UIColor(hexString: "2f71e9", alpha: 1.0)
                return cell
                }else if(indexPath.row < 7){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TarrifTableViewCell
                    if(indexPath.row == 1){
                        cell.textViewPlace1.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                        cell.textViewPlace2.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                        cell.textViewPlace3.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                    }else{
                        cell.textViewPlace1.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                        cell.textViewPlace2.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                        cell.textViewPlace3.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                    }
                cell.textViewPlace1.text = setSection[indexPath.row - 1][0]
                cell.textViewPlace2.text = setSection[indexPath.row - 1][1]
                cell.textViewPlace3.text = setSection[indexPath.row - 1][2]
              
                return cell
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TarifBaseTableViewCell
                     cell.textViewBase1.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                     cell.textViewBase1.text = setSection[indexPath.row - 1][0]
                    cell.textViewBase1.textAlignment = .center
                return cell
            }
                
            
            }else if(indexPath.section == 2){
                if(indexPath.row == 0 || indexPath.row == 13 ){
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! TarifLineTableViewCell
                    cell.viewColor.backgroundColor = UIColor(hexString: "2f71e9", alpha: 1.0)
                return cell
                }else if(indexPath.row == 1 || indexPath.row == 6 || indexPath.row == 9){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TarifBaseTableViewCell
              cell.textViewBase1.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
              cell.textViewBase1.text = setSectionLV2[indexPath.row - 1][0]
              cell.textViewBase1.textAlignment = .center
              cell.textViewBase1.textColor = UIColor(hexString: baseColor, alpha: 1.0)
              return cell
                
            }else if(indexPath.row == 12 || indexPath.row == 11 ){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TarifBaseTableViewCell
              cell.textViewBase1.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
              cell.textViewBase1.text = setSectionLV2[indexPath.row - 1][0]
              cell.textViewBase1.textAlignment = .center
                    cell.textViewBase1.textColor = UIColor.black
              return cell
                
            }else{
                 
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TarrifTableViewCell
                      if(indexPath.row == 0){
                          cell.textViewPlace1.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                          cell.textViewPlace2.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                          cell.textViewPlace3.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                      }else{
                          cell.textViewPlace1.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                          cell.textViewPlace2.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                          cell.textViewPlace3.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                      }
                  cell.textViewPlace1.text = setSectionLV2[indexPath.row - 1][0]
                  cell.textViewPlace2.text = setSectionLV2[indexPath.row - 1][1]
                  cell.textViewPlace3.text = setSectionLV2[indexPath.row - 1][2]
                
                  return cell
            }
                
            
            }else if(indexPath.section == 3){
                if(indexPath.row == 0 || indexPath.row == 5 ){
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! TarifLineTableViewCell
                    cell.viewColor.backgroundColor = UIColor(hexString: "2f71e9", alpha: 1.0)
                return cell
                }else if(indexPath.row == 4 || indexPath.row == 3 ){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TarifBaseTableViewCell
              cell.textViewBase1.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
              cell.textViewBase1.text = setSectionLV3[indexPath.row - 1][0]
              cell.textViewBase1.textAlignment = .center
                    cell.textViewBase1.textColor = UIColor.black
              return cell
                
            }else{
                 
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TarrifTableViewCell
                      if(indexPath.row == 1){
                          cell.textViewPlace1.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                          cell.textViewPlace2.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                          cell.textViewPlace3.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                      }else{
                          cell.textViewPlace1.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                          cell.textViewPlace2.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                          cell.textViewPlace3.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                      }
                  cell.textViewPlace1.text = setSectionLV3[indexPath.row - 1][0]
                  cell.textViewPlace2.text = setSectionLV3[indexPath.row - 1][1]
                  cell.textViewPlace3.text = setSectionLV3[indexPath.row - 1][2]
                
                  return cell
            }
                
            
            }else if(indexPath.section == 4){
               
                 if(indexPath.row == 0 || indexPath.row == 7 ){
                 let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! TarifLineTableViewCell
                     cell.viewColor.backgroundColor = UIColor(hexString: "2f71e9", alpha: 1.0)
                 return cell
                 }else if(indexPath.row == 6 || indexPath.row == 5 ){
                   let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TarifBaseTableViewCell
                  cell.textViewBase1.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                  cell.textViewBase1.text = setSectionLV4[indexPath.row - 1][0]
                  cell.textViewBase1.textAlignment = .center
                  cell.textViewBase1.textColor = UIColor.black
                  return cell
                                
               }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TarrifTableViewCell
                      if(indexPath.row == 0){
                          cell.textViewPlace1.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                          cell.textViewPlace2.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                          cell.textViewPlace3.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                      }else{
                          cell.textViewPlace1.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                          cell.textViewPlace2.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                          cell.textViewPlace3.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                      }
                  cell.textViewPlace1.text = setSectionLV4[indexPath.row - 1][0]
                  cell.textViewPlace2.text = setSectionLV4[indexPath.row - 1][1]
                  cell.textViewPlace3.text = setSectionLV4[indexPath.row - 1][2]
                
                  return cell
                }
            
                
            
            }else if(indexPath.section == 5){
                if(indexPath.row == 0 || indexPath.row == 12 ){
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! TarifLineTableViewCell
                    cell.viewColor.backgroundColor = UIColor(hexString: "2f71e9", alpha: 1.0)
                return cell
                }else if(indexPath.row == 1 || indexPath.row == 5 ){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TarifBaseTableViewCell
              cell.textViewBase1.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
              cell.textViewBase1.text = setSectionLV5[indexPath.row - 1][0]
              cell.textViewBase1.textAlignment = .center
              cell.textViewBase1.textColor = UIColor(hexString: baseColor, alpha: 1.0)
              return cell
                
            }else if(indexPath.row == 10 || indexPath.row == 11 ){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TarifBaseTableViewCell
              cell.textViewBase1.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
              cell.textViewBase1.text = setSectionLV5[indexPath.row - 1][0]
                    if(indexPath.row == 11){
                      cell.textViewBase1.textAlignment = .center
                    }else{
                       cell.textViewBase1.textAlignment = .left
                    }
                    cell.textViewBase1.textColor = UIColor.black
              return cell
                
            }else{
                 
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TarrifTableViewCell
                      if(indexPath.row == 0){
                          cell.textViewPlace1.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                          cell.textViewPlace2.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                          cell.textViewPlace3.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                      }else{
                          cell.textViewPlace1.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                          cell.textViewPlace2.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                          cell.textViewPlace3.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                      }
                  cell.textViewPlace1.text = setSectionLV5[indexPath.row - 1][0]
                  cell.textViewPlace2.text = setSectionLV5[indexPath.row - 1][1]
                  cell.textViewPlace3.text = setSectionLV5[indexPath.row - 1][2]
                
                  return cell
            }
                
            
            }else if(indexPath.section == 6){
                if(indexPath.row == 0 || indexPath.row == 5 ){
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! TarifLineTableViewCell
                    cell.viewColor.backgroundColor = UIColor(hexString: "2f71e9", alpha: 1.0)
                return cell
                }else if(indexPath.row == 4 || indexPath.row == 3 ){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TarifBaseTableViewCell
              cell.textViewBase1.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
              cell.textViewBase1.text = setSectionLV6[indexPath.row - 1][0]
              cell.textViewBase1.textAlignment = .center
                    cell.textViewBase1.textColor = UIColor.black
              return cell
                
               }else{
                 
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TarrifTableViewCell
                      if(indexPath.row == 1){
                          cell.textViewPlace1.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                          cell.textViewPlace2.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                          cell.textViewPlace3.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                      }else{
                          cell.textViewPlace1.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                          cell.textViewPlace2.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                          cell.textViewPlace3.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                      }
                  cell.textViewPlace1.text = setSectionLV6[indexPath.row - 1][0]
                  cell.textViewPlace2.text = setSectionLV6[indexPath.row - 1][1]
                  cell.textViewPlace3.text = setSectionLV6[indexPath.row - 1][2]
                
                  return cell
            }
                
            
            }else if(indexPath.section == 7){
                if(indexPath.row == 0 || indexPath.row == 5 ){
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! TarifLineTableViewCell
                    cell.viewColor.backgroundColor = UIColor(hexString: "2f71e9", alpha: 1.0)
                return cell
                }else if(indexPath.row == 4 || indexPath.row == 3 ){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TarifBaseTableViewCell
              cell.textViewBase1.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
              cell.textViewBase1.text = setSectionLV7[indexPath.row - 1][0]
              cell.textViewBase1.textAlignment = .center
                    cell.textViewBase1.textColor = UIColor.black
              return cell
                
            }else{
                 
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TarrifTableViewCell
                      if(indexPath.row == 1){
                          cell.textViewPlace1.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                          cell.textViewPlace2.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                          cell.textViewPlace3.font = UIFont(name: "RobotoSlab-Bold", size: 13.0)
                      }else{
                          cell.textViewPlace1.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                          cell.textViewPlace2.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                          cell.textViewPlace3.font = UIFont(name: "RobotoSlab-Regular", size: 13.0)
                      }
                  cell.textViewPlace1.text = setSectionLV7[indexPath.row - 1][0]
                  cell.textViewPlace2.text = setSectionLV7[indexPath.row - 1][1]
                  cell.textViewPlace3.text = setSectionLV7[indexPath.row - 1][2]
                
                  return cell
            }
                
            
            }else{
               let cell = tableView.dequeueReusableCell(withIdentifier: "cellButton", for: indexPath) as! UITableViewCell

                return cell
            }

        }

        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableViewAutomaticDimension
            if(indexPath.section == 1){
            if(indexPath.row < 6){
            return 55
            }else{
                return UITableViewAutomaticDimension
            }
            }else if(indexPath.section == 0){
                return 143
            }else{
                return 60
            }

        }
}
