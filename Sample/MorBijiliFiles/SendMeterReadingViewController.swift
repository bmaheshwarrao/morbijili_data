//
//  SendMeterReadingViewController.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 12/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import AlamofireSoap
import SwiftyXMLParser
 var secID = "ma*2543"
class SendMeterReadingViewController:CommonVSClass {
   
    
 //MARK: ================== IBOutlets ================
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: ================== Properties ================
     let dropDown = DropDown()
     var titleSet = String()
    var BillDetailsAPI = SendMeterModelDataAPI()
    var consumptionDB : [ConsumptionDisplay] = []
       var electricBillDetailsDB : [ElectricBillDetailsDisplay] = []
       var electricMainDetailsDB : [ElectricMainDetailsDisplay] = []
       var electricUnitsDetailsDB : [ElectricBillUnitDetailsDisplay] = []
       var electricBillBPDetailsDB : [ElectricBillBPDataDetailsDisplay] = []
       var electricBillLNDetailsDB : [ElectricBillLNDetailsDisplay] = []
       var electricOfficeDetailsDB : [ElectricOfficeDetailsDisplay] = []
    var ComplaintDetailsAPI = ComplainTypeModelDataAPI()
    var cellData : MainMorPartnerTableViewCell!
    var billDetailsDB : [BillDisplay] = []
    var PartnerDB:[Partner] = []
    var partnerBDValue : [String] = []
    var cellPhotoData : PhotoMeterTableViewCell!
    var cellDataText : MorDetailsTableViewCell!
    var cellDropData : MorDropDetailsTableViewCell!
    var cellTextData : MorDetailsTableViewCell!
    let imagePicker = UIImagePickerController()
    var imageData : Data? = nil
    var cnt = 0
    var setImage = 0
    var section1 : [String] = ["Name","Mobile No","Address","Bill Month","Last Reading"]
    var meterReadingType : [String] = ["गलत रीडिंग होना","समय पर रीडिंग न होना ","मीटर रीडिंग के समय घर बंद होना "]
    
    
    //MARK: ================== VC Life Cycle ================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        getPartnerData()
        self.title = titleSet
        if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
         section1  = ["Name","Address","Mobile No","Bill Month","Last Reading","Reason for Sending Meter Reading","Current Reading"]
         }else{
         section1  = ["नाम","पता","मोबाइल नंबर","बिल माह","पूर्व वाचन","मीटर रीडिंग भेजने का कारण","वर्तमान वाचन"]
          }
        // Do any additional setup after loading the view.
    }
      //MARK: ================== IB ACtions ================
    
    @IBAction func btnImageUploadClicked(_ sender: UIButton) {
        
        self.openCamera()
        
        
    }
    var cntdata = 0
     @IBAction func btnCloseClicked(_ sender: UIButton) {
        self.setImage = 0
        self.imageData = nil
        cellPhotoData.imgPhoto.image = UIImage(named: "click")
        cellPhotoData.btnClose.isHidden = true
       }
    @IBAction func btnSubmitMeterClicked(_ sender: UIButton) {
        if(cellTextData.textViewData.text == ""){
            self.view.makeToastHZL("Please Enter Meter Reading")
        }else if(self.setImage == 0){
            self.view.makeToastHZL("Please Click Picture of Meter Reading")
        }else{
            self.startLoadingPK(view: self.view, text: "saving....")
          //  let byteArray  = getArrayOfBytesFromImage(imageData: self.imageData! as NSData)
            
            let dateFormatterInv = DateFormatter()
            dateFormatterInv.timeZone = NSTimeZone.system
            dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
            let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
            let invStrUnique = dateFormatterInv.string(from: Date())
            let request_no = invStrUnique + mobile
            
            let imgData = self.imageData!.base64EncodedString(options: .lineLength64Characters)
            print(imgData)
            let message = "MA_mtrrdg|\(cellData.lblPartner.text!)|\(mobile)|\(cellTextData.textViewData.text!)|\(electricOfficeDetailsDB[0].COKEY)|\(imgData)|IMA|\(cellDropData.lblType.text!)|\(secID)|\(request_no)"
            let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
            print(message + " ------ " + encode!)
           AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_string", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
            self.stopLoadingPK(view: self.view)
           guard let respo = response.value else {
               return
           }
            print(response.value)
           let xml = try! XML.parse(respo)
           let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_stringResponse"]["MA_cons_stringResult"].text
           if let result = accessor {
               let resultInt = Int(result)
               if(resultInt! > 0){
                  
                   self.showSingleButtonAlertWithAction(title: "Meter Reading Sent Successfully", buttonTitle: "OK") {
                       self.navigationController?.popViewController(animated: true)
                   }
                   
                   
                  
               }else{
                   self.showSingleButtonWithMessage(title: "Confirmation", message: "Not able to Send Meter Reading", buttonName: "OK")
               }
           }
           }
        }
        
        
        
        
        
    }
    
   func getArrayOfBytesFromImage(imageData:NSData) -> NSMutableArray
   {
   // the number of elements:
   let count = imageData.length / MemoryLayout<UInt8>.size
   // create array of appropriate length:
   var bytes = [UInt8](repeating: 0, count: count)
   // copy bytes into array
   imageData.getBytes(&bytes, length:count)
    let byteArray:NSMutableArray = NSMutableArray()
   
    for i in 0...count - 1{
        byteArray.add(NSNumber(value: bytes[i]))
    }
   return byteArray
   }
    @IBAction func btnTypeClicked(_ sender: UIButton) {
        
        self.dropDown.anchorView = cellDropData.btnType // UIView or UIBarButtonItem
              self.dropDown.width = cellDropData.btnType.frame.size.width
              self.dropDown.bottomOffset = CGPoint(x: 0, y:(self.dropDown.anchorView?.plainView.bounds.height)!)
              self.dropDown.direction = .bottom
              // The list of items to display. Can be changed dynamically
              self.dropDown.dataSource = meterReadingType
              self.dropDown.show()
              self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                  print("Selected tem: \(item) at index: \(index)")
                  self.cellDropData.lblType.text = item
                
                  
              }
    }
    
    @IBAction func btnPartnerClicked(_ sender: UIButton) {
        self.dropDown.anchorView = cellData.btnPartner // UIView or UIBarButtonItem
        self.dropDown.width = cellData.btnPartner.frame.size.width
        self.dropDown.bottomOffset = CGPoint(x: 0, y:(self.dropDown.anchorView?.plainView.bounds.height)!)
        self.dropDown.direction = .bottom
        // The list of items to display. Can be changed dynamically
        self.dropDown.dataSource = partnerBDValue
        self.dropDown.show()
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected tem: \(item) at index: \(index)")
            self.cellData.lblPartner.text = item
            self.cntdata = 0
            self.getPaymentBillDetailsData(bpno: item)
           
        }
    }
    
    //MARK: ================== Custom Methods ================
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    @objc func getPartnerData() {
    
            self.PartnerDB = [Partner]()
            do {
    
                self.PartnerDB = try context.fetch(Partner.fetchRequest())
                //self.refresh.endRefreshing()
                if(self.PartnerDB.count > 0) {
                    for i in 0...self.PartnerDB.count - 1 {
                        partnerBDValue.append(self.PartnerDB[i].partner!)
                    }
                   
                }else{
                   
               }
                self.tableView.reloadData()
    
            } catch {
                print("Fetching Failed")
            }
        }
      
    
    @objc func getPaymentBillDetailsData(bpno : String){
        
     
        
        BillDetailsAPI.serviceCalling(obj: self , bpNo : bpno) { (dict) in
            
            if(dict.count > 0){
                           self.electricMainDetailsDB = dict as! [ElectricMainDetailsDisplay]
                           self.tableView.reloadData()
            }
            
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: ================== Extension ================
extension SendMeterReadingViewController: UITableViewDelegate,UITableViewDataSource {
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        }
        func numberOfSections(in tableView: UITableView) -> Int {
            return 7
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if(section == 0 ){
            return 1
            }else if(section == 1){
                if(electricBillDetailsDB.count > 0){
                return 5
                }
                return 0
            }else if(section == 6){
                if(electricBillDetailsDB.count > 0){
                return 1
                }
                return 0
            }else {
                if(electricBillDetailsDB.count > 0){
                return 1
                }
                return 0
            }
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if(indexPath.section == 0){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MainMorPartnerTableViewCell
            cellData = cell
            if(PartnerDB.count > 0){
                
                if(cnt == 0){
                    cell.lblPartner.text = partnerBDValue[indexPath.row]
                getPaymentBillDetailsData(bpno: partnerBDValue[0])
                }
         
            }else{
               cell.lblPartner.text = "---"
            }
            return cell
            }else if(indexPath.section == 1){
              let cell = tableView.dequeueReusableCell(withIdentifier: "cellData", for: indexPath) as! MorDetailsTableViewCell
              
                cell.textViewTitle.text = section1[indexPath.row]
                if(electricBillDetailsDB.count > 0){
               
                    if(cell.textViewTitle.text == "Name" || cell.textViewTitle.text == "नाम"){
                                           cell.textViewData.text = electricMainDetailsDB[0].BP_NAME
                                       }else if(cell.textViewTitle.text == "Mobile No" || cell.textViewTitle.text == "मोबाइल नंबर"){
                                           cell.textViewData.text = electricMainDetailsDB[0].TEL_NUMBER
                                       }else if(cell.textViewTitle.text == "Bill Number" || cell.textViewTitle.text == "बिल नंबर"){
                                           cell.textViewData.text = billDetailsDB[0].billno
                                       }else if(cell.textViewTitle.text == "Address" || cell.textViewTitle.text == "पता"){
                                           cell.textViewData.text = electricMainDetailsDB[0].ADD1 + "," + electricMainDetailsDB[0].ADD2 + "," + electricMainDetailsDB[0].ADD3
                                       }else if(cell.textViewTitle.text == "Bill Month" || cell.textViewTitle.text == "बिल माह"){
                                           let datee = self.dateFormatChange(inputDateStr: electricBillDetailsDB[0].BILL_MNTH, inputFormat: "yyyy/MM", outputFromat: "MMM yyyy")
                                           cell.textViewData.text = datee
                                       }else if(cell.textViewTitle.text == "Last Reading" || cell.textViewTitle.text == "पूर्व वाचन"){
                                         
                                           cell.textViewData.text = electricUnitsDetailsDB[0].PR_RD.trimmingCharacters(in: .whitespaces)
                                       }
                    
                    
                    
                    
                    
                }
                return cell
            }else if(indexPath.section == 2){
                   let cellDrop = tableView.dequeueReusableCell(withIdentifier: "cellDataTextRead", for: indexPath) as! MorDropDetailsTableViewCell
                    cellDrop.textViewTitle.text = section1[5]
                   cellDropData = cellDrop
                    if(cnt == 0){
                     cellDrop.lblType.text = meterReadingType[0]
                    }
                    cnt = 1
                   return cellDrop
                    
                    
                }else if(indexPath.section == 3){
                  let cellText = tableView.dequeueReusableCell(withIdentifier: "cellDataText", for: indexPath) as! MorDetailsTableViewCell
                    cellText.textViewTitle.text = section1[6]
                   cellTextData = cellText
                cellText.textViewData.keyboardType = .numberPad
                   return cellText
                }else if(indexPath.section == 4){
                  let cellPhoto = tableView.dequeueReusableCell(withIdentifier: "cellDataPhoto", for: indexPath) as! PhotoMeterTableViewCell
                    cellPhotoData = cellPhoto
                cellPhoto.btnClose.isHidden = true
                    return cellPhoto
            }else if(indexPath.section == 6){
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! UITableViewCell
                               
                               return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellButton", for: indexPath) as! UITableViewCell
                
                return cell
            }
             
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            if(section == 3 || section == 4){
               return 10.0
           }
            return 0.0
         }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
              
           
              let header = tableView.dequeueReusableCell(withIdentifier: "header") as! UITableViewCell
            return header
              
          }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if(indexPath.section == 4 || indexPath.section == 5){
                return 100
            }else if( indexPath.section == 6){
                return 130
            }
            return UITableViewAutomaticDimension
            
        }
}
extension SendMeterReadingViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
     

  
    
   public func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return
        }
    cellPhotoData.imgPhoto.image = image
    self.imageData = UIImageJPEGRepresentation(image, 1.0)
    cellPhotoData.btnClose.isHidden = false
    self.setImage = 1
       self.dismiss(animated: true, completion: nil)
    }
}
