//
//  BDFirstViewController.swift
//  Sample
//
//  Created by kamal saboo on 24/05/18.
//  Copyright © 2018 Viral. All rights reserved.
//

import UIKit

class BDFirstViewController: UIViewController, UITextFieldDelegate, LibraryPaymentStatusProtocol, CMPopTipViewDelegate,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var amountField: UITextField!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var txtPayCategory: UITextField!
    var bdvc: BDViewController!
    
    var keyboardDoneButtonView = UIToolbar()
    var popTipView = CMPopTipView()
    var msg = [Any]()
    var pmtOptArr = [Any]()
    var blnShowingFilterPopUp = false
    let IDIOM = UI_USER_INTERFACE_IDIOM()
    
    //MARK : - Initializer
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pmtOptArr = ["CC-F", "DC-f", "QP-F", "MB-f", "NB-F", "UPI-F", "SI-F", "TEZ-f", "UPI-f", "CCW-F"]
        // Do any additional setup after loading the view.
        keyboardDoneButtonView.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(BDFirstViewController.doneClicked(_:)))
        keyboardDoneButtonView.items = [doneButton]
        amountField.delegate = self
        amountField.inputAccessoryView = keyboardDoneButtonView
        emailField.delegate = self
        //Just for testing
        amountField.text = "1.0"
        emailField.text = "qwww@abc.com"
        
        //--EDIT-------------------------------------------------------------------
        
        //You need to do it somewhere so that it'll not give error for [mutArrayRound insertObject: atIndex:]
        
        //        var mutDic = [AnyHashable: Any]()
        //        var dict = [
        //            "P1" : "50",
        //            "P2" : "65"
        //        ]
        //        mutDic["1"] = "Score_Valaue"
        //        mutDic["data"] = ""
        //        print(" Dict is [\(mutDic)]")
        //        var dict1 = mutDic["data"] as? [AnyHashable: Any]
        //        if dict1?.count == nil {
        //            if let aDict1 = dict1 {
        //                print("mutDic inside dict is [\(aDict1)]")
        //            }
        //        } else {
        //            if let aDict1 = dict1 {
        //                print("mutDic inside dict is [\(aDict1)]")
        //            }
        //        }
        
    }
   
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .UIDeviceOrientationDidChange, object: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func payNowClicked(_ sender: Any) {
        
        amountField.resignFirstResponder()
        let amt_regex = "(?=.)^\\$?(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+)?(\\.[0-9]{1,2})?$"
        let regex = "\\b([a-zA-Z0-9%_.+\\-]+)@([a-zA-Z0-9.\\-]+?\\.[a-zA-Z]{2,6})\\b"
        let regextest = NSPredicate(format: "SELF MATCHES %@", amt_regex)
        let emailtest = NSPredicate(format: "SELF MATCHES %@", regex)
        if (amountField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 || amountField.text == nil) {
            
            let alertController = UIAlertController(title: "Sorry", message: "Please enter valid amount address to proceed", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                print("Cancel")
            })
            self.present(alertController, animated: true, completion: nil)
            
        } else if !regextest.evaluate(with: amountField.text) {
            
            let alertController = UIAlertController(title: "Sorry", message: "Please enter valid amount to proceed", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                print("Cancel")
            })
            self.present(alertController, animated: true, completion: nil)
        }else{
    
            SHKActivityIndicator.current().displayActivity("Fetching Data")
            let param1 = "amt=\(String(describing: amountField.text))&email=\(String(describing: emailField.text))"
            let str1 = "\(get_msg_token_string)"
            print("url dfgf str \(str1) ")
            
            
            
               // var respoStr = "RELSMART|BDT1712060041|NA|2.00|RJM|NA|NA|INR|DIRECT|R|relsmart|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|https://uat.billdesk.com/pgidsk/pgmerc/pg_dump.jsp|03CF79A0BBF4FC94BDF6EF9CBF1B5D075E4DA905B1CD6FCB698F631F0791E69D^NA"
                //  AIRMTST|ARP1512535012850|NA|null|NA|NA|NA|INR|NA|R|airmtst|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|http://202.189.247.38/billdesk/pg_resp.php|3022040893^AIRMTST|ARP1512535012850|NA|null|NA|NA|NA|INR|NA|R|airmtst|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|http://202.189.247.38/billdesk/pg_resp.php|2095100094|CP1005!AIRMTST!C6BE6AF66574672C3A977A735A6A57638D59A3AB72ABE891DA491ACD9CBBA834!NA!NA!NA
            
                   // let respoStr = "SAVEPGI|3720180520221132|NA|800|NA|NA|NA|INR|NA|R|savepgi|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|http://120.138.8.110/PGQA/PGDump|1761332264"
                    // let respoStr = "AIRMTST|ARP1527242111180|NA|1.00|NA|NA|NA|INR|NA|R|airmtst|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|https://uat.billdesk.com/pgidsk/pgmerc/pg_dump.jsp|2671260411"
                    //let respoStr = "AIRMTST|ARP1531475316250|NA|2|NA|NA|NA|INR|NA|R|airmtst|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|http://www.moamc.com/investonline/Pages/Invest/ResponseOfTopUpMobile.aspx|2616167553"
                    //let respoStr = "AIRMTST|ARP1531475316250|NA|2|NA|NA|NA|INR|NA|R|airmtst|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|https://www.motilaloswalMf.com/investonline/Pages/Invest/ResponseOfTopUpMobile.aspx|440453795"
            //**************** Below message for UPI Success  *****************//
          // let respoStr = "BDMONITOR|IndiaIdeas.com|NA|2|NA|NA|NA|INR|NA|R|bdmonitor|NA|NA|F|Andheri|Mumbai|02240920005|support@billdesk.com|NA|NA|NA|https://uat.billdesk.com/pgidsk/pgmerc/pg_dump.jsp|3405523928"
            let respoStr = "AIRMTST|ARP1533532648158|NA|null|NA|NA|NA|INR|NA|R|airmtst|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|https://uat.billdesk.com/pgidsk/pgmerc/pg_dump.jsp|1609093187"
            let token = "AIRMTST|ARP1533532648158|NA|null|NA|NA|NA|INR|NA|R|airmtst|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|https://uat.billdesk.com/pgidsk/pgmerc/pg_dump.jsp|841467614|CP1005!AIRMTST!2A865D4374C6FD1D0693D4DF42460274202A977897B0ED71BEA1612E9A418326!NA!NA!NA"
                   // let respoStr = "BDMONITOR|IndiaIdeas.com|NA|2|NA|NA|NA|INR|NA|R|bdmonitor|NA|NA|F|Andheri|Mumbai|02240920005|support@billdesk.com|NA|NA|NA|https://uat.billdesk.com/pgidsk/pgmerc/pg_dump.jsp"
            
            //**************** Below message for UPI failure  *****************//
                    //let respoStr = "RILTST|BDTEST0000001|NA|2.0|NA|NA|NA|INR|NA|R|riltst|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|https://uat.billdesk.com/pgidsk/pgmerc/pg_dump.jsp|88B83A75333FF16838A285C60228668FEE7A56368FD6874994FDA4B1E8C88805"
            
            
                    //let respoStr = "IDBIFORTIS|0406345|NA|1.00|NA|NA|NA|INR|NA|R|idbifortis|NA|NA|F|4000831305|NA|NA|NA|NA|NA|NA|https://customer.idbifederal.com/_layouts/pg/paystatus.aspx|1649630785"
                    //let respoStr = "DOLPHINHTL|SI201806@1529747640|NA|7669.00|NA|NA|NA|INR|NA|R|dolphinhtl|NA|NA|F|1234567890|test@gmail.com|10.0.5.224|Test|NA|7669.00|mobile|https://booking.ramojifilmcity.com/hotels/booking/mobileconfirmation|A3F3EF32EAF4E0A40EE90B6E586168B7D9AE31CE74E7EBA42C0BA18B077934CA";
                    print("text pay category \(txtPayCategory.text!)")
                    self.bdvc = BDViewController(message: respoStr, andToken:token, andEmail: emailField.text!, andMobile: "9819700500", andTxtPayCategory: txtPayCategory.text!)
            
                    self.bdvc?.delegate = self
                    SHKActivityIndicator.current().displayCompleted("")
                    self.bdvc?.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(self.bdvc!, animated: true)
                        
            //})
            /*
            let commonApiObj = CommonAPICall()
            //https://uat.billdesk.com/BDMService/combmsg.jsp?amt=1.00&email=def@billdesk.com

            commonApiObj.getApiCall(url: "\(get_msg_token_string)?amt=\(amountField.text!)&email=\(emailField.text!)", completion: { (result) in
                //self.decodeJsonResponse(resultData: result)
               
                
                do {
                    
                    
                    let someDictionaryFromJSON = try JSONSerialization.jsonObject(with: result, options: .allowFragments) as! [String: Any]
                    print(someDictionaryFromJSON)
                    //   let json4Swift_Base = Json4Swift_Base(someDictionaryFromJSON)
                    print(someDictionaryFromJSON)
                    
//                    let jsonDecoder = JSONDecoder()
//                    let responseModel = try jsonDecoder.decode(Json4Swift_Base.self, from: data)
//                    print(responseModel.options![0].id)
                    
                } catch let err {
                    print(err)
                }

                
            }, failure: {(getError)
                in
                print("error msg \(getError)")
                
            })*/
        }
    }
    
    // MARK : - Payment status protocol method
    func paymentStatus(_ message: String!) {
        navigationController?.popToViewController(self, animated: true)
        print("payment s`tatus response [\(message)]")
        //[self showAlert:message];
        var responseComponents = message.components(separatedBy: "|")
        if responseComponents.count >= 25 {
            var statusCode = responseComponents[14]
            showAlert(statusCode)
        } else {
            showAlert("Something went wrong")
        }
    }
    
    func onError(_ exception: NSException?) {
        if let anException = exception {
            print("Exception got in Merchant App \(anException)")
        }
    }
    
    func tryAgain() {
        print("Try again method in Merchant App")
    }
    
    func cancelTransaction() {
        print("Cancel Transaction method in Merchant App")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func showAlert(_ message: String) {
        
        let alertController = UIAlertController(title: "Payment status", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
            print("Cancel")
        })
        self.present(alertController, animated: true, completion: nil)
    }
    
    func createJsonObject(_ serverStr: String?) -> String? {
        let mostOuter = serverStr?.components(separatedBy: "^")
        let token = mostOuter?[1]
        let outerList = mostOuter?[0].components(separatedBy: "|")
        print("token is \(token ?? "")")
        if let aList = outerList {
            print("outerList is \(aList)")
        }
        let rawData = outerList?[7].components(separatedBy: "~")
        if let aData = rawData {
            print("rawData is \(aData)")
        }
        return token
    }
    @IBAction func doneClicked(_ sender: Any) {
        print("Done Clicked.")
        view.endEditing(true)
    }
    @IBAction func dropBtnClked(_ sender: Any) {
        if blnShowingFilterPopUp {
            return
        }
        var tempcontentView: UIView!
        if IDIOM == UIUserInterfaceIdiom.pad  {
            tempcontentView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 250))
        } else {
            let screenRect: CGRect = UIScreen.main.applicationFrame
            if screenRect.size.height == 568 {
                //xibName=@"ViewiPhone4inch";
                tempcontentView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 200))
            } else {
                // xibName=@"ViewiPhone";
                tempcontentView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 200))
            }
        }
        var tableView = UITableView(frame: tempcontentView.bounds, style: .plain)
        tableView.delegate = self
        tableView.dataSource = self
        tempcontentView.addSubview(tableView)
        popTipView = CMPopTipView(customView: tempcontentView)
        popTipView.delegate = self
        popTipView.shouldEnforceCustomViewPadding = true
        popTipView.disableTapToDismiss = true
        popTipView.preferredPointDirection = .up
        popTipView.hasGradientBackground = true
        popTipView.cornerRadius = 2.0
        popTipView.sidePadding = 10.0
        popTipView.topMargin = 0.0
        popTipView.pointerSize = 10.0
        popTipView.hasShadow = true
        
        popTipView.backgroundColor = UIColor.white
        popTipView.textColor = UIColor.black
        popTipView.animation = CMPopTipAnimation(rawValue: Int(arc4random() % 2))!
       // popTipView.has3DStyle = Bool(arc4random() % 2)
        popTipView.dismissTapAnywhere = true
        var button = sender as? UIButton
        popTipView.presentPointing(at: button, in: view, animated: true)
        blnShowingFilterPopUp = true

    }
    
    //MARK : tableview Datasource and Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // my code
        return pmtOptArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CellIdentifier = "newFriendCell"
        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier)
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: CellIdentifier)
        }
        cell?.textLabel?.text = pmtOptArr[indexPath.row] as? String
        //etc.
        if let aCell = cell {
            return aCell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Pushing next view
        txtPayCategory.text! = pmtOptArr[indexPath.row] as! String
        popTipView.dismiss(animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
        blnShowingFilterPopUp = false
    }
    // MARK: PopUp Tip Delegate
    func popTipViewWasDismissed(byUser popTipView: CMPopTipView?) {
        blnShowingFilterPopUp = false
    }
    
//    var shouldAutorotate: Bool {
//        //return (UIInterfaceOrientationLandscapeLeft);
//        // NSLog(@"viewController autorotate");
//        return true
//    }
//
//    var supportedInterfaceOrientations: UIInterfaceOrientationMask {
//        //return UIInterfaceOrientationMaskLandscape;
//        return .all
//        //return UIInterfaceOrientationMaskPortrait;
//    }

}
