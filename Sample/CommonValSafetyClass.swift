//
//  CommonClass.swift

//

import UIKit

import Reachability
import CoreData
import MBProgressHUD
import PKHUD
import AVFoundation
import Charts
import NVActivityIndicatorView

var baseColor  : String = "295890"
class CommonVSClass: UIViewController, NVActivityIndicatorViewable ,ScreenshotSharerViewControllerProtocol, ScreenshotSharerViewControllerTextProtocol, ScreenshotSharerViewControllerFontProtocol, ScreenshotSharerViewControllerColorProtocol{
    
    open weak var screenshotSharerInstance:ScreenshotSharer?
       
       open func screenshotSharer() -> ScreenshotSharer?
       {
           return self.screenshotSharerInstance
       }
       
       open func setScreenshotSharer(_ screenshotSharer: ScreenshotSharer)
       {
           self.screenshotSharerInstance = screenshotSharer
       }
       func randomString(length: Int) -> String {
         let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
         return String((0..<length).map{ _ in letters.randomElement()! })
       }
       open func setScreenshotImage(_ image:UIImage) {}
       
       open func setShareTitleText(_ text:String) {}
       open func setShareDescriptionText(_ text:String) {}
       open func setShareButtonTitleText(_ text:String) {}
       
       open func setShareTitleFont(_ font:UIFont) {}
       open func setShareDescriptionFont(_ font:UIFont) {}
       open func setShareButtonTitleFont(_ font:UIFont) {}
       
       open func setShareTitleTextColor(_ color:UIColor) {}
       open func setShareDescriptionTextColor(_ color:UIColor) {}
       open func setShareButtonTitleColor(_ color:UIColor) {}
       open func setShareButtonBackgroundColor(_ color:UIColor) {}
    // Activity Indicator(start)
    var confirmation = String()
    var electricityBilllbl = String()
      let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let label: UITextView = UITextView()
    let active: UIActivityIndicatorView = UIActivityIndicatorView()
    var refresh = UIRefreshControl()
//    func startLoading(){
//
//
//        HUD.show(.progress)
//
//    }
//    open func takeScreenshot(_ shouldSave: Bool = true) -> UIImage? {
//        var screenshotImage :UIImage?
//        let layer = UIApplication.shared.keyWindow!.layer
//        let scale = UIScreen.main.scale
//        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, 100.0);
//        guard let context = UIGraphicsGetCurrentContext() else {return nil}
//        layer.render(in:context)
//        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        if let image = screenshotImage, shouldSave {
//            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
//        }
//        return screenshotImage
//    }
    func dateFormatChange(inputDateStr:String,inputFormat:String,outputFromat:String) -> String
    {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = inputFormat
        dateFormatter.locale = NSLocale(localeIdentifier: "en") as Locale
        let resultDate: Date = dateFormatter.date(from: inputDateStr)!
        
        dateFormatter.dateFormat = outputFromat
        
        let resultDateStr = dateFormatter.string(from: resultDate)
        
        if(Debug.show_debug == true)
        {
           // print("resultDate",resultDateStr)
        }
        
        
        return resultDateStr
        
        
    }
     private let presentingIndicatorTypes = {
               return NVActivityIndicatorType.allCases.filter { $0 != .blank }
           }()
        
        func startLoadingPK(view : UIView,text : String){
           
    //MBHud.show(animated: true)
    //         view.addSubview(MBHud)
    //       HUD.show(.labeledRotatingImage(image: UIImage(named: "loading"), title: "Processing", subtitle: ""))
            let indicatorType = presentingIndicatorTypes[29]
    let size = CGSize(width: 30, height: 30)
            startAnimating(size, message: remLoad, type: indicatorType, fadeInAnimation: nil)
           
            
        }
        func stopLoadingPK(view : UIView){
            
           // MBHud.hide(animated: true)
        //  HUD.hide()
            self.stopAnimating(nil)
            
        }
    func addAttributeTextView(strBuilder : String,convertString : String)-> NSMutableAttributedString{
        let paragraph = NSMutableParagraphStyle()
              paragraph.alignment = .left
              paragraph.lineSpacing = 0
   
                              
        let agreeAttributedString = NSMutableAttributedString(string: strBuilder, attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "RobotoSlab-Regular", size:15.0)])
           
        
                              
                              //Submitted
        let SubmittedAttributedString = NSAttributedString(string:convertString, attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font:  UIFont(name: "RobotoSlab-Bold", size:15.0)])
                              
                              let range: NSRange = (agreeAttributedString.string as NSString).range(of: convertString)
                              if range.location != NSNotFound {
                                  agreeAttributedString.replaceCharacters(in: range, with: SubmittedAttributedString)
                              }
        agreeAttributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraph, range: NSRange(location: 0, length: agreeAttributedString.length))
        return agreeAttributedString
    }
    func addAttributeTextViewWithFont(strBuilder : String,convertString : String,size : CGFloat)-> NSMutableAttributedString{
         let paragraph = NSMutableParagraphStyle()
               paragraph.alignment = .left
               paragraph.lineSpacing = 0
    
                               
        let agreeAttributedString = NSMutableAttributedString(string: strBuilder, attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "RobotoSlab-Regular", size:size)])
            
         
                               
                               //Submitted
        let SubmittedAttributedString = NSAttributedString(string:convertString, attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font:  UIFont(name: "RobotoSlab-Bold", size:size)])
                               
                               let range: NSRange = (agreeAttributedString.string as NSString).range(of: convertString)
                               if range.location != NSNotFound {
                                   agreeAttributedString.replaceCharacters(in: range, with: SubmittedAttributedString)
                               }
        agreeAttributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraph, range: NSRange(location: 0, length: agreeAttributedString.length))
         return agreeAttributedString
     }
    var dated = String()
    var remNot = String()
    var toastConsumer = String()
     var AppName = String()
    var addCons = String()
   var remLoad = String()
    var addSure = String()
    var removeSure = String()
    var billPayment = String()
    var meterRead = String()
    var payHist = String()
    var complaintReg = String()
    var consumptionPattern = String()
    var rebate = String()
    var tarrif = String()
    var addcons = String()
     var putOTp = String()
    var remCons = String()
    var settingVal = String()
   var langg = String()
      var feedb = String()
      var privacyy = String()
    var morbijRem = String()
    var remove = String()
     var cancel = String()
     var addd = String()
    var addSureRem = String()
    var invAmt = String()
    var invalidamt = String()
    var okay = String()
    var sorry = String()
    var consRem = String()
    var addNot = String()
    var enterMeter = String()
    var addSureMeter = String()
    var yess = String()
    var nooo = String()
    var compStrNot = String()
    var regComp = String()
     var regCompAlready = String()
    var addConsAlready = String()
    var verifyy = String()
    var verifyyOTP = String()
var enterOtp = String()
    var resendOtp = String()
    var secCount = String()
    var notice = String()
    var bhasha = String()
    var loggOut = String()
    var billAmtPayyy = String()
    var loggOu1 = String()
    var halfffYogna  = String()
    var feedCons  = String()
    var NotfeedCons  = String()
    var RecSave = String()
    var RecSend = String()
    var add111 = String()
    var add112 = String()
    var add113 = String()
    var sendOtp = String()
    var mismatchOTP = String()
    var linkOTPerror = String()
    var morbijiliee = String()
  //  let locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.becomeFirstResponder()
        AppName = "Mor Bijili"
        self.active.center = self.view.center
        self.active.activityIndicatorViewStyle = .gray
        self.active.hidesWhenStopped = true
        self.view.addSubview(self.active)
        
        if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
            morbijiliee = "Mor Bijili"
            linkOTPerror = "Because of some techniqual issue the , mobile is not been linked with the B.P No"
            putOTp = "Please enter OTP"
            mismatchOTP = "OTP Mismatch"
            add111 = "Given B.P No is already registered with "
            add112 = " this mobile number,So Kindly send OTP to mobile number "
            add113 = " for verification "
            RecSave = "Reciept Saved"
            RecSend = "Reciept Send"
            enterOtp = "Enter OTP"
            regCompAlready = "Complaint already registered , Your Complaint No is - "
            addConsAlready = "B.P No already added "
            billAmtPayyy = "Amount trying to pay is less than Bill Amount"
            halfffYogna = "The rebate recieved from state government under the Bijlee Bill Half Yojns is Rs. "
            dated = "dated"
            loggOu1 = "Logout"
            loggOut = "Are you sure to Logout?"
            bhasha = "Are you sure to change language?"
            notice = "Notice"
            resendOtp = "Resend OTP"
            sendOtp = "Send OTP"
            verifyy = "Verify"
            verifyyOTP = "Verify OTP"
            secCount = "secs , resend OTP"
            regComp = "Complaint Registered Successfully , Your Complaint No is - "
            compStrNot = "Not able to Register Complaint"
            NotfeedCons = "Not able to save Feedback"
            yess = "YES"
            nooo = "NO"
            enterMeter = "Please Enter Meter Reading"
            remNot = "Not able to remove Consumer No (BP No.)"
            addNot = "Not able to add Consumer No (BP No.)"
            addCons = "Consumer No (BP No.) added successfully."
            feedCons = "Feedback saved successfully."
            consRem = "Consumer removed successfully."
            sorry = "Sorry"
            invalidamt = "Please enter valid amount to proceed"
            okay = "OK"
            invAmt = "Invalid Amount for payment , minimum amount must be more Rs 10"
            toastConsumer = "Enter Consumer No(BP No.)"
            addd = "Add"
            addSure = "Are you sure you want to add B.P No -"
            addSureMeter = "Are you sure you want to add Meter Reading ?"
            addSureRem = "from Mor Bijili App ?"
            remLoad = "please wait..."
            cancel = "Cancel"
            remove = "Remove"
            confirmation = "Confirmation"
            electricityBilllbl = "Electricity Bill"
            removeSure = "Are you sure you want to remove B.P No -"
            morbijRem = "from Mor Bijili App ?"
            billPayment = "Bill Payment"
            meterRead = "Send Meter Reading"
            payHist = "Payment History"
            complaintReg = "Complaint Registration"
            consumptionPattern = "Consumption Pattern"
            rebate = "Rebate on bijlee Bill Half Yojna"
            
            tarrif = "Tariff ( Electricity Rate )"
            addcons = "Add Consumer No (BP.No)."
            remCons = "Remove Consumer No (BP.No)."
            settingVal = "Setting"
            
            langg = "Change Language"
                       feedb = "Feedback"
                       privacyy = "Privacy"
        }else{
            morbijiliee = "मोर बिजली"
            linkOTPerror = "कुछ तकनीकी कारणों की वजह से जानकारी उपलब्ध नहीं हो पा रही है या फिर आपका मोबाइल नंबर किसी भी उपभोगता क्र. से लिंक नहीं है , कृपया अपना मोबाइल नंबर लिंक करिये |"
            putOTp = "कृपया ओ.टी.पी दर्ज करे |"
            mismatchOTP = "ओ.टी.पी मिलान नही हुआ |"
            add111 = "दर्ज उपभोगता क्रमांक पहले से ही "
            add112 = " मोबाइल नंबर के साथ रजिस्टर्ड है | अतः कृपया अपने मोबाइल नंबर "
            add113 = " पर ओ.टी.पी भेजकर वेरीफाई करे |"
            RecSend = "रिसीप्ट सेंड हो गया है "
            RecSave = "रिसीप्ट सेव हो गया है "
            enterOtp = "ओ.टी.पी दर्ज करें"
            feedCons = "आपका फीडबैक सेव कर लिया गया है "
            regCompAlready = "कंप्लेंट पहले ही दर्ज हो गया है , आपका कम्प्लेन नंबर है - "
            addConsAlready = "उपभोगता क्रमांक (B.P No) पहले ही दर्ज हो गया है "
            halfffYogna = "बिजली बिल हाफ योजना अंतर्गत राज्य शासन से प्राप्त छूट "
            billAmtPayyy = "आप जो राशि पेमेंट करना चाहते है वो भुगतान राशि से कम है |"
            dated = "दिनांक"
            loggOu1 = "लॉगआउट"
            bhasha = "क्या आप अपनी भाषा बदलना चाहते है ?"
            loggOut = "क्या आप लॉगआउट करना चाहते है ?"
            notice = "सुचना"
            secCount = "सेकंड के बाद ही ओ.टी.पी भेजा जा सकता है |"
            resendOtp = "ओ.टी.पी दोबारा भेजे"
            sendOtp = "ओ.टी.पी भेजे"
            verifyy = "जाँच"
            verifyyOTP = "ओ.टी.पी की जाँच"
            regComp = "कंप्लेंट दर्ज हो गया है , आपका कम्प्लेन नंबर है - "
            yess = "हाँ"
            nooo = "ना"
             
            enterMeter = "कृपया मीटर रीडिंग दर्ज करें"
            compStrNot = "कंप्लेंट दर्ज नही किया गया है"
            NotfeedCons = "फीडबैक सेवर्ज नही किया गया है"
            remNot = "उपभोक्ता क्रमांक(BP No.) नही हटाया गया है"
            addNot = "उपभोक्ता क्रमांक(BP No.) नही जोड़ा गया है"
            addCons = "उपभोक्ता क्रमांक(BP No.) जोड़ दिया गया है."
            consRem = "उपभोक्ता क्रमांक(BP No.) हटा दिया गया है"
            sorry = "माफी"
            okay = "ठीक है"
            invAmt = "अमान्य राशि, न्यूनतम राशि 10 रुपये होनी चाहिए"
            invalidamt = "कृपया आगे बढ़ने के लिए मान्य राशि दर्ज करें"
            toastConsumer = "कृपया उपभोक्ता क्रमांक(BP No.) दर्ज करे"
            addd = "जोड़े"
            addSure = "क्या आप उपभोक्ता क्रमांक -"
            remLoad = "कृपया प्रतीक्षा करें..."
            cancel = "रद्द"
            remove = "निकाले"
        removeSure = "क्या आप उपभोक्ता क्रमांक -"
            addSureRem = "को अपने मोर बिजली ऐप के अकाउंट में जोड़ना चाहते है?"
            addSureMeter = "क्या आप मीटर रीडिंग दर्ज करना चाहते है ?"
        morbijRem = "को अपने मोर बिजली ऐप के अकाउंट में से हटाना चाहते है?"
            langg = "भाषा बदले"
            feedb = "फीडबैक"
            privacyy = "प्राइवेसी पोलिसी"
            electricityBilllbl = "बिजली बिल"
            confirmation = "पुष्टीकरण"
            
            billPayment = "बिजली बिल पेमेंट"
                       meterRead = "मीटर रीडिंग भेजे"
                       payHist = "बिल भुगतान विवरण"
                       complaintReg = "बिजली सप्लाई शिकायत"
                       consumptionPattern = "बिजली खपत पैटर्नर्न"
                       rebate = "बिजली बिल हाफ योजना से प्राप्त छूट"
                       
                       tarrif = "टेर्रिफ(बिजली की दरें)"
                       addcons = "उपभोक्ता क्रमांक(BP.No) जोड़े"
                       remCons = "उपभोक्ता क्रमांक(BP.No) हटाएँ"
            settingVal = "सेटिंग"
            
        }
   
        
        
       
//        timer1 = SwiftTimer.repeaticTimer(interval: .seconds(5)) { timer in
//                  
//                  
//                  self.updateLoc()
//              }
//              timer1.start()
        
    }
  
 
    override var canBecomeFirstResponder: Bool {
           get {
               return true
           }
       }
    
 

    
    func checkDate(dateNow : Date) -> String {
        
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "dd-MM-yyyy"
        let n1Date = addDaystoGivenDate(baseDate:Date(),NumberOfDaysToAdd:1)
        let p1Date = addDaystoGivenDate(baseDate:Date(),NumberOfDaysToAdd:-1)
        
        let preDate = formatter1.string(from: p1Date)
        let nextDate = formatter1.string(from: n1Date)
        let dateNowStr = formatter1.string(from: dateNow)
        let todayDate = formatter1.string(from: Date())
        
        if(nextDate == dateNowStr){
            return "Tomorrow"
        }else if(preDate == dateNowStr){
            return "Yesterday"
        }else if(todayDate == dateNowStr){
            return "Today"
        }
        
        
        return ""
    }
    
    
    
    
    
    
    
    func verifyUrl (urlString: String?) -> Bool {
              //Check for nil
              if let urlString = urlString {
                  // create NSURL instance
                  if let url = NSURL(string: urlString) {
                      // check if your application can open the NSURL instance
                   return UIApplication.shared.canOpenURL(url as URL)
                  }
              }
              return false
          }

//    func linkPDF(urlData : String , title : String){
//              let storyBoard = UIStoryboard(name: "Truck", bundle: nil)
//              let proNav = storyBoard.instantiateViewController(withIdentifier: "ProgressWebNavViewController") as! ProgressWebNavViewController
//
//              let  progressWebViewController = proNav.topViewController as! ProgressWebViewController
//              guard   let url = URL(string: urlData) else {
//                  return
//              }
//              progressWebViewController.disableZoom = true
//              progressWebViewController.url = url
//              progressWebViewController.bypassedSSLHosts = [url.host!]
//              progressWebViewController.userAgent = "ProgressWebViewController/1.0.0"
//              progressWebViewController.websiteTitleInNavigationBar = false
//              progressWebViewController.navigationItem.title = title
//              progressWebViewController.leftNavigaionBarItemTypes = [.reload]
//              progressWebViewController.toolbarItemTypes = [.back, .forward, .activity]
//           progressWebViewController.modalPresentationStyle = .fullScreen
//              self.present(proNav, animated: true, completion: nil)
//          }
//
    func stringFromNumber(strNo : String) -> String{
        let largeNumber = Int(strNo)
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        let formattedNumber = numberFormatter.string(from: NSNumber(value:largeNumber!))
        
        return formattedNumber!
        
    }
    func addDaystoGivenDate(baseDate:Date,NumberOfDaysToAdd:Int)->Date
    {
        var dateComponents = DateComponents()
        let CurrentCalendar = Calendar.current
        let CalendarOption = NSCalendar.Options()
        
        dateComponents.day = NumberOfDaysToAdd
        let newDate = CurrentCalendar.date(byAdding: dateComponents, to: baseDate)
        // let newDate = CurrentCalendar.dateByAddingComponents(dateComponents, toDate: baseDate, options: CalendarOption)
        return newDate!
    }
    func startLoading(view:UIView){
        
        
        active.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2 - 100)
        active.activityIndicatorViewStyle = .gray
        active.startAnimating()
        self.active.isHidden = false
        active.isUserInteractionEnabled = false
        active.hidesWhenStopped = true
        view.addSubview(active)
        
        
    }
  
   
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    
    func fontDataSemiBold() -> UIFont {
        return UIFont.systemFont(ofSize: 18.0, weight: .semibold)
    }
    
    func fontDataBold() -> UIFont {
        return UIFont.systemFont(ofSize: 15.0, weight: .medium)
    }
    
    func stopLoading(view:UIView){
        
        
        active.stopAnimating()
        self.active.isHidden = true
        
    }
    func startLoading()
    {
      self.active.startAnimating()
    }
    func stopLoading()
    {
     self.active.stopAnimating()
        
 
    }
    var MBHud = MBProgressHUD(frame: CGRect(x: 0, y: -100, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height + 100))
    
     var pkHud = PKHUD()
   
    
    func startLoading(TB: UITableView){
        
        
        active.center = TB.center
        active.activityIndicatorViewStyle = .gray
        active.startAnimating()
        active.isUserInteractionEnabled = false
        active.hidesWhenStopped = true
        TB.addSubview(active)
        
    }
    
//    func stopLoading(){
//        HUD.hide()
//
//
//    }
    
    

    
    /// No Internet Image(Start)
    func noInternet()
    {
        let image: UIImageView = UIImageView()
        image.frame = CGRect.init(x: 20, y: 200, width: self.view.frame.width - 50, height: 250)
        image.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleLeftMargin, . flexibleRightMargin]
        
        image.center = self.view.center
        
        image.layer.cornerRadius = 10.0
        
        image.image = UIImage.init(named: "noReach")
        
        self.view.addSubview(image)
    }
    /// (end)
    
    
    
    /// No Data Image(Start)
    func noData()
    {
        let image: UIImageView = UIImageView()
        image.frame = CGRect.init(x: 20, y: 200, width: self.view.frame.width - 50, height: 250)
        image.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleLeftMargin, . flexibleRightMargin]
        
        image.center = self.view.center
        
        image.layer.cornerRadius = 10.0
        
        image.image = UIImage.init(named: "no_data")
        
        self.view.addSubview(image)
    }
    ///(end)
    
    
    /// No Data Label(Start)
    func noDataLabel(text: String)
    {
        
        
        label.frame = CGRect.init(x: 30, y: 20, width: self.view.frame.width - 20, height: 90)
        label.center = self.view.center
        label.font = UIFont.systemFont(ofSize: 15)
        label.text = text
       label.isEditable = false
        label.isSelectable = false;
        label.isScrollEnabled = false
        label.textAlignment = .center
        label.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleWidth]
        label.backgroundColor = UIColor.clear
        self.view.addSubview(label)
        
    }
    ///(end)
    
    func noInternetLabel()
    {
        
        let label: UILabel = UILabel()
        label.frame = CGRect.init(x: 30, y: 20, width: self.view.frame.width - 20, height: 30)
        label.center = self.view.center
        label.font = UIFont.systemFont(ofSize: 15)
        label.text = "No internet connection found. Check your internet connection or try again."
        label.numberOfLines = 2
        label.sizeToFit()
        label.textAlignment = .center
        label.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleWidth]
        
        self.view.addSubview(label)
        
    }
    
    func errorChecking(error: Error)
    {
        let errorStr = error.localizedDescription
        
        if error._code == NSURLErrorTimedOut {
            
            self.showSingleButtonWithMessage(title: "TimeOut!", message: errorStr, buttonName: "OK")
            
            // obj.stopLoading()
            
            
        }
        
        if("\(error._code)" == "-1003")
        {
            self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
        }
        else
        {
            
            self.showSingleButtonWithMessage(title: "Oops!", message: "something went wrong. Try Agin", buttonName: "OK")
            
        }
        
        
    }
    
    
    ///show alert for only display information
    func showSingleButtonAlertWithoutAction (title:String) {
        let alert = UIAlertController(title: title, message: "", preferredStyle: UIAlertController.Style.alert)
        self.present(alert, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    ///show alert with Single Button action
      func showSingleButtonAlertWithAction (title:String,buttonTitle:String,completionHandler:@escaping () -> ()) {
        let alert = UIAlertController(title: title, message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default, handler: { action in
               completionHandler()
           }))
           self.present(alert, animated: true, completion: nil)
       }
    func showSingleButtonAlertWithActionMessage (title:String,message : String,buttonTitle:String,completionHandler:@escaping () -> ()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default, handler: { action in
            completionHandler()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    ///show alert with right action button
    func showTwoButtonAlertWithRightAction (title:String,buttonTitleLeft:String,buttonTitleRight:String,completionHandler:@escaping () -> ()) {
        let alert = UIAlertController(title: title, message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: buttonTitleLeft, style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: buttonTitleRight, style: UIAlertAction.Style.default, handler: { action in
            completionHandler()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    ///show alert with left action button
    func showTwoButtonAlertWithLeftAction (title:String,message : String,buttonTitleLeft:String,buttonTitleRight:String,completionHandler:@escaping () -> ()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: buttonTitleLeft, style: UIAlertAction.Style.default, handler: { action in
            completionHandler()
        }))
        alert.addAction(UIAlertAction(title: buttonTitleRight, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    ///show alert with two action button
    func showTwoButtonAlertWithTwoAction(title:String,message : String,buttonTitleLeft:String,buttonTitleRight:String,completionHandlerLeft:@escaping () -> (),completionHandlerRight:@escaping () -> ()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: buttonTitleLeft, style: UIAlertAction.Style.default, handler: { action in
            completionHandlerLeft()
        }))
        alert.addAction(UIAlertAction(title: buttonTitleRight, style: UIAlertAction.Style.default, handler: { action in
            completionHandlerRight()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    func showSingleButtonWithMessage(title:String,message: String, buttonName: String)
    {
        
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: buttonName, style: UIAlertAction.Style.default, handler:nil))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    func getName()-> String{
        var name = String()
        
        let fname = UserDefaults.standard.string(forKey: "FirstName")
        let Mname = UserDefaults.standard.string(forKey: "MiddleName")
        let Lname = UserDefaults.standard.string(forKey: "Lastname")
        
        
        if(Mname == ""){
            name = fname! + " " + Lname!
        }else{
            name = fname! + " " + Mname!  + " " + Lname!
        }
        return name
    }
//    func errorChecking(error: Error)
//    {
//        let errorStr = error.localizedDescription
//
//        if error._code == NSURLErrorTimedOut {
//
//            self.showSingleButtonWithMessage(title: "TimeOut!", message: errorStr, buttonName: "OK")
//
//
//
//        }
//
//        if("\(error._code)" == "-1003")
//        {
//            self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
//
//        }
//        else
//        {
//
//            self.showSingleButtonWithMessage(title: "Oops!", message: "something went wrong. Try Agin", buttonName: "OK")
//
//        }
//
//
//    }
    
    func checkTextIsEmpty(text: String) -> Bool
    {
        
        if(text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)
        {
            return true
        }
        else
        {
            return false
        }
        
        
    }
    
    
    
    func noInternetShow()
    {
        self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
    }
    
    
    func alert (title:String){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            
            self.stopLoadingWithoutTouch()
            let alertController = UIAlertController(title: nil, message: title, preferredStyle: .alert)
            alertController.view.tintColor = UIColor.black
            self.present(alertController, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {() -> Void in
                alertController.dismiss(animated: true, completion: {() -> Void in
                    
                    
                })
            })
            
        }
    }
    
    func showToast(message : String) {
        
        
        let toastLabel = UILabel(frame: CGRect(x: 10, y: self.view.frame.size.height-100, width: self.view.frame.size.width-20, height: 35))
        //toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.blue
        toastLabel.numberOfLines = 0
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    func stopLoadingWithoutTouch()
    {
        self.active.stopAnimating()
        self.active.isUserInteractionEnabled = true
        //self.active.removeFromSuperview()
        
    }
    func IntegerData(strInteger : Int) -> Int {
           let maxInt = strInteger.numberOfDigits()
           if(maxInt > 0){
               if(maxInt == 1){
                   return 10
               }else if(maxInt == 2){
                   return 10
               }else if(maxInt == 3){
                   return 100
               }else if(maxInt == 4){
                   return 500
               }else if(maxInt == 5){
                   return 1000
               }else if(maxInt == 6){
                   return 2000
               }else if(maxInt == 7){
                   return 4000
               }else if(maxInt == 8){
                   return 8000
               }else{
                   return 10000
               }
           }else{
               return 10
           }
           
       }
//    var reachability = Reachability()!
//    func internetChecking(isAlert: Bool) -> Bool
//    {
//        if(self.reachability.connection != .none)
//        {
//            return true
//        }
//        else{
//
//            if(isAlert == true)
//            {
//                self.noInternetShow()
//            }
//            else
//            {
//                self.noDataLabel(text: "No internet connection avaliable. Please check your internet connection and try again!")
//            }
//
//
//            return false
//
//        }
//
//
//
//    }
    
    
    func downloadPDF(linkString:String,linkName:String,completionHandler:@escaping () -> ())
    {
        
        
        
            
        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
            let destinationFileUrl = documentsUrl.appendingPathComponent(linkName)
            print(linkString)
        let linkStringData = linkString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            //Create URL to the source file you want to download
        let fileURL = URL(string:linkStringData!)
            
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig)
            print(fileURL!)
            let request = URLRequest(url:fileURL!)
            
            let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                if let tempLocalUrl = tempLocalUrl, error == nil {
                    // Success
                    if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                        print(tempLocalUrl)
                        
                        print("Successfully downloaded. Status code: \(statusCode)")
                    }
                    
                    do {
                        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    } catch (let writeError) {
                        print("Error creating a file \(destinationFileUrl) : \(writeError)")
                    }
                    
                    completionHandler()
                    
                    
                } else {
                    //print("Error took place while downloading a file. Error description: %@", error?.localizedDescription as? Error);
                }
            }
            task.resume()
            
            
        
        
    }
    
    
    func checkPDFIsAvailable(linkName:String,  success:@escaping (String) -> Void,failure: @escaping () -> ())
    {
        
        
        print("linkName",linkName)
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        let filePath = url.appendingPathComponent(linkName)?.path
        let fileManager = FileManager.default
        
        //        cell.pdfFilepath = filePath!
        
        let pdfFilepath = filePath!
        print(filePath!)
        if fileManager.fileExists(atPath: filePath!) {
            
            print("FILE AVAILABLE")
            
            
            success(pdfFilepath)
            
            
        } else {
            
            print("FILE NOT AVAILABLE")
            
            failure()
            
            
            
        }
        
    }
    
    
}
public extension UIDevice {

    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }

        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod touch (5th generation)"
            case "iPod7,1":                                 return "iPod touch (6th generation)"
            case "iPod9,1":                                 return "iPod touch (7th generation)"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPhone12,1":                              return "iPhone 11"
            case "iPhone12,3":                              return "iPhone 11 Pro"
            case "iPhone12,5":                              return "iPhone 11 Pro Max"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad (3rd generation)"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad (4th generation)"
            case "iPad6,11", "iPad6,12":                    return "iPad (5th generation)"
            case "iPad7,5", "iPad7,6":                      return "iPad (6th generation)"
            case "iPad7,11", "iPad7,12":                    return "iPad (7th generation)"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad11,4", "iPad11,5":                    return "iPad Air (3rd generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad mini (5th generation)"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }

        return mapToDevice(identifier: identifier)
    }()

}
class Debug: NSObject {
    
    
    static let show_debug = true
    
}

extension String {

    func aesEncrypt(key:String, iv:String, options:Int = kCCOptionPKCS7Padding) -> String? {
        if let keyData = key.data(using: String.Encoding.utf8),
            let data = self.data(using: String.Encoding.utf8),
            let cryptData    = NSMutableData(length: Int((data.count)) + kCCBlockSizeAES128) {


            let keyLength              = size_t(kCCKeySizeAES128)
            let operation: CCOperation = UInt32(kCCEncrypt)
            let algoritm:  CCAlgorithm = UInt32(kCCAlgorithmAES128)
            let options:   CCOptions   = UInt32(options)



            var numBytesEncrypted :size_t = 0

            let cryptStatus = CCCrypt(operation,
                                      algoritm,
                                      options,
                                      (keyData as NSData).bytes, keyLength,
                                      iv,
                                      (data as NSData).bytes, data.count,
                                      cryptData.mutableBytes, cryptData.length,
                                      &numBytesEncrypted)

            if UInt32(cryptStatus) == UInt32(kCCSuccess) {
                cryptData.length = Int(numBytesEncrypted)
                let base64cryptString = cryptData.base64EncodedString(options: .lineLength64Characters)
                return base64cryptString


            }
            else {
                return nil
            }
        }
        return nil
    }

    func aesDecrypt(key:String, iv:String, options:Int = kCCOptionPKCS7Padding) -> String? {
        if let keyData = key.data(using: String.Encoding.utf8),
            let data = NSData(base64Encoded: self, options: .ignoreUnknownCharacters),
            let cryptData    = NSMutableData(length: Int((data.length)) + kCCBlockSizeAES128) {

            let keyLength              = size_t(kCCKeySizeAES128)
            let operation: CCOperation = UInt32(kCCDecrypt)
            let algoritm:  CCAlgorithm = UInt32(kCCAlgorithmAES128)
            let options:   CCOptions   = UInt32(options)

            var numBytesEncrypted :size_t = 0

            let cryptStatus = CCCrypt(operation,
                                      algoritm,
                                      options,
                                      (keyData as NSData).bytes, keyLength,
                                      iv,
                                      data.bytes, data.length,
                                      cryptData.mutableBytes, cryptData.length,
                                      &numBytesEncrypted)

            if UInt32(cryptStatus) == UInt32(kCCSuccess) {
                cryptData.length = Int(numBytesEncrypted)
                let unencryptedMessage = String(data: cryptData as Data, encoding:String.Encoding.utf8)
                return unencryptedMessage
            }
            else {
                return nil
            }
        }
        return nil
    }


}

@IBDesignable
class CardView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 2
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
    
}
@IBDesignable
class RoundedCornerView: UIView {
    
    var cornerRadiusValue : CGFloat = 0
    var corners : UIRectCorner = []
    var borderWidthValue : CGFloat = 0
    @IBInspectable public var borderWidth: CGFloat {
        get {
            return borderWidthValue
        }
        set {
            borderWidthValue = newValue
            setBorder(border : newValue)
        }
    }
    
    
    @IBInspectable public var cornerRadius : CGFloat {
        get {
            return cornerRadiusValue
        }
        set {
            cornerRadiusValue = newValue
        }
    }
    @IBInspectable public var topLeft : Bool {
        get {
            return corners.contains(.topLeft)
        }
        set {
            setCorner(newValue: newValue, for: .topLeft)
        }
    }
    
    
    @IBInspectable public var topRight : Bool {
        get {
            return corners.contains(.topRight)
        }
        set {
            setCorner(newValue: newValue, for: .topRight)
        }
    }
    
    @IBInspectable public var bottomLeft : Bool {
        get {
            return corners.contains(.bottomLeft)
        }
        set {
            setCorner(newValue: newValue, for: .bottomLeft)
        }
    }
    
    @IBInspectable public var bottomRight : Bool {
        get {
            return corners.contains(.bottomRight)
        }
        set {
            setCorner(newValue: newValue, for: .bottomRight)
        }
    }
    func setBorder(border:CGFloat) {
        if border > 0 {
            updateCornersBorder(value :border)
        }
    }
    func setCorner(newValue: Bool, for corner: UIRectCorner) {
        if newValue {
            addRectCorner(corner: corner)
        } else {
            removeRectCorner(corner: corner)
        }
    }
    
    func addRectCorner(corner: UIRectCorner) {
        corners.insert(corner)
        updateCorners()
    }
    
    func removeRectCorner(corner: UIRectCorner) {
        if corners.contains(corner) {
            corners.remove(corner)
            updateCorners()
        }
    }
    
    func updateCorners() {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: cornerRadiusValue, height: cornerRadiusValue))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        self.clipsToBounds = true
        //        if(borderWidthValue > 0){
        //        let borderLayer = CAShapeLayer()
        //        borderLayer.path = mask.path // Reuse the Bezier path
        //        borderLayer.fillColor = UIColor.clear.cgColor
        //        borderLayer.strokeColor = UIColor.black.cgColor
        //        borderLayer.lineWidth = 5
        //        borderLayer.frame = self.bounds
        //        self.layer.addSublayer(borderLayer)
        //        }
    }
    func updateCornersBorder(value :CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: cornerRadiusValue, height: cornerRadiusValue))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        self.clipsToBounds = true
        if(value > 0){
            let borderLayer = CAShapeLayer()
            borderLayer.path = mask.path // Reuse the Bezier path
            borderLayer.fillColor = UIColor.clear.cgColor
            borderLayer.strokeColor = UIColor.darkGray.cgColor
            borderLayer.lineWidth = 1
            borderLayer.frame = self.bounds
            self.layer.addSublayer(borderLayer)
        }
    }
    
}


extension Int {
    
    func numberOfDigits() -> Int {
        if abs(self) < 10 {
            return 1
        } else {
            return 1 + (self/10).numberOfDigits()
        }
    }
    
    func getDigits() -> [Int] {
        let num = self.numberOfDigits()
        var tempNumber = self
        var digitList = [Int]()
        
        for i in (0..<num).reversed() {
            let divider = Int(pow(CGFloat(10), CGFloat(i)))
            let digit = tempNumber/divider
            digitList.append(digit)
            tempNumber -= digit*divider
        }
        return digitList
    }
}
class ChartValueFormatter: NSObject, IValueFormatter {
    fileprivate var numberFormatter: NumberFormatter?
    
    convenience init(numberFormatter: NumberFormatter) {
        self.init()
        self.numberFormatter = numberFormatter
    }
    
    func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String {
        guard let numberFormatter = numberFormatter
            else {
                return ""
        }
        return numberFormatter.string(for: value)!
    }
}
public class BijiliBarFormatter: NSObject, IAxisValueFormatter {
    weak var chart: BarLineChartViewBase?
    var days : [String] = []
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String
    {
        if(consumptionDetailsDB.count > 0){
           
           
            for i in 0...consumptionDetailsDB.count - 1 {
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = NSTimeZone.system
                dateFormatter.dateFormat = "yyyy/MM"

                let date = dateFormatter.date(from: consumptionDetailsDB[(consumptionDetailsDB.count - 1) - i].BM)
                let dateFormatter2 = DateFormatter()
                dateFormatter.locale = NSLocale(localeIdentifier: "en") as Locale
                dateFormatter2.timeZone = NSTimeZone.system
                dateFormatter2.dateFormat = "MMM yyyy"




                switch date {
                case nil:
                    let date_TimeStr = dateFormatter2.string(from: Date())

                    days.append(date_TimeStr)
                    break;
                default:
                    let date_TimeStr = dateFormatter2.string(from: date!)

                    days.append(date_TimeStr)
                    break;
                }
            }
           
            print(days.count)
        }
        
        
        return days[Int(value)]
    }
}
public class PaymentBarFormatter: NSObject, IAxisValueFormatter {
    weak var chart: BarLineChartViewBase?
    var days : [String] = []
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String
    {
        if(paymentHistoryDetailsDB.count > 0){
           
           
            for i in 0...paymentHistoryDetailsDB.count - 1 {
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = NSTimeZone.system
                dateFormatter.dateFormat = "yyyy-MM-dd"

                let date = dateFormatter.date(from: paymentHistoryDetailsDB[(paymentHistoryDetailsDB.count - 1) - i].LVMONTH)
                let dateFormatter2 = DateFormatter()
                dateFormatter2.timeZone = NSTimeZone.system
                dateFormatter2.locale = NSLocale(localeIdentifier: "en") as Locale
                dateFormatter2.dateFormat = "dd MMM yyyy"




                switch date {
                case nil:
                    let date_TimeStr = dateFormatter2.string(from: Date())

                    days.append(date_TimeStr)
                    break;
                default:
                    let date_TimeStr = dateFormatter2.string(from: date!)

                    days.append(date_TimeStr)
                    break;
                }
            }
           
            print(days.count)
        }
        
        
        return days[Int(value)]
    }
}
public class RebateBarFormatter: NSObject, IAxisValueFormatter {
    weak var chart: BarLineChartViewBase?
    var days : [String] = []
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String
    {
        if(rebateDetailsDB.count > 0){
           
            days.append("")
            for i in 0...rebateDetailsDB.count - 1 {
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = NSTimeZone.system
                dateFormatter.dateFormat = "yyyy-MM-dd"

                let date = dateFormatter.date(from: rebateDetailsDB[(rebateDetailsDB.count - 1 ) - i].ZBUDAT)
                let dateFormatter2 = DateFormatter()
                dateFormatter.locale = NSLocale(localeIdentifier: "en") as Locale
                dateFormatter2.timeZone = NSTimeZone.system
                dateFormatter2.dateFormat = "MMM yyyy"




                switch date {
                case nil:
                    let date_TimeStr = dateFormatter2.string(from: Date())

                    days.append(date_TimeStr)
                    break;
                default:
                    let date_TimeStr = dateFormatter2.string(from: date!)

                    days.append(date_TimeStr)
                    break;
                }
            }
           days.append("")
            print(days.count)
        }
        
        
        return days[Int(value)]
    }
}
