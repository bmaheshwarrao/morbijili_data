//
//  loginMobileViewController.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 07/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import AlamofireSoap

import Alamofire
import SwiftyXMLParser
import Reachability
import EzPopup

class loginMobileViewController: CommonVSClass ,UITextFieldDelegate{
    
    @IBOutlet weak var btnOTPSend: UIButton!
    
    @IBOutlet weak var txtMobileNo: DesignableUITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtMobileNo.delegate = self
        txtMobileNo.keyboardType = .numberPad
      
        // Do any additional setup after loading the view.
    }
  
    var reachability = try! Reachability()
    var invStrUnique = String()
       var request_no = String()
       @IBAction func sendOTP(_ sender: UIButton) {
//        self.startLoadingPK(view: self.view,text : "Sending OTP")
        reachability = try! Reachability()
        if(reachability.connection == .unavailable){
            self.view.makeToastHZL("कृपया करके इंटरनेट चालू करे |")
        }else if(txtMobileNo.text == ""){
               self.view.makeToastHZL("मोबाइल नंबर दर्ज करे |")
           }else{
            
            self.startLoadingPK(view: self.view,text : "Sending OTP .... ")
            
            
                  let dateFormatterInv = DateFormatter()
                  dateFormatterInv.timeZone = NSTimeZone.system
                  dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
              
                    invStrUnique = dateFormatterInv.string(from: Date())
               request_no = invStrUnique + txtMobileNo.text!
               let message = "MA_getotp|\(txtMobileNo.text!)|600|1|\(invStrUnique)|\(secID)|\(request_no)"
                   
                   print(message)

                   let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
                   
                   
                  
               AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_string", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
                print("Request: \(response.value)")
                guard let respo = response.value else {
                    return
                }
                self.stopLoadingPK(view: self.view)
                let xml = try! XML.parse(respo)
                let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_stringResponse"]["MA_cons_stringResult"].text
                if let result = accessor {
                    let resultInt = Int(result)
                    if(resultInt! > 0){
                        var storyboard = UIStoryboard(name: "Main", bundle: nil)
                        if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
                              storyboard = UIStoryboard(name: "Main", bundle: nil)
                        }else{
                           storyboard = UIStoryboard(name: "MainHindi", bundle: nil)
                          }
                        
                        let ZIVC = storyboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                                               ZIVC.mobile = self.txtMobileNo.text!
                                               ZIVC.invStrno = self.invStrUnique
                        UserDefaults.standard.set(self.txtMobileNo.text!, forKey: "mobile")
                                           
                                               ZIVC.reqNo = self.request_no
                                                    let popupVC = PopupViewController(contentController: ZIVC, position: .bottom(0.0), popupWidth: UIScreen.main.bounds.width, popupHeight: 350 )
                                                    popupVC.cornerRadius = 0
                                                    popupVC.canTapOutsideToDismiss = false
                                               self.present(popupVC, animated: true, completion: nil)
                    }else{
                        self.showSingleButtonWithMessage(title: "सूचना", message: "कुछ तकनीकी कारणों की वजह से जानकारी उपलब्ध नहीं हो पा रही है या फिर आपका मोबाइल नंबर किसी भी उपभोगता क्र. से लिंक नहीं है , कृपया अपना मोबाइल नंबर लिंक करिये | ", buttonName: "OK")
                    }
                }
                
                   
               }
           }
           
       }
   
      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
               
                    if(textField == txtMobileNo){
                let maxLength = 10
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                        
     //            if(newString.length == maxLength){
     //                    valPhone = true
     //                  }else{
     //                     valPhone = false
     //                  }
                      return  newString.length <= maxLength
                    
                }
                return false
            }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
