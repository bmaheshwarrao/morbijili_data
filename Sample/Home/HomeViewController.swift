//
//  HomeViewController.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 09/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import Reachability
import CoreData
import AlamofireSoap
import Alamofire
import SwiftyXMLParser
import EzPopup
class HomeViewController: CommonVSClass,UINavigationBarDelegate,UINavigationControllerDelegate,UITableViewDelegate, UITableViewDataSource{
   
    @IBOutlet weak var homeTableView: UITableView!
    var PartnerSharingAPI = ElectricityPartnerModelDataAPI()
    var PartnerDB : [String] = []
    var refreshControl = UIRefreshControl()
    var indPath = IndexPath()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = morbijiliee
         NotificationCenter.default.addObserver(self, selector: #selector(self.callsync), name: NSNotification.Name(rawValue: "ShowPop"), object: nil)
        CheckAppVersion()
//        if(UserDefaults.standard.value(forKey: "Language") as! String == "en"){
//          selectedLang =  L10n(language: "en")
//        }else{
//           selectedLang = L10n(language: "hi")
//        }
        ShowLoadDetailsData()
        arrayOneSelect  = [electricityBilllbl,billPayment,meterRead,payHist]
        arrayTwoSelect  = [complaintReg,consumptionPattern,rebate,tarrif,addcons,remCons]
        self.homeTableView.delegate = self
        self.homeTableView.dataSource = self
        self.homeTableView.sectionIndexBackgroundColor = UIColor.clear
        self.homeTableView.sectionIndexColor = UIColor.lightGray
        self.homeTableView.sectionIndexColor = UIColor.blue
        self.homeTableView.isScrollEnabled = true
       
    }
    @objc func callsync(){
        if(showPOPP == false){
            showPOPP = true
          let storyboard = UIStoryboard(name: "Main", bundle: nil)
              
                 
                 
                 let ZIVC = storyboard.instantiateViewController(withIdentifier: "ShowPopLoadMsgViewController") as! ShowPopLoadMsgViewController
                
                 let popupVC = PopupViewController(contentController: ZIVC,  popupWidth: self.view.frame.width - 20, popupHeight: self.view.frame.height - 100)
                 popupVC.cornerRadius = 5
                 popupVC.canTapOutsideToDismiss = true
                 present(popupVC, animated: true, completion: nil)
        }
       }
    
    var showAPI = ShowOnLoadModelDataAPI()
    @objc func ShowLoadDetailsData(){
           
        
           
           showAPI.serviceCalling() { (dict) in
               
               if(dict.count > 0){
                  
               }
               
           }
           
           
       }
    
    
    override func viewWillAppear(_ animated: Bool) {
        getPartnerSharingData()
    }

   @objc func getPartnerSharingData(){
       
    
       
    PartnerSharingAPI.serviceCalling( obj: self) { (dict) in
        self.deleteParnterData()
         self.PartnerDB = dict as! [String]
        if(self.PartnerDB.count > 0){
            for i in 0...self.PartnerDB.count - 1 {
                self.saveDirPathData(name: self.PartnerDB[i])
            }
        }
           
       }
       
       
   }
    func CheckAppVersion(){
        //self.startLoadingPK(view: self.view,text : "Checking Version")
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let dateFormatterInv = DateFormatter()
         dateFormatterInv.timeZone = NSTimeZone.system
         dateFormatterInv.dateFormat = "ddMMyyyyHHmmss"
         let mobile = UserDefaults.standard.string(forKey: "mobile") as! String
         let invStrUnique = dateFormatterInv.string(from: Date())
         let request_no = invStrUnique + mobile
        let message = "MA_vermgr|3|\(version!)|\(secID)|\(request_no)"
            
            print(message)

        let encode = message.aesEncrypt(key: "tjdoi", iv: "tjdoi")
            print(encode!)
            
        
        AlamofireSoap.soapRequest("https://cspdcl.co.in/saubhagya/saubhagya.asmx", soapmethod: "MA_cons_string", soapparameters: ["cons_string":encode!], namespace: "http://tempuri.org").responseString { response in
         print("Request: \(response.value)")
         guard let respo = response.value else {
             return
         }
            self.stopLoadingPK(view: self.view)
         let xml = try! XML.parse(respo)
         let accessor = xml["soap:Envelope"]["soap:Body"]["MA_cons_stringResponse"]["MA_cons_stringResult"].text
         if let result = accessor {
             let resultInt = Int(result)
             if(resultInt! > 0){
                if let url = URL(string: "itms://itunes.apple.com/us/app/apple-store/id1466607686?mt=8"),
                                UIApplication.shared.canOpenURL(url){
                                //   UIApplication.shared.openURL(url)
                
                                if #available(iOS 10, *) {
                                    UIApplication.shared.open(url)
                                } else {
                                    UIApplication.shared.openURL(url)
                                }
                            }
                
             }else{
//                 self.showSingleButtonWithMessage(title: "सूचना", message: "ओ.टी.पी मिलान नही हुआ |", buttonName: "OK")
             }
         }
         
            }
        

    }
    func saveDirPathData(name:String){
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let tasks = Partner(context: context)
        tasks.partner = name
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    func deleteParnterData()
    {
      
        
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Partner")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
        
        do {
            try context.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }
    @IBAction func btnQuickLinkClicked(_ sender: UIButton) {
        var storyBoard = UIStoryboard(name: "Main", bundle: nil)
        if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
             storyBoard = UIStoryboard(name: "Main", bundle: nil)
        }else{
             storyBoard = UIStoryboard(name: "MainHindi", bundle: nil)
        }
        
        if(sender.tag == 0){
            
            let genVC = storyBoard.instantiateViewController(withIdentifier: "ElectricityBillViewController") as! ElectricityBillViewController
            genVC.titleSet = electricityBilllbl
            self.navigationController?.pushViewController(genVC, animated: true)
        }else if(sender.tag == 1){
            
            let genVC = storyBoard.instantiateViewController(withIdentifier: "BillPaymentViewController") as! BillPaymentViewController
            genVC.titleSet = billPayment
            self.navigationController?.pushViewController(genVC, animated: true)
        }else if(sender.tag == 2){
            
            let genVC = storyBoard.instantiateViewController(withIdentifier: "SendMeterReadingViewController") as! SendMeterReadingViewController
            genVC.titleSet = meterRead
            self.navigationController?.pushViewController(genVC, animated: true)
        }else{
            
            
            let genVC = storyBoard.instantiateViewController(withIdentifier: "PaymentHistoryViewController") as! PaymentHistoryViewController
            genVC.titleSet = payHist
            self.navigationController?.pushViewController(genVC, animated: true)
        }
        
    }
    
   @IBAction func btnLinkClicked(_ sender: UIButton) {
    var storyBoard = UIStoryboard(name: "Main", bundle: nil)
    if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
         storyBoard = UIStoryboard(name: "Main", bundle: nil)
    }else{
         storyBoard = UIStoryboard(name: "MainHindi", bundle: nil)
    }
   if(sender.tag == 0){
       
       let genVC = storyBoard.instantiateViewController(withIdentifier: "ComplaintRegisterViewController") as! ComplaintRegisterViewController
       genVC.titleSet = complaintReg
       self.navigationController?.pushViewController(genVC, animated: true)
   }else if(sender.tag == 1){
    consumptionDetailsDB = []
       
       let genVC = storyBoard.instantiateViewController(withIdentifier: "ConsumptionPatternViewController") as! ConsumptionPatternViewController
       genVC.titleSet = consumptionPattern
       self.navigationController?.pushViewController(genVC, animated: true)
   }else if(sender.tag == 2){
    rebateDetailsDB = []
       
       let genVC = storyBoard.instantiateViewController(withIdentifier: "RebateBijiliViewController") as! RebateBijiliViewController
       genVC.titleSet = rebate
       self.navigationController?.pushViewController(genVC, animated: true)
   }else if(sender.tag == 3){
    
        storyBoard = UIStoryboard(name: "Main", bundle: nil)
       let genVC = storyBoard.instantiateViewController(withIdentifier: "TarrifViewController") as! TarrifViewController
       genVC.titleSet = tarrif
       self.navigationController?.pushViewController(genVC, animated: true)
   }else if(sender.tag == 4){
    
       
       let genVC = storyBoard.instantiateViewController(withIdentifier: "AddConsumerViewController") as! AddConsumerViewController
       genVC.titleSet = addcons
       self.navigationController?.pushViewController(genVC, animated: true)
   }else if(sender.tag == 5){
    
       
       let genVC = storyBoard.instantiateViewController(withIdentifier: "RemoveConsumerViewController") as! RemoveConsumerViewController
       genVC.titleSet = remCons
       self.navigationController?.pushViewController(genVC, animated: true)
   }
    
    
      }

    @IBAction func btnSettingClicked(_ sender: UIBarButtonItem) {
        var storyBoard = UIStoryboard(name: "Main", bundle: nil)
        if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
             storyBoard = UIStoryboard(name: "Main", bundle: nil)
        }else{
             storyBoard = UIStoryboard(name: "MainHindi", bundle: nil)
        }
              let genVC = storyBoard.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
              genVC.titleSet = settingVal
              self.navigationController?.pushViewController(genVC, animated: true)
    }
    var reachability = try! Reachability()
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0  ){

            return 0.0
        }else{
            return 35.0

        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {




        let header = tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! HzlMainCellHeaderTableViewCell
        if(section == 2){
          if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
            header.lblHeader.text = "Bijili Links"
          }else{
            header.lblHeader.text = "बिजली लिंक्स"
            }

        }else if(section == 1){
            if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
                       header.lblHeader.text = "Quick Links"
                     }else{
                header.lblHeader.text = "शीघ्र लिंक्स"
                       
                       }
            

        }else{
            header.lblHeader.text = ""
        }
        header.separatorInset = UIEdgeInsets(top: 0, left: 10000, bottom: 0, right: 0);
        return header
    }
    var indexing = 0
   var arrayOneSelect : [String] = ["Electricity Bill","Bill Payment","Send Meter Reading","Payment History"]
    var imgArrayOne : [String] = ["electricityBill","billPayment","meterReading","paymentHistory"]
    var arrayTwoSelect : [String] = ["Complaint Registration","Consumption Pattern","Rebate on bijlee Bill Half Yojna","Tariff ( Electricity Rate )","Add Consumer No.","Remove Consumer No."]
    var imgArrayTwo : [String] = ["Complaint","consumption","rebate","tarif","addConsumer","removeConsumer"]
//    var arrayOneSelect : [String] = ["इलेक्ट्रिसिटी बिल","Bill Payment","Send Meter Reading"]
//        var arrayTwoSelect : [String] = ["Complaint Registration","Consumption Pattern","Payment History","Rebate on bijlee Bill Half Yojna","Tariff ( Electricity Rate )","Add Consumer No.","Remove Consumer No."]
   
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 1
        }else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let page = "page"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: page) as! PageTableViewCell
            if(indexing == 0){
            cell.contentView.frame.size.height = cell.imageCollectionView.frame.size.height
            cell.contentView.frame.size.width = cell.imageCollectionView.frame.size.width
            
            
                cell.pageView(ApplicationName: "HZL_ONE")
            tableView.isScrollEnabled = true
            }
            return cell
            
        }else  if indexPath.section == 1 {
            let page = "buttonsQuick"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: page) as! MainGraphTableViewCell
            if(indexing == 0){
                cell.HomePageCalling(obj: arrayOneSelect,objImg : imgArrayOne,section : 0)
            tableView.isScrollEnabled = true
            }
            return cell
            
        }else  {
            let page = "buttons"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: page) as! MainGraphTableViewCell
            if(indexing == 0){
            cell.HomePageCalling(obj: arrayTwoSelect,objImg : imgArrayTwo,section : 1)
            tableView.isScrollEnabled = true
            }
            return cell
            
        }
        
        
       
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        

        let screenSize = UIScreen.main.bounds
        //let screenHeight = screenSize.height-navheight-tabHeight!
        
        let screenWidth = screenSize.width
        if indexPath.section == 0{
            return (screenWidth/2) + 25
        }else if indexPath.section == 1{
            print(arrayOneSelect.count)
            var height : CGFloat = 0
            let dd = arrayOneSelect.count / 4
            let ff = arrayOneSelect.count % 4
            if(ff == 0){
                height = CGFloat(dd * 100)
            }else{
                height = CGFloat((dd+1) * 100)
            }
            
            
            return height
        }else {
            var height : CGFloat = 0
            let dd = arrayTwoSelect.count / 3
            let ff = arrayTwoSelect.count % 3
            if(ff == 0){
                height = CGFloat(dd * 100)
            }else{
                height = CGFloat((dd+1) * 100)
            }
            
            
            return height
        }
        
    }
    
  
    
    
   
    
    
    
    
}
