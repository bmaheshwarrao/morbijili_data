//
//  PageTableViewCell.swift
//  Sesa Goa Idea App
//
//  Created by SARVANG INFOTCH on 05/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

import Reachability



class PageTableViewCell: UITableViewCell,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var view = UIView()
    var timer = Timer()
    var lastXAxis = CGFloat()
    var contentOffset = CGPoint()
    var i = CGFloat()
   
    
    var scrollingTimer = Timer()
    
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.imageCollectionView.delegate = self
        self.imageCollectionView.dataSource = self
        
      //  getBannerImageData()
        
        // Initialization code
    }
    
    func pageFromMain()
    {
        self.timer.invalidate()
        
        
        self.timer = Timer()
        self.lastXAxis = CGFloat()
        self.contentOffset = CGPoint()
        self.imageCollectionView.contentOffset = CGPoint.init(x: 0, y: 0)
        
    }
    
   
    
    var reachablty = try! Reachability()
     var dataArray: [BannerDataModel] = []
   
    func pageView(ApplicationName : String)
    {
  
   
        self.imageCollectionView.reloadData()
        
    }
        
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath) as! ImageSlideCollectionViewCell
        cell.imageSlide.image = UIImage(named : "banner")
//        cell.contentView.frame.size.width = cell.imageSlide.frame.size.width
//        cell.contentView.frame.size.height = cell.imageSlide.frame.size.height
//
        cell.pageControl.isHidden = true
        cell.pageControl.numberOfPages = 1
//        if let url = NSURL(string: dataArray[indexPath.row].Banner_URL) {
//            cell.imageSlide.sd_setImage(with: url as URL!, placeholderImage: UIImage.init(named: "placed"))
//        }
//
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: self.imageCollectionView.bounds.size.width, height: self.imageCollectionView.bounds.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        //print(indexPath.item)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let myCell = cell as? ImageSlideCollectionViewCell {
            myCell.pageControl.currentPage = indexPath.item
        }
        
    }
    
    @objc func scrollToNextCell(){
        
        let cellSize = CGSize(width:self.contentView.frame.width, height:self.contentView.frame.height)
        
        
        self.contentOffset = self.imageCollectionView.contentOffset
        
        if(self.lastXAxis == self.contentOffset.x)
        {
            self.imageCollectionView.contentOffset = CGPoint.init(x: 0, y: 0)
            
        }
        
        self.imageCollectionView.scrollRectToVisible(CGRect(x:self.contentOffset.x + cellSize.width , y:self.contentOffset.y, width:cellSize.width , height:cellSize.height), animated: true)
        
        
        
        self.lastXAxis = self.contentOffset.x
        
        
    }
    
    func startTimer() {
        
        self.timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(PageTableViewCell.scrollToNextCell), userInfo: nil, repeats: true);
        
        
    }
    
    
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
class BannerDataModel: NSObject {
    
    
    
    var ID = String()
    var Application_Name = String()
    var Banner_URL = String()

    
    

    
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = ""
        }else{
            
            let ros = str["ID"]
            
            self.ID = (ros?.description)!
            
            
        }
        if str["Application_Name"] is NSNull || str["Application_Name"] == nil{
            self.Application_Name =  ""
        }else{
            
            let fross = str["Application_Name"]
            
            self.Application_Name = (fross?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
        
        if str["Banner_URL"] is NSNull || str["Banner_URL"] == nil{
            self.Banner_URL = "0"
        }else{
            let emp1 = str["Banner_URL"]
            
            self.Banner_URL = (emp1?.description)!
            // print(self.Employee_1)
        }
       
        
        
    }
    
}
