
//
//  MainGraphTableViewCell.swift
//  VallSafety
//
//  Created by SARVANG INFOTCH on 18/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit
import Reachability
import Localize_Swift
class MainGraphTableViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
 let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
   
    @IBOutlet weak var buttonCollectionView: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        buttonCollectionView.delegate = self
        buttonCollectionView.dataSource = self
    buttonCollectionView.isScrollEnabled = true
        // Initialization code
    }
   var sectionCollect = Int()
    var reachablty = try! Reachability()
    var dataArray : [String] = []
    var dataArrayImage : [String] = []
    func HomePageCalling(obj : [String],objImg : [String],section : Int)
    {
        dataArrayImage = objImg
       dataArray = obj
        sectionCollect = section
        self.buttonCollectionView.reloadData()
        
        
        
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        return dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buttons", for: indexPath) as! buttonsCollectionViewCell
      
        cell.textViewBtnName.text =    dataArray[indexPath.row]
        cell.textViewBtnName.font = UIFont(name: "RobotoSlab-Regular", size: 12.0)
        cell.imgButton.image = UIImage(named: dataArrayImage[indexPath.row])
        cell.btnAppName.tag = indexPath.row
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var wid = CGFloat()
        if(sectionCollect == 0){
            wid = UIScreen.main.bounds.width / 4
        }else{
            wid = UIScreen.main.bounds.width / 3
        }
        
        print(wid)
        return CGSize(width: wid, height: 100)
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        //print(indexPath.item)
        
        
    }
}
