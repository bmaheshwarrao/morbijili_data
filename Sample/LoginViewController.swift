//
//  LoginViewController.swift
//  MorBijli
//
//  Created by Bunga Mahesh on 06/03/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import AlamofireSoap
import Alamofire
import CryptoSwift
import ViewPager_Swift
import EzPopup
var showPOPP = false
class LoginViewController: CommonVSClass {
    
    @IBOutlet weak var containerView: UIView!
    var tabs = [ViewPagerTab(title: "Register Bp No", image: UIImage(named: "legal")),ViewPagerTab(title: "Mobile No", image: UIImage(named: "legal"))]
    
    var options: ViewPagerOptions?
    var pager:ViewPager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ShowLoadDetailsData()
         NotificationCenter.default.addObserver(self, selector: #selector(self.callsync), name: NSNotification.Name(rawValue: "ShowPop"), object: nil)
        
        if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
            tabs = [ViewPagerTab(title: "Registration", image: UIImage(named: "legal")),ViewPagerTab(title: "Login", image: UIImage(named: "legal"))]
        }else{
            tabs = [ViewPagerTab(title: "मोबाइल नo पंजीकरण", image: UIImage(named: "legal")),ViewPagerTab(title: "लॉग इन", image: UIImage(named: "legal"))]
        }
        self.options = ViewPagerOptions()
        self.options?.isTabIndicatorAvailable = true
        self.options?.isTabHighlightAvailable = true
        self.options?.tabIndicatorViewBackgroundColor = UIColor.init(hexString: "A8BE1B", alpha: 1.0)!
        self.options?.tabViewBackgroundDefaultColor = UIColor.init(hexString: baseColorGreen, alpha: 1.0)!
        self.options?.tabViewBackgroundHighlightColor = UIColor.init(hexString: baseColor, alpha: 1.0)!
        self.options?.tabViewHeight = 45
        self.options?.tabIndicatorViewHeight = 5
        self.options?.viewPagerTransitionStyle = .scroll
        self.options?.tabType = .basic
        self.options?.distribution = .segmented
        
      guard let options = self.options else { return }
        pager = ViewPager(viewController: self, containerView: containerView)
           //  pager = ViewPager(viewController: self)
             pager?.setOptions(options: options)
             pager?.setDataSource(dataSource: self)
             pager?.setDelegate(delegate: self)
             pager?.build()
             
         }
         
         
         deinit {
             print("Memory Deallocation")
         }

        // and many more...
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   @objc func callsync(){
    if(showPOPP == false){
        showPOPP = true
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
               
                  
                  
                  let ZIVC = storyboard.instantiateViewController(withIdentifier: "ShowPopLoadMsgViewController") as! ShowPopLoadMsgViewController
                 
                  let popupVC = PopupViewController(contentController: ZIVC,  popupWidth: self.view.frame.width - 20, popupHeight: self.view.frame.height - 100)
                  popupVC.cornerRadius = 5
                  popupVC.canTapOutsideToDismiss = true
                  present(popupVC, animated: true, completion: nil)
    }
        }
    var showAPI = ShowOnLoadModelDataAPI()
       @objc func ShowLoadDetailsData(){
              
           
              
              showAPI.serviceCalling() { (dict) in
                  
                  if(dict.count > 0){
                     
                  }
                  
              }
              
              
          }
    
}
extension LoginViewController: ViewPagerDataSource {
    
    func numberOfPages() -> Int {
        return 2
    }
    
    func viewControllerAtPosition(position:Int) -> UIViewController {
        
            var storyboard = UIStoryboard(name: "Main", bundle: nil)
            if(UserDefaults.standard.string(forKey: "Language") as! String == "en"){
                  storyboard = UIStoryboard(name: "Main", bundle: nil)
            }else{
               storyboard = UIStoryboard(name: "MainHindi", bundle: nil)
              }
                if(position == 0) {
                
                    let empVC = storyboard.instantiateViewController(withIdentifier: "LoginWithBPViewController") as! LoginWithBPViewController
                    return  empVC as UIViewController
                }else {
                    let nonEmpVC = storyboard.instantiateViewController(withIdentifier: "loginMobileViewController") as! loginMobileViewController
                    return  nonEmpVC as UIViewController
                }
    }
    
    func tabsForPages() -> [ViewPagerTab] {
        return tabs
    }
    
    func startViewPagerAtIndex() -> Int {
        return 0
    }
}

extension LoginViewController: ViewPagerDelegate {
    
    func willMoveToControllerAtIndex(index:Int) {
        print("Moving to page \(index)")
    }
    
    func didMoveToControllerAtIndex(index: Int) {
        print("Moved to page \(index)")
    }
}
